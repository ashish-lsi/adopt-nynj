<?php

namespace Modules\Weather\Entities;
use Illuminate\Database\Eloquent\Model;

class WeatherCity extends Model {

    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'country', 'coord_lon', 'coord_lat'];

}
