<?php

namespace Modules\Weather\Entities;

use Illuminate\Support\Facades\Config;

class Helper {

    public static function getWeatherDetails($params) {
        $key = Config::get('globals.weather_api_key');
        $url = Config::get('globals.weather_api_url');

        $params['url'] = $url . "?lat=" . $params['lat'] . '&lon=' . $params['long'] . '&appid=' . $key . '&units=imperial';

        $data = self::curlGET($params);

        return json_decode($data);
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to call the api via CURL
     * @param array $params
     * @return string
     */
    public static function curlGET($params) {
        $handle = curl_init();

        curl_setopt_array($handle, array(
            CURLOPT_URL => $params['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0",
                )
        );

        $data = curl_exec($handle);

        if (!$data) {
            die('Error: "' . curl_error($handle) . '" - Code: ' . curl_errno($handle));
        }

        curl_close($handle);

        return $data;
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to get the weather alerts by lat and long
     * @param array $params
     * @return string
     */
    public static function getWeatherAlerts($params) {
        $url = Config::get('globals.weather_alerts_url');

        $params['url'] = $url . $params['lat'] . "," . $params['long'];

        $data = self::curlGET($params);

        return json_decode($data);
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to convert the temp from kelvin to fahrenheit
     * @param float $kelvinVal
     * @return float
     */
    public static function convertFahrenheit($kelvinVal) {
        $val = ($kelvinVal - 273.15) * 9 / 5 + 32;
        $formatVal = number_format((float) $val, 1, '.', '');  // Outputs -> 105.00

        return $formatVal;
    }

}
