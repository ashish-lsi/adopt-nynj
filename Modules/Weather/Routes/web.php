<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Frontend\CommonController;
use App\Http\Controllers\Frontend\User\DashboardController;
use Modules\Weather\Http\Controllers\WeatherController;

Route::prefix('weather')->group(function() {
    Route::get('/', 'WeatherController@index');
    Route::get('getWeatherCity', [WeatherController::class, 'getWeatherCity'])->name('get_weather_city');
    Route::post('showWeather', [WeatherController::class, 'ShowWeather'])->name('show_weather');
    Route::post('getLatLang', [WeatherController::class, 'getLatLngDetails'])->name('getLatLngDetails');
});
