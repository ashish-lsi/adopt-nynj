<?php

namespace Modules\MLM\Entities;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class MLMConnection extends Model
{
    protected $table = 'mlm_connections';
    protected $fillable = [];

    function parent(){
        return $this->belongsTo(User::class,'user_id');
    }
}
