<?php

namespace Modules\MLM\Emails;

use App\Models\Auth\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInvitationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $user;
    private $url;
    public function __construct(User $user,$url)
    {
        //
        $this->user = $user;
        $this->url = $url;
    }

    /**
     * Build the message.s
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mlm::mails.send_invitation',['user'=>$this->user,'url'=>$this->url])
        ->subject('Invitation from '.$this->user->name);
    }
}
