<?php

namespace App\Frontend;

use Illuminate\Database\Eloquent\Model;

class AdminReport extends Model
{
    //
    protected $table = "admin_report";
}
