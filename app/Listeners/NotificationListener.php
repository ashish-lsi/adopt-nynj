<?php

namespace App\Listeners;

use App\Events\PostApply;
use App\Models\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\PostApplyMailJob;

class NotificationListener {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostApply  $event
     * @return void
     */
    public function handle(PostApply $event) {
        //
        $notification = new Notification();
        $notification->user_id = auth()->user()->id;
        $notification->post_id = $event->post->post_id;
        $notification->post_user_id = $event->post->company_id;
        $notification->comment_id = $event->post->comment_id;
        $notification->type = 1;
        $notification->save();

        dispatch(new PostApplyMailJob($notification, $event->token));
    }

}
