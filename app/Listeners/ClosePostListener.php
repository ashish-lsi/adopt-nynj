<?php

namespace App\Listeners;

use App\Events\ClosePostEvent;
use App\Jobs\PostCloseMailJob;
use App\Models\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClosePostListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClosePostEvent  $event
     * @return void
     */
    public function handle(ClosePostEvent $event)
    {
        //
       
        $notification = new Notification();
        $notification->user_id = auth()->user()->id;
        $notification->post_id = $event->comment->post_id;
        $notification->comment_id = $event->comment->comment_id;
        if( $event->comment->reply_user_id != null)
        $notification->post_user_id = $event->comment->reply_user_id;  
        else 
        $notification->post_user_id = $event->comment->post->company_id;       
        $notification->type = 0;
        $notification->save();
        dispatch(new PostCloseMailJob($notification));
        //dd($event);
    }
}
