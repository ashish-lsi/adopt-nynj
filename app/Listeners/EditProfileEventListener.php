<?php

namespace App\Listeners;

use App\Events\EditProfileEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\EditProfileMailJob;

class EditProfileEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EditProfileEvent  $event
     * @return void
     */
    public function handle(EditProfileEvent $event)
    {
        //
        dispatch(new EditProfileMailJob($event->user));

    }
}
