<?php

namespace App\Mail;

use App\Models\Auth\User;
use App\Models\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostReportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $post;
    public $reportText,$user;
    public function __construct(Post $post,User $user, $reportText)
    {
        //
        $this->post = $post;
        $this->user = $user;
        $this->reportText = $reportText;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = $this->user->company_name;
        $post_title = $this->post->title;
        return $this
            //->to(config('mail.from.address'), config('mail.from.name'))
            ->view('frontend.mail.postReportMail', ['post' => $this->post,'user'=>$this->user,'reportText'=>$this->reportText])
            //->text('frontend.mail.contact-text')
            ->subject(__(" $company has flagged a post", ['app_name' => app_name()]))
            ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
