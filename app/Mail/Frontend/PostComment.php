<?php

namespace App\Mail\Frontend;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;

class PostComment extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $notification;

    public function __construct(Notification $notification) {
        $this->notification = $notification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        // return $this->view('view.name');

        \Illuminate\Support\Facades\Log::info($this->notification->reply_user_name .'-----------');
        
        if (isset($this->notification->type) && $this->notification->type == 3) {
            return $this
                            ->view('frontend.mail.commentTag', ['notification' => $this->notification])
                            ->subject(__('strings.emails.commentTag.subject', ['app_name' => app_name()]))
                            ->from(config('mail.from.address'), config('mail.from.name'));
        } else {

            return $this
                            ->view('frontend.mail.postComment', ['notification' => $this->notification])
                            ->subject(__('See who commented on your post', ['app_name' => app_name()]))
                            ->from(config('mail.from.address'), config('mail.from.name'));
        }
    }

}
