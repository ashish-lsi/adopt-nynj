<?php

namespace App\Mail\Frontend;

use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostApproveMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $notification;

    public function __construct(Notification $notification) {
        //
        $this->notification = $notification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        //->to(config('mail.from.address'), config('mail.from.name'))
        ->view('frontend.mail.postApprove', ['notification' => $this->notification])
        //->text('frontend.mail.contact-text')
        ->subject(__('strings.emails.postApprove.subject', ['app_name' => app_name()]))
        ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
