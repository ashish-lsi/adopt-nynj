<?php

namespace App\Mail\Frontend;

use App\Models\Auth\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRejectedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = $this->user->company_name;
        return $this
            //->to(config('mail.from.address'), config('mail.from.name'))
            ->view('frontend.mail.userRejected', ['user' => $this->user])
            //->text('frontend.mail.contact-text')
            ->subject(__(" Your registration was rejected", ['app_name' => app_name()]))
            ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
