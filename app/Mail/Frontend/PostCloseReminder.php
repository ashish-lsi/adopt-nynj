<?php

namespace App\Mail\Frontend;

use App\Models\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostCloseReminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $post;
    public function __construct(Post $post)
    {
        //
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            //->to(config('mail.from.address'), config('mail.from.name'))
            ->view('frontend.mail.postCloseReminder', ['post' => $this->post])
            //->text('frontend.mail.contact-text')
            ->subject(__('Have you completed the transaction?', ['app_name' => app_name()]))
            ->from(config('mail.from.address'), config('mail.from.name'));
        //->replyTo($this->request->email, $this->request->name);
    }
}
