<?php

namespace App\Mail\Frontend;

use App\Models\Auth\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->view('frontend.mail.userActivated', ['user' => $this->user])
        //->text('frontend.mail.contact-text')
        ->subject(__('Congratulations! Your account is now approved', ['app_name' => app_name()]))
        ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
