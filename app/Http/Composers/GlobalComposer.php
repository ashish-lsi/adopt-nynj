<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use App\Models\Auth\User;
use Modules\Weather\Entities\Helper;

/**
 * Class GlobalComposer.
 */
class GlobalComposer {

    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view) {
        $view->with('logged_in_user', auth()->user());
        
        if (!is_null(auth()->user())) {
            $address = User::with('address')->where('address_id', auth()->user()->address_id)->first();
            
            if (isset($address->address)) {
                $params['lat'] = $address->address->lat;
                $params['long'] = $address->address->lang;

                $cityTmpData = Helper::getWeatherDetails($params);
                $view->with('user_city_id', $cityTmpData->city->id ?? 0);
            }
        }

        // $view->with('categories',Category::where('enabled', 0)->orderBy('name', 'asc')->get());
    }

}
