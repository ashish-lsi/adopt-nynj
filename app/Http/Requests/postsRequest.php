<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class postsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            "category_id" => ['required'],
            //"location" => ['required'],
            "type" => ['required', 'string', 'max:20'],
            "title" => ['required', 'string', 'max:191'],
            "article" => ['required', 'string'],
            "file" => ['max:10000']

        ];
    }


    public function messages()
    {
        return [
            'category_id.required' => 'Category is required',
            //'location.required'  => 'Location is required',
            'type.required' => 'Pleaes choose a help',
            'article' => 'Article field is required',
            'file.max' => 'Max file size upload is 10 MB'
        ];
    }
}
