<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\Frontend\UserActivated;
use App\Models\Auth\User;
use App\Models\WordpressPost;
use Auth;
use Corcel\Model\Post;
use Igaster\LaravelTheme\Facades\Theme;
use App\VisitorsLog;
use Mail;
use Request;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */

    public function approveUser($token)
    {
        $user = User::where('confirmation_code', $token)->where('active',0)->first();
        if($user){
            $userActivated = new UserActivated($user);
            Mail::to($user->email)->send($userActivated);
            $user->active = 1;
            $user->save();
            return redirect('/login')->withFlashSuccess(__("Account Approved Successfully"));
        }
        return redirect('/login')->withFlashSuccess(__("User Not Found!"));

        // dd($user);
       
    }
    public function index()
    {



        if (in_array(Theme::get(), ['adoptFarm', 'adoptNY', 'empowerTheme'])) {
            if (Auth::check())
                return redirect('/dashboard');

            else {
                // $posts = WordpressPost::whereIn('type', ['1', '2', '3'])->orderby('created_at', 'desc')->get();
                // return view('frontend.index', compact('posts'));
                return redirect('/login');

            }
        } else {
            $posts = WordpressPost::whereIn('type', ['1', '2', '3'])->orderby('created_at', 'desc')->get();
            return view('frontend.index', compact('posts'));
        }
    }

    public function alertsView($id)
    {
        $alerts = WordpressPost::where(['id' => $id])->first();
        return view('frontend.viewAlerts', compact('alerts'));
    }
}
