<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\postsRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\Address;
use App\Models\Post;
use App\Models\State;
use App\Models\City;
use App\Models\DiversityType;
use App\Helpers\Helper;
use App\Models\Attachment;
use App\Models\WeatherCity;
use App\Models\SubscribeNews;
use Illuminate\Support\Facades\DB;
use Image;
use Theme;
use App\Models\WordpressPost;
use App\Models\Notification;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request) {

        $requested = Post::where(['enabled' => 1, 'type' => 0])->count();
        $donated = Post::where(['enabled' => 1, 'type' => 1])->count();

        $user = auth()->user();
        if (in_array(Theme::get(), ['adoptNY', 'empowerTheme'])) {
            $posts = Post::with('user')->where(['enabled' => 1, 'status' => 0])->whereIn('type', [1])->orderby('created_at', 'DESC')->get();

            //Getting the help seek and give count
            $helpCnt = DB::select("select type, count(*) as aggregate from `posts` where (`company_id` = $user->id and `enabled` = 1 and `status` = 0 and posts.`enabled` = 1) group by `type`");

            //Getting the start doners
            $startDoners = DB::select("select count(posts.type) as aggregate, users.* from `posts`, users where (users.id = posts.company_id and posts.`type` = 1 and posts.`status` = 0 and posts.`enabled` = 1) group by company_id ORDER BY aggregate DESC LIMIT 5");

            //Getting most asked categories
            $popularCats = DB::select("SELECT C.*,
                                            Count(p.post_id) AS aggregate
                                     FROM categories C
                                     LEFT OUTER JOIN posts p ON p.category_id = C.category_id and p.`status` = 0 and p.`enabled` = 1
                                     GROUP BY C.name");
            usort($popularCats, function($a, $b) use($request) {
                if ($a == $b)
                    return 0;
                if ($request->cat_filter == 'z-a')
                    return ($a->name_clean > $b->name_clean) ? -1 : 1;
                else
                    return ($a->name_clean < $b->name_clean) ? -1 : 1;
            });

            $news = WordpressPost::whereIn('type', ['1', '2', '3'])->orderby('created_at', 'desc')->get();

            //$pending_actions details
            $pending_actions = Notification::with('post', 'user', 'comment')->where('post_user_id', auth()->user()->id)->where('seen', 0)->where('type', 1)->whereNotNull('comment_id')->orderBy('created_at', 'DESC')->get();
            $pending_actions_count = $pending_actions->count();

            return view('frontend.user.dashboard', compact('posts', 'requested', 'donated', 'helpCnt', 'startDoners', 'popularCats', 'news', 'pending_actions', 'pending_actions_count'));
        }
    }

    public function store(postsRequest $request) {

        $filename = "";

        if ($request->has('file')) {
            $uploadedFile = $request->file('file');
            $filename = time() . $uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
                    'files', $uploadedFile, $filename
            );
        }

        $post = new Post;
        $post->company_id = $request->company_id;
        $post->type = $request->type;

        if ($post->type != 2) {
            $post->category_id = $request->category_id;
            $post->location = $request->location;
            $post->city = $request->city;
            $post->state = $request->state;
            $post->pincode = $request->pincode;
        }
        if ($post->type == 1) {
            $post->diversity_type_ids = implode(',', $request->diversity);
        }

        $post->title = $request->title;
        $post->status = 0;
        $post->article = $request->article;

        $post->file = $filename;
        $post->enabled = $request->enabled;

        $post->save();

        return back()->with('success', 'Post created successfully.');
    }

    public function storeNew(Request $request) {

        $this->validate($request, [
            'attachements.*' => 'mimes:doc,pdf,docx,png,jpg,jpeg,xlsx,xls,csv'
        ]);

        $filename = "";

        if ($request->post_id != "") {
            $post = Post::where("post_id", $request->post_id)->first();
        } else {
            $post = new Post();
            $post->company_id = auth()->user()->id;
        }

        $post->type = $request->type;

        if ($post->type != 2) {
            $post->category_id = $request->category_id;
            $post->location = $request->location;
            $post->city = $request->City;
            $post->state = $request->State;
            $post->pincode = $request->Pincode;
            $post->lat = $request->addr_lat;
            $post->lang = $request->addr_long;
        }

        $post->title = $request->title;
        $post->status = 0;
        $post->article = $request->article;

        $post->file = $filename;
        $post->qnty = $request->qnty;
        $post->enabled = $request->enabled;

        $post->save();

        if ($request->hasfile('attachements')) {
            $files = $request->file('attachements');

            foreach ($files as $file) {
                $filename = time() . $file->getClientOriginalName();
                $filename_thumb = 'thumb_' . $filename;
                $file_ext = $file->getClientOriginalExtension();
                if (in_array($file_ext, ['png', 'jpeg', 'jpg'])) {
                    ini_set('memory_limit', '256M');
                    Image::make($file)->resize(50, 50)->save('storage/app/public/thumb/' . $filename_thumb);
                }

                Storage::disk('local')->putFileAs(
                        'public', $file, $filename
                );

                $attachement = new Attachment();
                $attachement->post_id = $post->post_id;
                $attachement->location = $filename;
                $attachement->thumb = $filename_thumb;
                $attachement->extension = $file_ext;
                $attachement->save();
            }
        }

        if ($request->post_id != "")
            return redirect(route('frontend.user.post-details', ['post_id' => $post->post_id, 'post_title' => $post->title,]))->with('success', 'Post updated successfully.');
        else
            return redirect(route('frontend.user.post-details', ['post_id' => $post->post_id, 'post_title' => $post->title,]))->with('success', 'Post created successfully.');
    }

    /**
     * This function is used get the weather forecast details in the new theme adoptFarm
     * @return type
     */
    public function showWeather() {
        $searchQuery = $_POST ?? '';

        //Checking if query is from new theme
        if (isset($searchQuery['address'])) {

            $params['lat'] = $searchQuery['addr-lat'];
            $params['long'] = $searchQuery['addr-long'];
            $params = $searchQuery;
        } else {

            //If searched with numeric zip then set zip
            $params['code'] = is_numeric($searchQuery['txtcityname']) ? $searchQuery['txtcityname'] : $searchQuery['txtcityid'];
            //Setting the type of search
            $params['type'] = is_numeric($searchQuery['txtcityname']) ? 'zip' : 'cityid';

            //Get city details
            $result = WeatherCity::where('id', $params['code'])->first();
            $params['lat'] = $result->coord_lat;
            $params['long'] = $result->coord_lon;
        }

        //Call 5 day / 3 hour forecast data
        $cityTmpData = Helper::getWeatherDetails($params);

        $citydata = [];
        if (isset($cityTmpData->cod) && $cityTmpData->cod == 200) {
            foreach ($cityTmpData->list as $data) {
                $date = date('D M j', strtotime($data->dt_txt));
                $citydata['list'][$date][] = (object) [
                            'icon' => $data->weather[0]->icon,
                            'description' => $data->weather[0]->description,
                            'temp_min' => $data->main->temp_min,
                            'temp_max' => $data->main->temp_max,
                            'speed' => $data->wind->speed,
                            'humidity' => $data->main->humidity,
                            'pressure' => $data->main->pressure,
                            'time' => date('G:i a', strtotime($data->dt_txt))
                ];
            }

            $citydata['city'] = $cityTmpData->city;
        }

        //Call alerts data
        $alertsdata = Helper::getWeatherAlerts($params);

        return view('frontend.user.weather', compact('searchQuery', 'params', 'citydata', 'alertsdata'));
    }

    public function PrivacyPolicy() {
        return view('frontend.user.privacy');
    }

    public function subscribe_news() {
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $exists = SubscribeNews::where('email', $email)->first();

            if ($exists !== null) {
                return back()->with('success', 'Sorry you are already subscribed to us!!');
            }

            $model = new SubscribeNews();
            $model->email = $_POST['email'];
            $model->save();

            return back()->with('success', 'Thank you for subscribing us!!');
        }
    }

}
