<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Category;
use App\Models\OrganizationServes;
use App\Models\WordpressPost;

class NewDashboardController extends Controller {

    //
    public function dashboard(Request $request) {

        $organization_serves = OrganizationServes::where('active',1)->get();
        $cat_id = Category::where('name', 'LIKE', '%' . $request->get('query') . '%')->first();

        
        $posts = Post::with('user', 'attachements', 'comments')
                ->join('users','users.id','=','posts.company_id')
                ->leftJoin('user_org_serves','user_org_serves.user_id','=','posts.company_id')

                ->where(function ($query) use ($request, $cat_id) {
                    if (!is_null($request->get('query'))) {
                        $query->where('title', 'LIKE', '%' . $request->get('query') . '%')
                        ->orWhere('article', 'LIKE', '%' . $request->get('query') . '%')
                        ->orWhere('location', 'LIKE', '%' . $request->get('query') . '%')
                        ->orWhere('state', 'LIKE', '%' . $request->get('query') . '%')
                        ->orWhere('city', 'LIKE', '%' . $request->get('query') . '%')
                        ->orWhere('pincode', 'LIKE', '%' . $request->get('query') . '%');
                        if (!is_null($cat_id))
                            $query->orWhere('category_id', $cat_id->category_id);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->category != "" && !is_null($request->category[0]))
                        $query->whereIn('posts.category_id', $request->category);
                })
                ->where(function ($query) use ($request) {
                    if ($request->from_date != "" && $request->to_date != "")
                        $query->whereDate('posts.created_at', '>=', $request->from_date)
                        ->whereDate('posts.created_at', '<=', $request->to_date);
                })
                ->where(function ($query) use ($request) {
                    if ($request->location != "")
                        $query->where('location', 'LIKE', '%' . $request->location . '%');
                    if ($request->state != "")
                        $query->where('state', 'LIKE', '%' . $request->state . '%');
                    if ($request->city != "")
                        $query->where('city', 'LIKE', '%' . $request->city . '%');
                    if ($request->company != "")
                        $query->where('posts.company_id', auth()->user()->id);
                    if ($request->pincode != "")
                        $query->where('pincode', 'LIKE', '%' . $request->pincode . '%');
                })->where(function ($query) use ($request) {
                    if ($request->diversity != "")
                        $query->whereRaw("FIND_IN_SET(" . $request->diversity . ",diversity_type_ids)");
                })
                ->where(function ($query) use ($request) {
                    if ($request->type != "")
                        $query->where('type', $request->type);
                })
                ->where(['enabled' => 1, 'status' => 0])
                ->where(function ($query) use ($request) {
                    if ($request->has('organization_serves') )
                        $query->whereIn("user_org_serves.org_serve_id",$request->organization_serves);
                })
                ->groupby('posts.post_id')
                ->orderby('posts.created_at', $request->has('order_by')&& $request->order_by == "newest" ? 'DESC':'ASC')//->toSql();
                ->paginate(20)
                ->appends($request->all());

        if ($request->ajax()) {
            return json_encode(["html" => view('frontend.includes.postCompact', compact('posts'))->render(), 'count' => count($posts)]);
        }
        $news = WordpressPost::whereIn('type', ['1', '2', '3'])->orderby('created_at', 'desc')->take(100)->get();
        return view('frontend.user.home', compact('posts', 'news','organization_serves'));
    }

}
