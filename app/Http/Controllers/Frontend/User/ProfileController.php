<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Models\Auth\User;
use App\Models\Post;
use Illuminate\Support\Facades\Storage;
use App\Models\Address;
use App\Events\EditProfileEvent;
use App\Models\Achievement;
use Illuminate\Http\Request;
use App\Models\Notification;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller {

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function index() {
        $user = $loggedInUser = auth()->user();
        $achievements = Achievement::where('user_id', $user->id)->where('active', 1)->get();
        $posts = Post::with('comments')
                        ->latest()
                        ->leftJoin('categories', 'posts.category_id', '=', 'categories.category_id')
                        ->leftJoin('users', 'posts.company_id', '=', 'users.id')
                        ->leftJoin('address', 'posts.address_id', '=', 'address.address_id')
                        ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                        ->where('posts.enabled', 1)
                        ->where('posts.company_id', $user->id)->get();

        $interactionsPostIds = Notification::where('user_id', $user->id)->orWhere('post_user_id', $user->id)->groupBy('post_id')->orderby('created_at', 'DESC')->pluck('post_id')->toArray();

        $interactions = Post::with('comments')
                        ->latest()
                        ->leftJoin('categories', 'posts.category_id', '=', 'categories.category_id')
                        ->leftJoin('users', 'posts.company_id', '=', 'users.id')
                        ->leftJoin('address', 'posts.address_id', '=', 'address.address_id')
                        ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                        ->where('posts.enabled', 1)
                        ->where('status', 0)->whereIn('posts.post_id', $interactionsPostIds)->get();

        $userAddr = Address::where('company_id', auth()->user()->id)->get();

        return view('frontend.user.profile', compact('user', 'posts', 'achievements', 'loggedInUser', 'userAddr', 'interactions'));
    }

    public function delete_achievement(Request $request, $id) {
        $achievement = Achievement::where('user_id', auth()->user()->id)->where('id', $id)->first();
        $achievement->active = 0;
        $achievement->save();
        // $achievement->update(["active"=>0]);
        return back()->with(['err' => "", 'msg' => "Deleted Successfully"]);
    }

    public function add_achievement(Request $request) {
        $uploads = "";
        $achievements = new Achievement();
        $achievements->user_id = auth()->user()->id;
        $achievements->title = $request->title;
        $achievements->description = $request->description;
        if ($request->has('img')) {
            $uploadedFile = $request->file('img');
            $uploads = time() . $uploadedFile->getClientOriginalName();

            Storage::disk('local')->putFileAs(
                    'public', $uploadedFile, $uploads
            );
        }
        if ($uploads != "")
            $achievements->img = $uploads;

        $achievements->type = $request->type;

        $achievements->save();
        if ($achievements) {
            return redirect('/profile')->withFlashSuccess(
                            'Added Successfully!!'
            );
        } else {
            return ["err" => "Something went wrong"];
        }
    }

    public function edit() {
        $user = auth()->user();

        //dd($user->address);
        return view('frontend.user.edit', compact('user'));
    }

    public function view(User $user) {
        $posts = Post::with('comments')
                        ->latest()
                        ->join('categories', 'posts.category_id', '=', 'categories.category_id')
                        ->join('users', 'posts.company_id', '=', 'users.id')
                        ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location')
                        ->where('posts.company_id', $user->id)
                        ->where('posts.status', 0)
                        ->orderby('created_at', 'DESC')->get();

        $achievements = Achievement::where('user_id', $user->id)->where('active', 1)->get();
        $loggedInUser = auth()->user();

        $interactions = [];
        if ($user->id == $loggedInUser->id) {
            $interactionsPostIds = Notification::where('user_id', $user->id)->orWhere('post_user_id', $user->id)->groupBy('post_id')->orderby('created_at', 'DESC')->pluck('post_id')->toArray();

            $interactions = Post::with('comments')
                            ->latest()
                            ->leftJoin('categories', 'posts.category_id', '=', 'categories.category_id')
                            ->leftJoin('users', 'posts.company_id', '=', 'users.id')
                            ->leftJoin('address', 'posts.address_id', '=', 'address.address_id')
                            ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                            ->where('status', 0)->whereIn('posts.post_id', $interactionsPostIds)->get();
        }
        $userAddr = Address::where('address_id', $user->address_id)->get();

        return view('frontend.user.profile', compact('user', 'posts', 'achievements', 'userAddr', 'loggedInUser', 'interactions'));
    }

    public function delete_address(Request $request, $id) {
        $address = Address::find($id);
        if ($address->company_id == auth()->user()->id) {
            $address->delete();
            if ($request->ajax()) {
                return 1;
            }
            return back()->with(["msg" => "Deleted Successfully", "err" => ""]);
        } else {
            if ($request->ajax()) {
                return 0;
            }
            return back()->with(["msg" => "", "err" => "You are not authorised to delete."]);
        }
    }

    public function add_address(Request $request) {
        $model = new Address();
        $model->company_id = auth()->user()->id;
        $model->address = $request->address_location;
        $model->City = $request->city;
        $model->State = $request->state;
        $model->Pincode = $request->pincode;
        $model->lat = $request->addr_lat;
        $model->lang = $request->addr_long;

        if ($model->save()) {
            return json_encode($model);
        } else {
            return 0;
        }
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(UpdateProfileRequest $request) {

        try {
            request()->validate([
                'first_name' => 'required|min:3|max:100',
                'last_name' => 'required|min:2|max:100',
                'company_name' => 'nullable|min:0|max:100|regex:/^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]*)$/u',
                'contact' => 'required|numeric',
                'description' => 'required|min:2|max:250',
                'second_email' => 'nullable|email|unique:users',
            ]);

            $avatar_location = "";
            if ($request->has('avatar_location')) {
                $uploadedFile = $request->file('avatar_location');
                $avatar_location = time() . $uploadedFile->getClientOriginalName();
                Storage::disk('local')->putFileAs(
                        'public', $uploadedFile, $avatar_location
                );
            }

            $user = auth()->user();

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->Website = $request->website;
            $user->organization_serves = $user->organization_serves = !empty($request->organization_serves) ? implode(',', $request->organization_serves) : '';

            $user->company_name = $request->company_name;
            $user->description = $request->description;
            $user->contact = $request->contact;
            $user->second_email = $request->second_email;

            if ($avatar_location != "")
                $user->avatar_location = $avatar_location;

            $user->diversity_id = $request->diversity;
            $user->save();

            if ($user->active == 0)
                event(new EditProfileEvent($user));

            return redirect('/profile')->withFlashSuccess('Profile Updated Successfully!!');
        } catch (Exception $exc) {
            echo '<pre>';
            print_r($exc);
            die;
        }

        return back()->with(['msg' => "Profile Saved Successfully"]);
    }

}
