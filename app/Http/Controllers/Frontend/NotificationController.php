<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use View;
use App\Models\Post;

class NotificationController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $notifications = Notification::with('post', 'user', 'comment')->where('post_user_id', auth()->user()->id)->where('seen', 0)->orderBy('created_at', 'DESC')->get();

        return view('frontend.user.notification', compact('notifications'));
    }

    public function viewNotification(Request $request, $id) {

        $notification = Notification::where('id', $id)->first();

        if ($notification->type != 1) {
            $notification->seen = 1;
            $notification->update();
        }

        $post = Post::find($notification->post_id);

        if ($notification) {
            return redirect("/post-details/$post->post_id/" . str_slug($post->title) . "?comment_id=$notification->comment_id");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function markAllRead() {
        $data['seen'] = 1;
        Notification::where('post_user_id', auth()->user()->id)->update($data);

        return 1;
    }

    public function getNew() {
        $notifications = Notification::with('post', 'user', 'comment')->where('post_user_id', auth()->user()->id)->where('seen', 0)->get();

        $data = [];

        if ($notifications->count() > 0) {
            foreach ($notifications as $key => $notification) {
                $view = View::make('frontend.includes.notif', compact('notification'));
                $data['data'][$key] = $view->render();
            }

            $data['count'] = $notifications->count();
        }

        return json_encode($data);
    }

}
