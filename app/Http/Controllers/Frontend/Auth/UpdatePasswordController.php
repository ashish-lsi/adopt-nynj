<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Http\Requests\Frontend\User\UpdatePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UpdatePasswordController.
 */
class UpdatePasswordController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ChangePasswordController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdatePasswordRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(Request $request)
    {
        if($request->password != $request->confirm_password){
            return redirect()->route('frontend.user.account')->withFlashDanger(__('Confirm Password did not match. please try again.'));
    
        }
        $user = auth()->user();
        if(Hash::check($request->old_password,$user->password)){
            $user->password = Hash::make($request->password);
            $user->save();
            if($user){
                return redirect()->route('frontend.user.account')->withFlashSuccess(__('Password changed Successfully'));
    
            }
        }
        else{
            return redirect()->route('frontend.user.account')->withFlashDanger(__('Old Password Does not match'));
    
        }
        //$this->userRepository->updatePassword($request->only('old_password', 'password'));

        //return redirect()->route('frontend.user.account')->withFlashSuccess(__('strings.frontend.user.password_updated'));
    }
}
