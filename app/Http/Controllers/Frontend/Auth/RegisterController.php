<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Helpers\Frontend\Auth\Socialite;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Models\Auth\User;
use App\Models\Address;
use App\Models\CompanyType;
use App\Models\DiversityType;
use App\OrganizationServes;
use App\TempUsers;
use Illuminate\Http\Request;
use Modules\MLM\Entities\MLMConnection;

/**
 * Class RegisterController.
 */
class RegisterController extends Controller {

    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * RegisterController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectPath() {
        return route(home_route());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {
        abort_unless(config('access.registration'), 404);

        $companyTypes = CompanyType::where('enabled', 1)->get();
        $diversity_type = DiversityType::where('enabled', 1)->get();
        $organization_serves = OrganizationServes::where('active', 1)->get();

        return view('frontend.auth.register', compact('companyTypes', 'diversity_type', 'organization_serves'));

        if (isset($_GET['code']) && \Igaster\LaravelTheme\Facades\Theme::get() == 'adoptFarm') {

            $confirm_code = $_GET['code'];
            $user = User::where('confirmation_code', $confirm_code)->first();

            if (!is_null($user)) {
                if ($user->confirmed == 0) {
                    $userAddr = Address::where('address_id', $user->address_id)->first();

                    return view('frontend.auth.register', compact('companyTypes', 'user', 'userAddr'))
                                    ->withSocialiteLinks((new Socialite)->getSocialLinks());
                } else {
                    return redirect('/login')->withFlashSuccess(
                                    'Your account is already registerd. Please login to access your account.'
                    );
                }
            } else {
                die('Invalid vendor');
            }
        } elseif (\Igaster\LaravelTheme\Facades\Theme::get() == 'empowerTheme') {

            return view('frontend.auth.register', compact('companyTypes'))->withSocialiteLinks((new Socialite)->getSocialLinks());
        }
    }

    public function insertData(Request $request) {
        $register = TempUsers::all();
        foreach ($register as $data) {
            $request->merge([
                'first_name' => $data->first_name,
                'vendor_id' => $data->vendor_id,
                'last_name' => $data->last_name,
                'company_name' => $data->company,
                'category' => 0,
                'company_type' => 1,
                'email' => $data->email_id,
                'password' => '123456',
                'address' => $data->address,
                'State' => $data->state,
                'City' => $data->city,
                'Pincode' => $data->zipcode,
                'addr_lat' => '00',
                'addr_long' => '0'
                    ]
            );
            $this->register($request);
            // $user = $this->userRepository->create(
            // );
        }
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function register(\Illuminate\Http\Request $request) {

        abort_unless(config('access.registration'), 404);

        request()->validate([
            'first_name' => 'required|min:3|max:100',
            'last_name' => 'required|min:2|max:100',
            'company_name' => 'nullable|min:0|max:100|regex:/^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]*)$/u',
            'email' => 'required|email|unique:users',
            'phone' => 'numeric',
            'company_type' => 'required',
            'password' => 'required|min:6|max:20',
            'password_confirmation' => 'required|min:6|max:20|same:password',
            'address' => 'required|min:3|max:150',
            'State' => 'required|min:3|max:50',
            'City' => 'required|min:3|max:50',
            'Pincode' => 'required|numeric',
            'tnc' => 'required',
            'g-recaptcha-response' => 'required',
        ]);

        if (isset($_POST['g-recaptcha-response'])) {
            $captcha = $_POST['g-recaptcha-response'];
        }
        
        if (!$captcha) {
            return back()->withInput($request->input())->withErrors(['Please check the captcha!!']);
        }

        $user = $this->userRepository->create($request->only('company_name', 'vendor_id', 'no_vendor_id', 'description', 'company_type', 'diversity_type', 'category', 'organization_serves', 'website', 'first_name', 'last_name', 'email', 'phone', 'password'));

        if ($user) {
            if ($request->has('ie') && $request->get('ie') != '') {
                $requested_email = base64_decode(str_rot13($request->get('ie')));
                $requested_user = User::where('email', $requested_email)->first();
                if ($requested_email) {
                    $mlmConnections = new MLMConnection();
                    $mlmConnections->user_id = $requested_user->id;
                    $mlmConnections->child_id = $user->id;
                    $mlmConnections->save();
                }
            }

            $addressData = [
                'company_id' => $user->id,
                'address' => $request->address,
                'State' => $request->State,
                'City' => $request->City,
                'Pincode' => $request->Pincode,
                'lat' => $request->addr_lat,
                'lang' => $request->addr_long
            ];

            $adddata = new Address($addressData);
            $adddata->save();

            if ($adddata) {
                $share = User::find($user->id);
                $share->address_id = $adddata->address_id;
                $share->save();
            }
        }
     

        // If the user must confirm their email or their account requires approval,
        // create the account but don't log them in.
        if (config('access.users.confirm_email') || config('access.users.requires_approval')) {
            event(new UserRegistered($user));
            
            return redirect('/login')->withFlashSuccess(
                            config('access.users.requires_approval') ?
                            __('exceptions.frontend.auth.confirmation.created_pending') :
                            __('exceptions.frontend.auth.confirmation.created_confirm')
            );
        } else {
            event(new UserRegistered($user));
        return redirect('/login')->withFlashSuccess(__("Thank you! Your registration is pending the council's approval. Please allow 24 hours for the same."));
        }
    }

    /**
     * @author Ashish Sharma
     * @description This function is used to save the data of auto register process
     * 
     * @param \Illuminate\Http\Request $request
     * @return type
     */
    public function registerAuto(\Illuminate\Http\Request $request) {

        abort_unless(config('access.registration'), 404);

        if ($request->post('confirm_code')) {
            $confirm_code = $request->post('confirm_code');

            //Save the user table data
            $user = User::where('confirmation_code', $confirm_code)->first();
            $user->password = $request->post('password');
            $user->confirmed = 1;
            $user->save();

            //Save the user address data
            $address = Address::where('address_id', $user->address_id)->first();
            $address->lat = $request->post('addr_lat');
            $address->lang = $request->post('addr_long');
            $address->save();

            return redirect('/login')->withFlashSuccess(
                            'Thank you for joining us. Please login to access your account.'
            );
        }
    }

}
