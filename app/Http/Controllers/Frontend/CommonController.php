<?php

namespace App\Http\Controllers\Frontend;

use App\Events\ApprovePostEvent;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Mail\PostReportMail;
use Illuminate\Http\Request;
use App\Models\DiversityDocument;
use App\Models\State;
use App\Models\City;
use App\Models\WeatherCity;
use App\Models\Address;
use App\Models\Achievement;
use App\Models\Comment;
use App\Models\Post as AppPost;
use App\Models\UserPostApply;
use App\Models\WordpressPost;
use Corcel\Model\Post;
use Illuminate\Support\Facades\DB;
use Mail;

class CommonController extends Controller {

    //
    public function getDiversityDocument(Request $request) {
        $diversity_documents = DiversityDocument::where('diversity_id', $request->diversity)->get();
        return $diversity_documents;
        //dd($diversity_documents);
    }

    public function reportPost(Request $request) {
        $reportText = $request->reported;
        $post = AppPost::where('post_id',$request->post_id)->first();
        $reportMail = new PostReportMail($post,auth()->user(),$reportText);
        Mail::to(config('mail.from.admin'))->send($reportMail);

        return back()->with(["msg"=>"Reported Successfully"]);
        
        // dd($reportText);
    }
    public function approvePost(Request $request) {
        $token_data = Helper::decodeApproveToken($request->token);
        //dd($user_apply_id);
        $request->merge(
                [
                    'post_apply_id' => $token_data[0],
                    'is_reply_to_id' => $token_data[1],
                    'company_id' => $token_data[2],
        ]);
        $user_post_apply = UserPostApply::find($request->post_apply_id);
        $user_post_apply->approved = 1;
        $user_post_apply->save();
        // Inserting default approval comment.
        $comments = new Comment();
        $comments->reply_user_id = $user_post_apply->user_id;
        $comments->comment = "Your Request Approved.";
        $comments->post_id = $user_post_apply->post->post_id;
        $comments->is_reply_to_id = $request->is_reply_to_id;
        $comments->company_id = $request->company_id;
        $comments->save();

        event(new ApprovePostEvent($comments));

        // $user_post_apply = UserPostApply::find($user_apply_id);
        // $user_post_apply->approved = 1;
        // $user_post_apply->save();
        return redirect('/login')->withFlashSuccess('Request Approved Successfully, Please Login to see details.');
    }

    public function change_password(Request $request) {
        if ($request->password != $request->confirm_password) {
            return back()->with(["err" => "Confirm Password not matching with password"]);
        }
    }

    public function wpPostDetails(Request $request, $slug, $post_id) {

        $post = WordpressPost::find($post_id);
        $related_posts = [];
        if (isset($post->type)) {
            $related_posts = WordpressPost::where('type', $post->type)->orderby('created_at', 'desc')->take(20)->get();
        }
        return view('frontend.wp.postdetails', compact('post', 'related_posts'));
    }

    public function getState(Request $request) {
        $result = State::where('state', 'LIKE', '%' . $request->get('q') . '%')->get();
        $suggestions = [];
        foreach ($result as $data) {
            array_push($suggestions, ['id' => $data->state, 'text' => $data->state]);
        }
        return json_encode(['results' => $suggestions]);
    }

    public function getCity(Request $request) {
        $result = City::where('city', 'LIKE', '%' . $request->get('q') . '%')->get();
        $suggestions = [];
        foreach ($result as $data) {
            array_push($suggestions, ['id' => $data->city, 'text' => $data->city]);
        }
        return json_encode(['results' => $suggestions]);
    }

    public function viewAwards($id) {
        $news = Achievement::find($id);
        return view('frontend.viewAwards', compact('news'));
    }

    public function socialPostDetailsOld($post_id) {
        $post_requests = [];
        $post = AppPost::with('comments')
                ->with('user_post_apply')
                // ->join('users', 'posts.company_id', '=', 'users.id')
                // ->join('address', 'posts.address_id', '=', 'address.address_id')
                ->where('posts.post_id', $post_id)
                ->first();


        if (auth()->user()) {
            $questions = DB::table("question")
                    ->leftjoin('question_options', 'question.question_id', '=', 'question_options.question_id')
                    ->where('question.post_id', $post_id)
                    ->get();

            $already_applied = UserPostApply::where(['post_id' => $post_id, 'user_id' => auth()->user()->id])->get();

            $approved = 0;
            if ($post->company_id == auth()->user()->id)
                $post_requests = UserPostApply::with('user', 'post')->where('post_id', $post_id)->get();

            if ($post->company_id != auth()->user()->id) {
                $post_user_apply = UserPostApply::where(['post_id' => $post->post_id, 'user_id' => auth()->user()->id])->first();
                if ($post_user_apply)
                    $approved = $post_user_apply->approved;
            }
            // dd($post_request);

            $questionsList = [];
            foreach ($questions as $value) {
                $questionsList[$value->question_id]['title'] = $value->title;
                $questionsList[$value->question_id]['option_type'] = $value->option_type;
                $questionsList[$value->question_id]['enabled'] = $value->enabled;

                if ($value->option_type != '1') {
                    if (isset($questionsList[$value->question_id]['answers'])) {
                        $questionsList[$value->question_id]['answers'] = $questionsList[$value->question_id]['answers'] . ',' . $value->lable;
                    } else {
                        $questionsList[$value->question_id]['answers'] = $value->lable;
                    }
                } else {
                    $questionsList[$value->question_id]['answers'] = '';
                }
            }

            return view('frontend.user.postDetailsOpen', compact('post', 'questionsList', 'post_requests', 'approved', 'already_applied'));
        } else {
            return view('frontend.user.postDetailsOpen', compact('post'));
        }
    }

    public function socialPostDetails($post_id) {
        $post_requests = [];
        $post = AppPost::with('comments')
                ->with('user_post_apply')
                // ->join('users', 'posts.company_id', '=', 'users.id')
                // ->join('address', 'posts.address_id', '=', 'address.address_id')
                ->where('posts.post_id', $post_id)
                ->first();

        return view('frontend.user.postDetailsOpen', compact('post'));
    }

    public function about(Request $request) {
        //$post = Post::where('post_name', 'about-us')->first();

        return view('frontend.wp.about');
    }

    public function getUserAddress(Request $request) {
        $address = Address::where('address_id', $request->address_id)->get();
        return $address ?? [];
    }

    public function getAddress(Request $request) {
        $query = $request->get('term');
        $result = Address::where('address', 'LIKE', "%$query%")->offset(0)->limit(10)->get();
        $suggestions = [];
        foreach ($result as $data) {
            array_push($suggestions, ['value' => $data->address, 'City' => $data->City, 'State' => $data->State, 'Pincode' => $data->Pincode]);
        }
        return json_encode($suggestions);
    }

    public function getWeatherCity(Request $request) {
        $query = $request->get('term');
        $result = WeatherCity::where('name', 'LIKE', "%$query%")->offset(0)->limit(10)->get();
        $suggestions = [];
        foreach ($result as $data) {
            array_push($suggestions, ['id' => $data->id, 'label' => $data->name, 'value' => $data->name]);
        }
        return json_encode($suggestions);
    }

    public function getWeatherDetails($data) {
        
    }

    public function announcements() {
        $related_posts = [];
        $post = WordpressPost::where('type', '2')->orderby('created_at', 'desc')->first();
        if ($post)
            $related_posts = WordpressPost::where('type', $post->type)->orderby('created_at', 'desc')->take(20)->get();
        //dd($related_posts);
        return view('frontend.wp.postdetails', compact('post', 'related_posts'));
    }

    public function alerts() {
        $related_posts = [];
        $post = WordpressPost::where('type', '1')->orderby('created_at', 'desc')->first();
        if ($post)
            $related_posts = WordpressPost::where('type', $post->type)->orderby('created_at', 'desc')->take(20)->get();
        //dd($related_posts);
        return view('frontend.wp.postdetails', compact('post', 'related_posts'));
    }

    public function success_stories() {
        $related_posts = [];
        $post = WordpressPost::where('type', '3')->orderby('created_at', 'desc')->first();
        if ($post)
            $related_posts = WordpressPost::where('type', $post->type)->orderby('created_at', 'desc')->take(20)->get();
        //dd($related_posts);
        return view('frontend.wp.postdetails', compact('post', 'related_posts'));
    }

    public function privacy() {
        return view('frontend.privacy');
    }

    public function terms() {
        return view('frontend.terms');
    }

    public function how_to_use() {
        return view('frontend.how_to_use');
    }

    public function readLogs() {
        $date = isset($_GET['back']) ? date('Y-m-d', strtotime("-1 days")) : date('Y-m-d');
        $file = storage_path('logs/laravel-' . $date . '.log');

        $searchfor = '[' . $date;

        // the following line prevents the browser from parsing this as HTML.
        header('Content-Type: text/plain');

        // get the file contents, assuming the file to be readable (and exist)
        $contents = file_get_contents($file);

        // escape special characters in the query
        $pattern = preg_quote($searchfor, '/');

        // finalise the regular expression, matching the whole line
        $pattern = "/^.*$pattern.*\$/m";

        // search, and store all matching occurences in $matches
        if (preg_match_all($pattern, $contents, $matches)) {
            echo "Found matches: \n\n";
            echo implode("\n\n", $matches[0]);
            echo "\n\n";
        } else {
            echo "No matches found";
        }
    }
}
