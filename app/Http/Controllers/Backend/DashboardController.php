<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\Address;
use App\Models\Post;
use App\Exports\HelpSeekerExport;
use App\Models\Auth\User;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{

        /**
         * @return \Illuminate\View\View
         */
        public function index()
        {

                $counts = DB::table('posts')
                        ->select(DB::raw('SUM(type = 0) AS help_seeker, SUM(type = 1) AS help_giver, SUM(status = 0) AS open_request, SUM(status = 1) AS close_request'))
                        ->where('enabled',1)
                        ->get();

                $seekerChart = DB::table("posts")
                        ->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw("(COUNT(*)) as total"))
                        ->where('type', 0)
                        ->where('enabled',1)
                        ->orderBy('created_at')
                        ->groupBy(DB::raw("MONTHNAME(created_at)"))
                        ->get();

                $giverChart = DB::table("posts")
                        ->select(DB::raw("MONTHNAME(created_at) as month"), DB::raw("(COUNT(*)) as total"))
                        ->where('type', 1)
                        ->where('enabled',1)
                        ->orderBy('created_at')
                        ->groupBy(DB::raw("MONTHNAME(created_at)"))
                        ->get();


                $categoryChart = DB::table("posts")
                        ->join('categories', 'posts.category_id', '=', 'categories.category_id')
                        ->select('categories.name as cname', 'type', DB::raw("(COUNT(posts.post_id)) as total"))
                        ->where('posts.enabled',1)
                        ->groupBy('categories.name', 'type')
                        ->get();




                $categories =  array_column($categoryChart->toArray(), 'cname');
                $uniqueCat = array_unique($categories);

                $helpSeekerPost = $categoryChart->where('type', 0)->toArray();

                $helpGiverPost = $categoryChart->where('type', 1)->toArray();
                $helpSeekerArray = [];
                $helpGiverArray = [];
                foreach ($uniqueCat as $cat) {
                        $foundSeeker = array_filter($helpSeekerPost, function ($value) use ($cat) {
                                return $value->cname == $cat;
                        });
                        $foundSeeker = array_values($foundSeeker);
                        if (!empty($foundSeeker)) {
                                $helpSeekerArray[$cat] = $foundSeeker[0]->total;
                        } else {
                                $helpSeekerArray[$cat] = 0;
                        }

                        $foundGiver = array_filter($helpGiverPost, function ($value) use ($cat) {
                                return $value->cname == $cat;
                        });
                        $foundGiver = array_values($foundGiver);
                        if (!empty($foundGiver)) {
                                $helpGiverArray[$cat] = $foundGiver[0]->total;
                        } else {
                                $helpGiverArray[$cat] = 0;
                        }
                }

               // dd($helpSeekerArray);

                $sumCategoryChart = $categoryChart->groupBy('cname')->map(function ($row) {
                        return $row->sum('total');
                });
                $resultData = [];
                foreach ($sumCategoryChart as $key => $value) {
                        $resultData["cname"][] = $key;
                        $resultData["total"][] = $value;
                }

                $categoryChart = $resultData;

                //dd(array_column($helpSeekerPost,'cname'));
                $users = User::latest()->take(5)->get();

                //Setting the authencticated user cookie for newsletter
                $user = auth()->user();
                setcookie("tti_user", $user->id, 0, "/");
                //Setting the authencticated user cookie for newsletter

                return view('backend.dashboard', compact('counts', 'helpSeekerArray', 'helpGiverArray', 'seekerChart', 'giverChart', 'categoryChart', 'users'));
        }

        public function downloadChartData(Request $request)
        {

                $chartdata = Post::latest()
                        ->leftjoin('categories', 'posts.category_id', '=', 'categories.category_id')
                        ->leftjoin('users', 'posts.company_id', '=', 'users.id')
                        ->leftjoin('address', 'posts.address_id', '=', 'address.address_id')
                        ->select('posts.*', 'categories.name as categoryName', 'users.company_name', 'users.avatar_location', 'address.address', 'address.City', 'address.State', 'address.Pincode', 'address.phone1')
                        ->get();

                $type = 'xls';
                return Excel::download(new HelpSeekerExport($chartdata), 'HelpChart_Dashboard.' . $type);
        }
}
