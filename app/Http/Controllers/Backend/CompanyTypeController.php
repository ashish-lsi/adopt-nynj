<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CompanyType;
use App\Exports\CompanyTypeExport;
use Excel;

class CompanyTypeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $results = CompanyType::latest()->paginate(25);
        if($request->get('action_type') == 'export'){
            $type = 'xls';
        return Excel::download(new CompanyTypeExport($results), 'CompanyType.' . $type);
        }else
        return view('backend.companyType.index', compact('results'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('backend.companyType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required|max:255',
            'enabled' => 'required',
        ]);

        CompanyType::create($validatedData);
        return redirect()->route('admin.companyType.index')->with('success', 'Record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function show($companyType) {
        $result = CompanyType::find($companyType);
        return view('backend.companyType.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function edit($companyType) {
        $result = CompanyType::findOrFail($companyType);
        return view('backend.companyType.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required|max:255',
            'enabled' => 'required',
        ]);

        CompanyType::where('company_type_id', $id)->update($validatedData);
        return redirect()->route('admin.companyType.index')->with('success', 'Record updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyType  $companyType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            CompanyType::where('company_type_id', $id)->delete();
            return redirect()->route('admin.companyType.index')->with('success', 'Record deleted successfully.');
        } catch (Exception $exc) {
            echo $exc->getMessage();
            die;
        }
    }

}
