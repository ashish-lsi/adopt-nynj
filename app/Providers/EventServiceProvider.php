<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider.
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        //
        'App\Events\PostApply'=>['App\Listeners\NotificationListener'],
        'App\Events\PostCommentEvent'=>['App\Listeners\CommentNotificationListener'],
        'App\Events\EditProfileEvent'=>['App\Listeners\EditProfileEventListener'],
        'App\Events\ClosePostEvent'=>['App\Listeners\ClosePostListener'],
        'App\Events\ApprovePostEvent'=>['App\Listeners\ApprovePostListner'],


    ];

    /**
     * Class event subscribers.
     *
     * @var array
     */
    protected $subscribe = [
        /*
         * Frontend Subscribers
         */

        /*
         * Auth Subscribers
         */
        \App\Listeners\Frontend\Auth\UserEventListener::class,

        /*
         * Backend Subscribers
         */

        /*
         * Auth Subscribers
         */
        \App\Listeners\Backend\Auth\User\UserEventListener::class,
        \App\Listeners\Backend\Auth\Role\RoleEventListener::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
