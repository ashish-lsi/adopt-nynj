<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CategoryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public $data ;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function collection()
    {
        $seekerArray = [];
       
        foreach($this->data as $d){
            $seekerArray[] = array(
            'Category Name' => $d->name,
            'Description' => $d->name_clean,
            'Enabled' => $d->enabled == 1 ? 'Yes' : 'No',              
            'Created At' => $d->created_at                
            );
        }
        return collect($seekerArray);
        //$this->data = $data;
        //
    }
    public function headings(): array {
        return [
            'Category Name',
            'Description',
            'Enabled',            
            'Created At'           
        ];
    }
}
