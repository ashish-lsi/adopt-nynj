<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CloseRequestExport implements FromCollection, WithHeadings {

    /**
     * @return \Illuminate\Support\Collection
     */
    use Exportable;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    public function collection() {
        $seekerArray = [];

        foreach ($this->data as $k => $d) {

            if (isset($d->help_details['data'])) {
                foreach ($d->help_details['data'] as $value) {
                    $seekerArray[] = array(
                        'Type' => $d->type == 0 ? 'Help Seeker' : 'Help Giver',
                        'Category' => $d->categoryName,
                        'Name' => $d->company_name,
                        'Title' => $d->title,
                        'Description' => $d->article,
                        'Quantity' => $d->qnty,
                        'DonateTo' => $value['company_name'],
                        'DonateQty' => $value['qty'],
                        'Location' => $d->location,
                        'Open Since' => $d->created_at,
                    );
                }
            } else {
                $seekerArray[] = array(
                    'Type' => $d->type == 0 ? 'Help Seeker' : 'Help Giver',
                    'Category' => $d->categoryName,
                    'Name' => $d->company_name,
                    'Title' => $d->title,
                    'Description' => $d->article,
                    'Quantity' => $d->qnty,
                    'DonateTo' => '--',
                    'DonateQty' => '--',
                    'Location' => $d->location,
                    'Open Since' => $d->created_at,
                );
            }
        }

        return collect($seekerArray);
    }

    public function headings(): array {
        return [
            'Post Type',
            'Category',
            'Company Name',
            'Post Title',
            'Description',
            'Total Quantity',
            'Donated to/by',
            'Donated to/by qty',
            'Location',
            'Created on',
        ];
    }

}
