<?php

namespace App\Models;

use Models\Auth\User;
use App\Models\DiversityDocument;

use Illuminate\Database\Eloquent\Model;

class DiversityType extends Model {

    protected $table = 'diversity_type';
    protected $primaryKey = "id";
    protected $fillable = ['name', 'description', 'enabled', 'created_at', 'updated_at'];

    public function diversity_documents() {
        return $this->hasMany(DiversityDocument::class, 'diversity_id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'diversity_id', 'id');
    }

}
