<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionOptions extends Model {

    protected $table = "question_options";
    protected $primaryKey = "option_id";
    protected $fillable = ['name', 'question_id', 'lable', 'value', 'created_at', 'updated_at'];

}
