<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiversityDocument extends Model {

    //
    protected $table = "diversity_document";
    protected $primaryKey = "id";
    protected $fillable = ['diversity_id', 'title', 'description', 'optional', 'enabled', 'created_at', 'updated_at'];

}
