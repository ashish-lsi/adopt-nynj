<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model {

    //
    protected $table = "company_type";
    protected $primaryKey = "company_type_id";
    protected $fillable = ['company_type_id', 'name', 'description', 'enabled', 'created_at', 'updated_at'];

    public function user() {
        return $this->belongsTo(User::class, 'company_type', 'company_type_id');
    }
}
