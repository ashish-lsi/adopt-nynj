<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationServes extends Model {

    //
    protected $table = "organization_serves";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'description', 'active', 'created_at', 'updated_at'];

}
