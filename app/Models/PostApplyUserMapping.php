<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostApplyUserMapping extends Model {

    protected $table = "post_apply_user_mapping";
}
