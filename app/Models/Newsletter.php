<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model {

    protected $table = 'newsletter';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'name', 'html', 'preview_file', 'created_at', 'updated_at'];
}
