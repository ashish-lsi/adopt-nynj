<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuccessStories extends Model {

    protected $table = 'sucess_stories';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'title', 'body', 'attachment', 'enabled', 'created_at', 'updated_at'];

}
