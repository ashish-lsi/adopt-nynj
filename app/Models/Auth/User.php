<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\SendUserPasswordReset;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use App\Models\Post;
use App\Models\Address;
use App\Models\Category;

/**
 * Class User.
 */
class User extends Authenticatable {

    use HasRoles,
        Notifiable,
        SendUserPasswordReset,
        SoftDeletes,
        UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'diversity_id',
        'company_type',
        'category_id',
        'address_id',
        'vendor_id',
        'no_vendor_id',
        'contact',
        'description',
        'organization_serves',
        
        'cover_page',
        'employees_no',
        'Website',
        'registration_number',
        'commensment_date',
        'ownership_type',
        'remark',
        'first_name',
        'last_name',
        'email',
        'avatar_type',
        'avatar_location',
        'password',
        'password_changed_at',
        'active',
        'share_contact',
        'confirmation_code',
        'confirmed',
        'timezone',
        'last_login_at',
        'last_login_ip',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['last_login_at', 'deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'confirmed' => 'boolean',
    ];

    public function users() {
        return $this->belongsToMany(User::class, 'mlm_connections', 'user_id', 'child_id');
    }

    public function posts() {
        return $this->hasMany(Post::class, 'company_id');
    }

    public function addresses() {
        return $this->hasMany(Address::class, 'company_id');
    }

    public function address() {
        return $this->hasOne(Address::class, 'company_id');
    }

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    protected $with = ['diversityType', 'companyType'];

}
