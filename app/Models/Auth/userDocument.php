<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\DiversityDocument;

class userDocument extends Model {

    //
    protected $table = "user_document";

    public function diversity_document() {
        return $this->belongsTo(DiversityDocument::class);
    }

}
