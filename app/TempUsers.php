<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempUsers extends Model
{
    //
    public $timestamps = false;
    protected $table = 'tempusers';
}
