<?php

use App\Http\Controllers\Frontend\Auth\RegisterController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\PostController;
use App\Http\Controllers\Frontend\NotificationController;
use App\Http\Controllers\Frontend\seekerAnalysisController;
use App\Http\Controllers\Frontend\CommentController;
use App\Models\Comment;
use App\Notification;
use App\Http\Controllers\Frontend\SearchController;
use App\Http\Controllers\Frontend\CommonController;
use App\Http\Controllers\Frontend\Auth\UpdatePasswordController;
use App\Http\Controllers\Frontend\CompanyController;
use App\Http\Controllers\Frontend\User\NewDashboardController;
use App\Models\Auth\User;
use App\Models\OrganizationServes;
use App\Post;
use App\UserOrgServe;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/how_to_use', [CommonController::class, 'how_to_use'])->name('how_to_use');
Route::get('/about', [CommonController::class, 'about'])->name('about');
Route::get('/privacy', [CommonController::class, 'privacy'])->name('privacy');
Route::get('/terms', [CommonController::class, 'terms'])->name('terms');
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/user/approve/{token}',[HomeController::class,'approveUser'])->name('approve.user');

Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');
Route::get('/suggestPost', [PostController::class, 'suggestPost'])->name('suggestPost');

Route::get('/mail', function () {
    $notification = Notification::find(31);
    //dd($comment->user->email);
    return view('frontend.mail.postComment', ['notification' => $notification]);
});
Route::post('/post/report',[CommonController::class,'reportPost'])->name('report');
Route::get('/approve',[CommonController::class, 'approvePost'])->name('approve_post');
Route::get('/awards/{id}', [CommonController::class, 'viewAwards'])->name('view_awards');
Route::get('/post/{post_id}/{slug}', [CommonController::class, 'socialPostDetails'])->name('social_post_details');
Route::get('/wp-post/{slug}/{post_id}', [CommonController::class, 'wpPostDetails'])->name('wp-post-details');
Route::get('getState', [CommonController::class, 'getState'])->name('get_state');
Route::get('getCity', [CommonController::class, 'getCity'])->name('get_city');

Route::get('/getUserAddress', [CommonController::class, 'getUserAddress'])->name('get_user_address');

Route::get('getAddress', [CommonController::class, 'getAddress'])->name('get_address');
Route::get('/announcements', [CommonController::class, 'announcements'])->name('announcements');
Route::get('/alerts', [CommonController::class, 'alerts'])->name('alerts');
Route::get('/alerts-view/{id}', [HomeController::class, 'alertsView'])->name('alertsView');

Route::get('/success-stories', [CommonController::class, 'success_stories'])->name('success-stories');

Route::get('getWeatherCity', [CommonController::class, 'getWeatherCity'])->name('get_weather_city');
Route::post('dashboard/showWeather', [DashboardController::class, 'ShowWeather'])->name('show_weather');

Route::get('/privacy-policy', [DashboardController::class, 'PrivacyPolicy'])->name('privacy_policy');
Route::post('/subscribe-us', [DashboardController::class, 'subscribe_news'])->name('subscribe_news');

Route::get('read-logs', [CommonController::class, 'readLogs'])->name('readLogs');
Route::post('get-mention-users', [PostController::class, 'getMentionUsers'])->name('getMentionUsers');
/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */

Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */


        Route::get('account', [AccountController::class, 'index'])->name('account');
        Route::get('profile', [ProfileController::class, 'index'])->name('profile');
        Route::post('profile/update', [ProfileController::class, 'update'])->name('profile.update');
        Route::post('profile/addAddress', [ProfileController::class, 'add_address'])->name('profile.addAddr');
        Route::get('/getDiversityDocument', [CommonController::class, 'getDiversityDocument'])->name('getDiversityDocument');

        Route::group(['middleware' => ['active_user']], function () {

            Route::post('/fetchComments',[CommentController::class,'fetchComments'])->name('fetchComments');
            Route::get('/insert',[RegisterController::class,'insertData']);

            Route::get('/posts',[NewDashboardController::class,'dashboard'])->name('home');
            Route::get('post-details/{post_id}/{post_title}', [PostController::class, 'postDetails'])->name('post-details');
            Route::get('edit-post/{post}', [PostController::class, 'edit'])->name('edit-post');
            Route::post('editPost', [PostController::class, 'editPost'])->name('editPost');
            Route::get('download/{file_name}', [PostController::class, 'download'])->name('download');
            Route::get('file-download/{file_name}', [PostController::class, 'file_download'])->name('file_download');

            Route::post('applyPost', [PostController::class, 'applyPost'])->name('applyPost');
            Route::post('closePostInd',[PostController::class, 'closePostInd'])->name('closePostInd');
            Route::post('closePost', [PostController::class, 'closePost'])->name('closePost');
            Route::post('post/destroy', [PostController::class, 'destroy'])->name('deletePost');
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::post('dashboard', [DashboardController::class, 'store'])->name('dashboard');
            Route::post('dashboard-new', [DashboardController::class, 'storeNew'])->name('dashboard_new');
            Route::get('viewNotification/{id}', [NotificationController::class, 'viewNotification'])->name('viewNotification');
            Route::get('notification/getNew', [NotificationController::class, 'getNew'])->name('notification.getNew');

            Route::get('profile/{user}', [ProfileController::class, 'view'])->name('view-profile');
            Route::get('search', [SearchController::class, 'search'])->name('search');
            Route::post('approve', [PostController::class, 'approve'])->name('approve');
            /*
             * User Account Specific
             */
            // Route::get('account', [AccountController::class, 'index'])->name('account');
            //Route::post('')

            /*
             * User Profile Specific
             */
            Route::post('/change-password', [UpdatePasswordController::class, 'update'])->name('change-password');
            //Route::get('profile', [ProfileController::class, 'index'])->name('profile');
            Route::get('profile/{id}/edit', [ProfileController::class, 'edit'])->name('profile.edit');


            Route::post('/achievements', [ProfileController::class, 'add_achievement'])->name('achievements');
            Route::get('/delete-achievement/{id}', [ProfileController::class, 'delete_achievement'])->name('delete-achievement');
            Route::get('/company', [CompanyController::class, 'index'])->name('findCompany');
            /*
             * User Post Specific
             */
            Route::get('address/delete/{id}', [ProfileController::class, 'delete_address'])->name('address.delete');
           // Route::get('posts', [PostController::class, 'index'])->name('posts');
            // Route::get('posts/create', [PostController::class, 'create'])->name('posts.create');
            // Route::get('posts/{post}', [PostController::class, 'show'])->name('posts');
            // Route::post('posts', [PostController::class, 'store'])->name('posts');
            // Route::get('posts/{post}/edit', [PostController::class, 'edit'])->name('posts.edit');
            // Route::patch('posts/{post}', [PostController::class, 'update'])->name('posts');
            // Route::delete('posts/{post}', [PostController::class, 'destroy'])->name('posts');
            //Route::post('comment', 'CommentController@store')->name('comment');
            Route::post('comment', [CommentController::class, 'store'])->name('comment');



            Route::get('seekerAnalysis', [seekerAnalysisController::class, 'index'])->name('seekerAnalysis');



            /*
             * User Notification Specific
             */
            Route::get('notification', [NotificationController::class, 'index'])->name('notification');
            Route::get('notification/markAllRead', [NotificationController::class, 'markAllRead'])->name('markAllRead');
        });
    });
});
