//----------------------------Clear all notifications----------------------------
$('.mark-read').click(function (e) {
    $.ajax({
        url: '/notification/markAllRead',
        success: function (data) {
            console.log('all notifications cleared.');

            var html = '<div class="notfication-details"><div class="notification-info"><h3>Currently there are no new notifications in your account.</h3></div></div>';
            $('.notification-container').html(html);
        },
    });
});

$(document).on('ajaxStart submit', function (e) {
    $(".loader-container").show();
});

$(document).on('ajaxStop', function (e) {
    $(".loader-container").hide();
});

$('#frmCreatePost').submit(function (e) {
    var country = $('#addr-country').val();
    var addr_lat = $('#addr-lat').val();

    if (country != 'United States') {
        alert('Only United States locations are allowed to register!!');
        $('#address').focus();
        return false;
    }

    if (addr_lat == '') {
        alert('Please select a valid location!!');
        $('#address').focus();
        return false;
    }

    $("#btnSubmit").attr("disabled", true);
});

function getNotifications() {
    $.ajax({
        url: notif_url,
        global: false,
        success: function (data) {
            var notifdata = JSON.parse(data);

            if (notifdata.count !== undefined) {
                //Show notification count
                $('#notificaction_count').html(notifdata.count);

                //Show notification count
                var html = ""
                $.each(notifdata.data, function (key, valueObj) {
                    html += valueObj;
                });
                $('.notification-container').html(html);
            }
        }
    });
}

setInterval(
        function () {
            if (localStorage.getItem("dashboard_popup") === true) {
                getNotifications();
            }
        },
        90000);

function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

$("#search_d").autocomplete({
    source: "/suggestPost",
    select: function (suggestPost, ui) {
        event.preventDefault();
        $("#search_d").val(ui.item.label);
    }
});

$("#place_title").autocomplete({
    source: "/suggestPost",
    select: function (suggestPost, ui) {
        //console.log(typeof ui);
        event.preventDefault();
        $("#place_title").val(ui.item.label);
        //$("#article").append(ui.item.article);
        $("#category_id").val(ui.item.category_id);
    }
});
