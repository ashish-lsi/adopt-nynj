
<div id="mailsub" class="notification" align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


                <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                    <tr><td>
                            <!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"> </div>
                        </td></tr>
                    <!--header -->
                    <tr><td align="center" bgcolor="#007BC1">

                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="left">
                                        <div class="mob_center_bl" style="float: left; display: inline-block; width:100%;">
                                            <table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;">
                                                <tr><td align="center" valign="middle">
                                                        <!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
                                                        <table width="115" border="0" cellspacing="0" cellpadding="0" >
                                                            <tr><td align="left" valign="top" class="mob_center">
                                                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                        <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                                        <img src="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" width="150" height="70" alt="adopt logo" border="0" style="display: block;" /></font></a>
                                                                </td></tr>
                                                        </table>						
                                                    </td></tr>
                                            </table></div>
                                    </td>
                                </tr>
                            </table>
                            <!-- padding --><div style="height: 30px; line-height: 50px; font-size: 10px;"> </div>
                        </td></tr>
                    <!--header END-->

                    <!--content 1 -->
                    <tr><td align="center" bgcolor="#fbfcfd">
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
                                        <div style="line-height: 44px;">
                                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                                You Got New Notification
                                            </span></font>
                                        </div>
                                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                    </td></tr>
                                <tr><td align="center">
                                        <div style="line-height: 24px;">
                                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
                                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
                                                <strong> {{$notification->user->company_name}} </strong>  <br> also commented on his post.
                                            </span></font>  
                                        </div>
                                        <!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
                                    </td></tr>

                            </table>
                            <table style="border-radius:4px;border-collapse:collapse;word-wrap:break-word;min-height:32px;width:inherit;background-color:#007BC1;text-align:center">
                                <tbody><tr style="border-collapse:collapse">
                                        <td height="32" width="12" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">&nbsp;</td>
                                        <td height="32" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">
                                            <a href="{{route('frontend.user.post-details',['post_id'=>$notification->post_id])}}" style="color:#fff;font-size:18px;line-height:42px;text-decoration:none" target="_blank" >
                                                <strong>View Comments</strong>
                                            </a>
                                        </td>
                                        <td height="32" width="12" style="font-size:14px;font-family:Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;color:#353a3d;border-collapse:collapse;font-weight:400;word-wrap:break-word;line-height:1.4">&nbsp;</td>
                                    </tr>
                                </tbody></table>


                        </td></tr>
                    <!--content 1 END-->




                    <!--footer -->
                    <tr><td class="iage_footer" align="center" bgcolor="#ffffff">
                            <!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>	

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                            2019 © {{app_name()}}. ALL Rights Reserved.
                                        </span></font>				
                                    </td></tr>			
                            </table>

                            <!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>	
                        </td></tr>
                    <!--footer END-->
                    <tr><td>
                            <!-- padding --><div style="height: 50px; line-height: 80px; font-size: 10px;"> </div>
                        </td></tr>
                </table>
                <!--[if gte mso 10]>
                </td></tr>
                </table>
                <![endif]-->

            </td></tr>
    </table>
</div> 

