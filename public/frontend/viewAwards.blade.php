@extends('frontend.layouts.app')

@section('title', $news->title)
@section('meta_description', $news->description )
@section('meta-fb')
<meta property="og:url" content="http://dev.empowerveterans.us/awards/{{$news->id}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$news->title}}" />
<meta property="og:description" content="{{$news->description}}" />
<meta property="og:site_name" content="Empowerveterans" />

<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo-no-bg.png" />
@stop

@push('after-styles')
<style>
    .hidden {
        display: none;
    }

    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .btn-file {
        padding: 10px 0 15px;
        float: left;
    }

    .my-post a {
        text-align: right;
        padding: 10px 0px;
    }
</style>
@endpush

@section('content')
<div class="container">
    @if(session('msg'))
    <div class="alert alert-success">{{session('msg')}}</div>
    @endif
    <div class="row">


        <div class="col-sm-3">
            <!--left col-->



        </div>
        <!--/col-3-->

        <div class="col-sm-6">
            <div class="user-post">
                <div class="media">

                    <div class="user-post-details">

                        <div class="media-body">
                            <h4 class="company-name"> {{$news->title}} <span class="badge post-type">{{($news->type==0)? __('Awards'):__('Achievements')}}</span></h4>
                            @if($news->img != "")
                            <img src="{{asset('public/storage/'.$news->img)}}">
                            @endif
                            <p>{{$news->description}}</p>
                            
                        </div>
                    </div>
                  

                 
                    <!-- /row -->
                </div>
            </div>


        </div>

        <div class="col-sm-3">
            <!--left col-->
          

        </div>
        <!--/col-3-->


    </div>
</div>






@endsection


@push('after-scripts')

<script type="text/javascript">
    function toggleShareButton() {
        $('.share_button').toggleClass('hidden');
    }
    $(function() {
        $('.share_button').addClass('hidden');

        //radio box validation
        $("input[name$='help']").click(function() {
            var test = $(this).val();

            $("div.desc").hide();
            $("#hlp" + test).show();
        });


        //radio box validation

        $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]')
            .on('click', function(event) {
                var $panel = $(this).closest('.panel-google-plus');
                $comment = $panel.find('.panel-google-plus-comment');

                $comment.find('.btn:first-child').addClass('disabled');
                $comment.find('textarea').val('');

                $panel.toggleClass('panel-google-plus-show-comment');

                if ($panel.hasClass('panel-google-plus-show-comment')) {
                    $comment.find('textarea').focus();
                }
            });
        $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
            var $comment = $(this).closest('.panel-google-plus-comment');

            $comment.find('button[type="submit"]').addClass('disabled');
            if ($(this).val().length >= 1) {
                $comment.find('button[type="submit"]').removeClass('disabled');
            }
        });
    });

    $('.new-comment').on('click', function() {
        var id = $(this).data('id');

        $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function() {});

    });
    $('.new-comment-box').on('click', function() {
        var id = $(this).data('id');

        $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function() {});

    });

    function bindReplyButtonEvent() {

        $('.show-comment').on('click', '.new-comment-1', function() {
            var id = $(this).data('id');

            $('.show-comment-1[data-id="' + id + '"]').toggleClass('hidden');

        });
    }




    bindReplyButtonEvent();

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();


        $('.commentForm').on('submit', function(e) {

            e.preventDefault();
            var formData = new FormData(this);
            //alert($(this).attr('action'));
            var self = this;
            $.ajax({
                url: $(self).attr('action'),
                type: 'POST',
                data: formData,
                success: function(data) {

                    $(self)[0].reset();
                    $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function() {});
                    $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
                    // //bindReplyButtonEvent();
                    console.log(data);
                },
                processData: false,
                contentType: false

            });
        });



    });

    function applyPost() {
        $('#applyModal').modal('toggle');
    }

    function closePost() {
        $('#closePostModal').modal('toggle');

    }
    // function addComment(event) {
    //     // alert(form.action);
    //    // $(form).preventDefaults();
    //    event.preventDefault();
    //     var formData = new FormData(event.target);
    //     $.ajax({
    //         url: $(form).attr('action'),
    //         type: 'post',
    //         dataType: 'json',
    //         data: formData,
    //         success: function(data) {

    //             $(form)[0].reset();
    //             $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function() {});
    //             $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
    //             //bindReplyButtonEvent();
    //             console.log(data);
    //         }

    //     });
    //     return false;
    // }
    // var app = new Vue({

    // });
</script>
<script type="text/javascript">
    var wrapper = $('#doc-container'); //Input field wrapper
    var fieldHTML = getFieldHTML();

    //Once add button is clicked
    $(wrapper).on('click', '.add_button', function(e) {
        $(wrapper).append(fieldHTML);
        $('#counter').val(parseInt($('#counter').val()) + 1);
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e) {
        e.preventDefault();
        $(this).parent('td').parent('tr').remove(); //Remove field html
        $('#counter').val(parseInt($('#counter').val()) - 1);
    });

    function getFieldHTML() {
        var fieldHTML = '<tr><td><textarea name="quest[title][]" class="form-control" placeholder="Enter your question here" required></textarea></td><td><select name="quest[option_type][]" class="form-control" required><option value="1">Textbox</option><option value="2">Dropdown List</option><option value="3">Radio button</option></select></td><td><textarea name="quest[answers][]" class="form-control" placeholder="Enter your answers here in comma separated form"></textarea></td><td><input type="checkbox" name="quest[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
        return fieldHTML;
    }
</script>
@endpush