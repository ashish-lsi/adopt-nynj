@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
@section('indexstyle')
<link rel="stylesheet" href='css/style.css' />
@stop
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"/>
		<li data-target="#myCarousel" data-slide-to="1"/>

	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="{{ asset('img/frontend/slider1.jpg') }}" alt="banner 1">
			</div>

			<div class="item">
				<img src="{{ asset('img/frontend/slider2.jpg') }}" alt="banner 2">
				</div>


			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"/>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"/>
				<span class="sr-only">Next</span>
			</a>
		</div>


		<!-- Page Content -->
		<div class="container " >

			<div class="gridBox">


				<div class="col-sm-3 col-md-3">
				<h3 class="title blueText "> <a href="/announcements"> Announcements </a> </h3>
                @foreach($posts as $post)
              @if($post->type  == "2")
					<div class="blueBg  " >
			
                        <h4 class="entry-title ">{!! $post->title !!}</h4>
                        <!-- @if($post->image != "")
                        <img src="{{asset('storage/app/files/'.$post->image)}}" >
                        @endif -->
						<p class="paragraph ">{!! str_limit($post->content,400) !!}</p>
						<a href="{{route('frontend.wp-post-details',['slug'=>str_slug($post->title),'id'=>$post->id])}}" class="readMore blueText more-link">Continue reading
						</a>
					</div>
              @endif             
              @endforeach
				</div>

				<div class="col-sm-6 col-md-6" >
				<h3 class="title blueText "><a href="/success-stories">Success Stories </a></h3>
                @foreach($posts as $post)
              @if($post->type  == "3")
					<div class="blueBg  " >
			
                        <h4 class="entry-title ">{!! $post->title !!}</h4>
                        <!-- @if($post->image != "")
                        <img src="{{asset('storage/app/files/'.$post->image)}}" >
                        @endif -->
						<p class="paragraph ">{!! str_limit($post->content,400) !!}</p>
						<a href="{{route('frontend.wp-post-details',['slug'=>str_slug($post->title),'id'=>$post->id])}}" class="readMore blueText more-link">Continue reading
						</a>
					</div>
              @endif             
              @endforeach
				</div>

				<div class="col-sm-3 col-md-3" >
					<h3 class="title blueText"><a href="/alerts"> Alerts </a></h3>
              @foreach($posts as $post)
              @if($post->type  == "1")
					<div class="blueBg  " >
			
                        <h4 class="entry-title ">{!! $post->title !!}</h4>
                        <!-- @if($post->image != "")
                        <img src="{{asset('storage/app/files/'.$post->image)}}" >
                        @endif -->
						<p class="paragraph ">{!! str_limit($post->content,400) !!}</p>
						<a href="{{route('frontend.wp-post-details',['slug'=>str_slug($post->title),'id'=>$post->id])}}" class="readMore blueText more-link">Continue reading
						</a>
					</div>
              @endif             
              @endforeach
				</div>

				<!-- <div class="col-sm-4  ">
              <div class="whitebg">
                  <h3 class="title blueText ">Weather Forecast</h3>
                  <p class="paragraph ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                  <a href="post-details.html" class="readMore blueText ">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
              </div>
              </div>
              <div class="col-sm-4 col-md-4">
              <div class="  blueBg ">
                  <h3 class="title blueText ">Alerts</h3>
                  <p class="paragraph ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <a href="post-details.html" class="readMore blueText ">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
              </div>
              </div> -->
			</div>




			<!-- <div class="gridBox">
              <div class="col-sm-6 col-md-3 ">
                  <img src="{{ asset('img/frontend/image1.jpg') }}" class="img-fluid" />
              </div>
              <div class=" col-sm-6">
                  <div class=" blueBg 2">
                  <h3 class="title blueText ">Success Stories</h3>
                  <p class="paragraph ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
              </div>
              </div>
              <div class="col-sm-6 col-md-3 ">
                  <img src="{{ asset('img/frontend/image1.jpg') }}" class="img-fluid" />
              </div>
          </div>
        -->


			<!-- <div class="gridBox">
              <div class="col-sm-6 col-md-3 ">
              <div class=" blueBg ">
                  <h3 class="title blueText ">Recent Activities</h3>
                  <p class="paragraph ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <a href="post-details.html" class="readMore blueText ">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
              </div>        
              </div>        
              <div class=" col-sm-6 ">
              <img src="{{ asset('img/frontend/image3.jpg') }}" class="img-fluid" />
                <div class="whitebg">
                  <h3 class="title blueText ">Success Stories</h3>
                  <p class="paragraph ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                  <a href="post-details.html" class="readMore blueText ">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                  </div>
              </div>
              <div class="col-sm-6 col-md-3 ">
              <div class="  blueBg ">
                  <h3 class="title blueText ">News</h3>
                  <p class="paragraph ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                  <a href="post-details.html" class="readMore blueText ">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
              </div>
          </div> -->

		</div> 
	</div> 


@endsection
	