@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@push('after-styles')
<style>
    .badge.post-type {
        float: right;
        width: auto;
    }

    .post-title {
        width: 100%;
    }
</style>
@endpush
@section('content')

<div class="container ">
    <div class="col-sm-3">
        <!--left col-->

        <div class="panel panel-default side-filter">
            <div class="panel-heading">{{__('Filter')}} </div>
            <div class="panel-body">
                <form >
                    <ul class="">
                        <li> <input type="radio" name="type" id="help_seeker_radio" {{Input::get("type")==0 ? 'checked':''}}  value="0"  class="radio_nav_btn"><label for="help_seeker_radio">{{__('Help Seeker')}}</label> </li>
                        <li> <input type="radio" name="type" id="help_giver_radio" {{Input::get("type")==1 ? 'checked':''}} value="1" class="radio_nav_btn"> <label for="help_giver_radio">{{__('Providing help')}}</label></li>
                        <li>
                        <label>{{__('Category')}} </label>
                            <select class="form-control select_value" name="category">
                                <option value=""> All </option>
                                @foreach ($categories as $category)

                                <option value="{{ $category->category_id }}" {{Input::get("category")==$category->category_id ? 'selected':''}} > {{ $category->name }} </option>

                                @endforeach
                            </select>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>{{__('From Date')}} </label>
                                <input type="date" name="from_date" value="{{Input::get('from_date')}}" class="form-control from_date">
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label>{{__('To Date')}}</label>
                                <input type="date" name="to_date" value="{{Input::get('to_date')}}" class="form-control to_date">

                            </div>
                        </li>

                        <li>
                        <label>{{__('Diversity Type')}}</label>
                            <select class="form-control" name="diversity">
                                <option value="">{{__('Diversity Type')}}</option>
                                @foreach($diversity  as $diver)
                                <option value="{{$diver->id}}">{{$diver->name}}</option>
                                @endforeach
                            </select>
                        </li>

                        <li>
                        <label>{{__('Location')}}</label> 
                            <input type="text" name="location" value="{{Input::get('location')}}" class="form-control location" placeholder="{{__('Location')}}" >
                        </li>
                        <li> <input type="text" name="state" value="{{Input::get('state')}}" class="form-control location" placeholder="{{__('State')}}" >
                        </li>
                        <li> <input type="text" name="city" value="{{Input::get('city')}}" class="form-control location" placeholder="{{__('City')}}" >
                        </li>
                        <input class="form-control query" type="text" name="query" value="{{Input::get('query')}}" placeholder="{{__('Search')}}" aria-label="Search" >
                        <br>
						<button type="button"  class="btn btn-default my-2 my-sm-0 resetBtn" >{{__('Reset')}}</button>
                        <button class="btn btn-info my-2 my-sm-0" type="submit"><i class="fa fa-filter"> </i> {{__('Submit')}}</button>
                    </ul>
                </form >
            </div>

        </div>




    </div>
    <!--/col-3-->

    <div class="col-sm-6">



        <div class="alert alert-info search-text">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>{{count($posts)}}</strong> {{__('results were found for the search for')}}
            <strong class="text-primary">" {{Input::get('query')}} "</strong>
        </div>
        @if(count($posts))
        @foreach($posts as $post)
        <div class="user-post">
            <div class="media">
                <div class="user-post-details search-page">
                    <div class="row">

                        <div class="company-name search-page">
                            @if($post->user)
                            <a class="float-left post-title" href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}"><strong>{{ $post->user->company_name }}</strong>
                                <span class="badge post-type time-of-post"> {{$post->created_at->diffForHumans()}}</span>
                                <span class="badge post-type">{{($post->type==0)?'Help Seeker':'Providing help'}}</span>
                            </a>
                            @if($post->user->address)
                            <span>{{ $post->location }} 
                            {{ ($post->city) ? ", ".$post->city :"" }}{{ ($post->state) ? ", ".$post->state :"" }}{{ ($post->pincode) ? ", ".$post->pincode :"" }} </span>
                            
                            @endif
                            @endif

                        </div>

                    </div>
                    <div class="search-page-post-details">
                        <h4><a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}"> {{$post->title}} </a></h4>

                        <p>{{ $post->article }}
                            <!-- <a href="#"> readmore </a> -->

                        </p>
                    </div>

                </div>

            </div>
        </div>
        @endforeach
        {!! $posts->links() !!}
        @endif

    </div>

    <div class="col-sm-3">
        <!--left col-->

        <div class="panding-post">

            <h3>{{__('My Post')}} </h3>
            @if($my_post)
            <div class="my-post">
                <h4><a href="{{route('frontend.user.post-details',['post_id'=>$my_post->post_id])}}"> {{$my_post->title}} </a> </h4>
                <!-- <img src="{{ asset('img/frontend/image1.jpg') }}"> -->
                <div class="my-post-footer">
                    <!-- <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share </a> -->

                        <i class="fa fa-comments"> </i> {{$my_post->comments->count()}} comments


                    <p>{{$my_post->article}}</p>

                </div>
            </div>
            @endif
        </div>

    </div>
    <!--/col-3-->

</div>

@endsection


@push('after-scripts')

<script>
    $(document).ready(function(){
        $('.resetBtn').click(function(){
           $('input').each(function(){
                $(this).val('');
           });
           $('select').each(function(){
                $(this).val('');
           });
        })
    })

</script>
@endpush
