@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')

<div class="row">

    <div class="col-sm-3">
        <!--left col-->


        <div class="profile-details">
            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle" alt="avatar">
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">Company Details </div>
            <div class="panel-body">
                <ul>
                    <li><strong>Company name : </strong>{{$post->company_name}} </li>
                    <li><strong>Address :</strong> {{$post->address}} </li>
                    <li><strong>Contact details :</strong>
                        <div class="contact_lft_d">
                            <a href="mailto: {{$post->email}}">
                                {{$post->email}}</a>
                        </div>
                        <div class="contact_lft_d">
                            <a href="tel:{{$post->contact}}">
                                {{$post->contact}}
                            </a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>




    </div>
    <!--/col-3-->

    <div class="col-sm-6">



        <div class="user-post">
            <div class="media">

                <div class="user-post-details">

                    <div class="media-body">
                        <h4> {{$post->title}} </h4>

                        <p>{{$post->article}}</p>
                        @if($post->file != "")
                        <div class="row">
                            <div class="post-details-file">
                                <div class="col-sm-12">

                                    <h4>Attached Files </h4>
                                </div>

                                <a href="/download/{{$post->file}}">
                                    <i class="fa fa-paperclip" aria-hidden="true"></i> {{$post->file}}
                                </a>


                                <!-- 			 
                <a href="#">
               <i class="fa fa-paperclip" aria-hidden="true"></i> product-list.pdf
                </a>
                
            
		
               <a href="#" >
              <i class="fa fa-paperclip" aria-hidden="true"></i> Company-logo.jpg
                </a>
                
          
			 
                <a href="#" >
             <i class="fa fa-paperclip" aria-hidden="true"></i> product-card.jpg
                </a> -->


                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <div class="panel-footer">
                    <span type="button" class=" btn btn-default btn-disabled">
                        <span class=" fa fa-calendar "> </span> {{$post->created_at->diffForHumans()}}
                    </span>
                    <!-- <span type="button" class=" btn btn-default btn-disabled ">
                        <span class="fa fa-clock-o"> </span> 4:28 pm
                    </span>
                   -->



                </div>
                <div class="panel-google-plus-comment">

                    <div class="panel-google-plus-textarea">
                        <form method="post" action="{{route('frontend.user.comment')}}">
                            @csrf
                            <textarea rows="3" name="body"></textarea>
                            <input type="hidden" name="post_id" value="{{$post->post_id}}">
                            <button type="submit" class=" btn btn-success  ">Post comment</button>
                            <button type="reset" class=" btn btn-default ">Cancel</button>
                            <a href="#" class="pull-right"> Show all Comment </a>

                        </form>
                    </div>

                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">

                            @foreach($post->comments as $comment)
                            <div class="row">

                                <div class="col-md-2">
                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg"
                                        class="img img-rounded img-fluid profile-pic-xs" />

                                </div>
                                <div class="col-md-10">
                                    <p>
                                        <a class="float-left"
                                            href="profile.html"><strong>{{$comment->user->company_name}}</strong>
                                        </a><small style="float:right">{{$comment->created_at->diffForHumans()}}</small>
                                    </p>
                                    <div class="clearfix"></div>
                                    <p>{{$comment->comment}}</p>
                                    <p>
                                        <a class="float-right btn btn-outline-primary ml-2 new-comment"> <i
                                                class="fa fa-reply"></i> Reply</a>
                                        <div class="show-comment">
                                            <form method="post" action="{{ route('frontend.user.comment') }}">
                                                @csrf
                                                <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                <input type="hidden" name="is_reply_to_id"
                                                    value="{{ $comment->comment_id }}" />
                                                <textarea type="textbox" name="body" class=" form-control"> </textarea>
                                                <button type="submit" class=" btn btn-success  ">Post</button>
                                            </form>
                                        </div>
                                    </p>
                                </div>
                            </div>

                            @foreach($comment->replies as $reply)
                            <div class="card card-inner">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg"
                                                class="img img-rounded img-fluid profile-pic-xs" />

                                        </div>
                                        <div class="col-md-10">
                                            <p><a
                                                    href="profile.html"><strong>{{$reply->user->company_name}}</strong></a><small
                                                    style="float:right">{{$reply->created_at->diffForHumans()}}</small>
                                            </p>
                                            <p>{{$reply->comment}}</p>
                                            <p>
                                                <a class="float-right btn btn-outline-primary ml-2"> <i
                                                        class="fa fa-reply"></i> Reply</a>

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endforeach

                        </div>
                    </div>


                </div><!-- /row -->
            </div>
        </div>


    </div>

    <div class="col-sm-3">
        <!--left col-->

        <div class="panding-post">

            <h3>My Post </h3>
            <div class="my-post">
                <h4> Taylormade is simply dummy text of the printing and typesetting industry.</h4>
                <img src="images/image1.jpg">
                <div class="my-post-footer">
                    <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share </a>
                    <a href="#">
                        <i class="fa fa-comments"> </i> 97 comments
                    </a>

                    <p>With the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                        desktop publishing software like Aldus PageMaker including versions <a href="post-details.html"
                            class="readMore blueText helveticaNeueLight">Read More <i class="fa fa-angle-double-down"
                                aria-hidden="true"></i></a></p>

                </div>
            </div>
        </div>


    </div>
    <!--/col-3-->

</div>


</div>

@endsection


@push('after-scripts')

<script type="text/javascript">
$(function() {


    //radio box validation
    $("input[name$='help']").click(function() {
        var test = $(this).val();

        $("div.desc").hide();
        $("#hlp" + test).show();
    });


    //radio box validation

    $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]')
        .on('click', function(event) {
            var $panel = $(this).closest('.panel-google-plus');
            $comment = $panel.find('.panel-google-plus-comment');

            $comment.find('.btn:first-child').addClass('disabled');
            $comment.find('textarea').val('');

            $panel.toggleClass('panel-google-plus-show-comment');

            if ($panel.hasClass('panel-google-plus-show-comment')) {
                $comment.find('textarea').focus();
            }
        });
    $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
        var $comment = $(this).closest('.panel-google-plus-comment');

        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
    });
});



$(document).ready(function() {
    $(".new-comment").click(function() {
        $(".show-comment").toggle();
    });
});
</script>

@endpush