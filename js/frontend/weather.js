//Weather Auto complete 
$('#txtcityname').on('change', function (e) {
    $('#txtcityid').val('');
});
$('#txtcityname').autocomplete({
    source: url,
    minLength: 3,
    select: function (event, ui) {
        $('#txtcityid').val(ui.item.id);
        $('#txtcityname').val(ui.item.value);
        $('#frmWeather').submit();
    }
});

$('#pagination-demo').twbsPagination({
    totalPages: 5,
    // the current page that show on start
    startPage: 1,
    // maximum visible pages
    visiblePages: 5,
    initiateStartPageClick: true,
    // template for pagination links
    href: false,
    // variable name in href template for page number
    hrefVariable: '{{number}}',
    // Text labels
    first: 'First',
    prev: 'Previous',
    next: 'Next',
    last: 'Last',
    // carousel-style pagination
    loop: false,
    // callback function
    onPageClick: function (event, page) {
        $('.page-active').removeClass('page-active');
        $('#page' + page).addClass('page-active');
    },
    // pagination Classes
    paginationClass: 'pagination',
    nextClass: 'next',
    prevClass: 'prev',
    lastClass: 'last',
    firstClass: 'first',
    pageClass: 'page',
    activeClass: 'active',
    disabledClass: 'disabled'

});


$('#frmWeather').on('submit', function (e) {
    var city = $('#txtcityid').val();
    if (city == '') {
        alert('Please select a valid city from list!!');
        return false;
    }
});
    