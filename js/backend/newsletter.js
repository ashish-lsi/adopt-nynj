//---------Step wise forms starting---------
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

//Init the select2 boxes
$('.select2').select2({
    placeholder: "Please select..",
    allowClear: true
});

//Merge the template & data and show in the CKeditor
$('.nextBtn').on('click', function () {
    if (currentTab === 7) {
        $('#preview_final').html('');
        var fullHtml = '<ul id="sortable" style="list-style: none; margin-left: 15%; max-width: 800px">';
        $('.preview_html').each(function () {
            var html = $(this).html();
            if (html !== '') {
                var html = $(this).get(0).outerHTML;
                fullHtml += '<li class="ui-state-default">' + html + '</li>';
            }
        });

        fullHtml += '</ul>';
        $('#preview_final').html(fullHtml);
    }
});

//Download the final template
$('#btnSubmit').on('click', function () {
    download();
    $('form').submit();
});

//Merge the speakers message template - step 1
function mergeSpeaker(tplId) {
    //Send data to merge with template
    $.ajax({
        type: 'POST',
        url: mergeSpeakerMsgUrl,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {tplId: tplId},
        success: function (result) {
            result = '<div class="module-container">' + result + '</div>';
            $('#speakerPreview').html(result);

            //Init the editor
            initEditor();
        }
    });
}

//Merge the alerts template - step 2
function mergeContents(contentType, tplId, contentValid) {
    //Check if message is filled
    var selectedAlerts = $('#txt' + contentType).val();

    if (selectedAlerts.length == 0) {
        alert('Please select ' + contentType + ' first to edit this template!!');
        return false;
    } else if (selectedAlerts.length != contentValid) {
        alert('To edit this template please select ' + contentValid + ' ' + contentType + '!!');
        return false;
    }

    //Send data to merge with template
    $.ajax({
        type: 'POST',
        url: mergeContentsUrl,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {selectedAlerts: selectedAlerts, tplId: tplId},
        success: function (result) {
            result = '<div class="module-container">' + result + '</div>';
            $('#' + contentType + 'Preview').html(result);

            //Init the editor
            initEditor();
        }
    });
}

//Merge the alerts template - step 2
function mergeCustom(tplId) {
    //Send data to merge with template
    var eleCnt = $("#customPreview").find("[data-val-attr='" + tplId + "']").length;
    if (eleCnt > 0) {
        $("#customPreview").find("[data-val-attr='" + tplId + "']").remove();
    } else {
        $.ajax({
            type: 'POST',
            url: mergeCustomUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {tplId: tplId},
            success: function (result) {
                result = '<div class="module-container" data-val-attr = "' + tplId + '">' + result + '</div>';
                $('#customPreview').append(result);

                //Init the editor
                initEditor();
            }
        });
    }
}

//Merge the Share links template - step 5
function mergeShareLinks(tplId) {
    //Check if message is filled
    var txtFb = $('#txtFb').val();
    var txtTwitter = $('#txtTwitter').val();
    var txtGoogle = $('#txtGoogle').val();
    var txtYoutube = $('#txtYoutube').val();
    var txtWhatsup = $('#txtWhatsup').val();

    if (txtFb == '' && txtTwitter == '' && txtGoogle == '' && txtYoutube == '' && txtWhatsup == '') {
        alert('Please enter at least of the above links to edit this template!!');
        return false;
    }

    //Send data to merge with template
    $.ajax({
        type: 'POST',
        url: mergeShareLinksUrl,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {txtFb: txtFb, txtTwitter: txtTwitter, txtGoogle: txtGoogle, txtYoutube: txtYoutube, txtWhatsup: txtWhatsup, tplId: tplId},
        success: function (result) {
            result = '<div class="module-container">' + result + '</div>';
            $('#sharePreview').html(result);

            //Init the editor
            initEditor();
        }
    });
}

//Merge the Share links template - step 5
function mergeContactUs(tplId) {
    //Send data to merge with template
    $.ajax({
        type: 'POST',
        url: mergeContactUsUrl,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {tplId: tplId},
        success: function (result) {
            result = '<div class="module-container">' + result + '</div>';
            $('#contactPreview').html(result);

            //Init the editor
            initEditor();
        }
    });
}

// This function will display the specified tab of the form ...
function showTab(n) {
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";

    $('html,body').animate({scrollTop: 0}, 'slow');

    // ... and run a function that displays the correct step indicator:
    fixStepIndicator(n);
}

// This function will figure out which tab to display
function nextPrev(n) {
    var x = document.getElementsByClassName("tab");

    // Exit the function if any field in the current tab is invalid:
    if (currentTab === 0 && !validateStep1())
        return false;

    if (currentTab === 1 && !validateStep2())
        return false;

    if (currentTab === 2 && !validateStep3())
        return false;

    if (currentTab === 3 && !validateStep4())
        return false;

    // Hide the current tab:
    x[currentTab].style.display = "none";

    // If the valid status is true, mark the step as finished and valid:
    document.getElementsByClassName("step")[currentTab].className += " finish";

    // Increase or decrease the current tab by 1:
    if (n !== -1) {
        currentTab = currentTab + 1;
    } else {
        currentTab = currentTab - 1;
    }

    // if you have reached the end of the form... :
    if (currentTab === x.length) {
        //...the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }

    // Otherwise, display the correct tab:
    showTab(currentTab);
}

// This function deals with validation of the form 1 fields
function validateStep1() {
    var valid = true;
    var txtName = $('#txtName').val();

    if (txtName === '') {
        valid = false;
        alert('Please enter template name!!');
        $('#txtName').focus();
    }

    return valid; // return the valid status
}

// This function deals with validation of the form 2 fields
function validateStep2() {
    var valid = true;
    var txtAlerts = $('#txtAlerts').find('option:selected').length;
    var tpl = $("#AlertsPreview").html();

    if (txtAlerts !== 0 && tpl === '') {
        valid = false;
        alert('Please select a template!!');
    }

    return valid; // return the valid status
}

// This function deals with validation of the form 2 fields
function validateStep3() {
    var valid = true;
    var txtAnnouncements = $('#txtAnnouncements').find('option:selected').length;
    var tpl = $("#AnnouncementsPreview").html();

    if (txtAnnouncements !== 0 && tpl === '') {
        valid = false;
        alert('Please select a template!!');
    }

    return valid; // return the valid status
}

// This function deals with validation of the form 2 fields
function validateStep4() {
    var valid = true;
    var txtStories = $('#txtStories').find('option:selected').length;
    var tpl = $("#StoriesPreview").html();

    if (txtStories !== 0 && tpl === '') {
        valid = false;
        alert('Please select a template!!');
    }

    return valid; // return the valid status
}

// This function removes the "active" class of all steps...
function fixStepIndicator(n) {
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class to the current step:
    x[n].className += " active";
}

//---------Step wise forms ending---------

function download() {
    var fileName = 'Newsletter.html';

    //Destroy the editor
    if (CKEDITOR.instances.preview !== undefined) {
        CKEDITOR.instances.preview.destroy();
    }

    //get the final html
    var elementHtml = $('#preview_final').html();
    var link = document.createElement('a');
    var mimeType = 'text/html';

    $('#tplHtml').val(elementHtml);

    link.setAttribute('download', fileName);
    link.setAttribute('href', 'data:' + mimeType + ';charset=utf-8,' + encodeURIComponent(elementHtml));
    link.click();

    //Re-init the editor
    initEditor();
}

function initEditor() {
    editableBlocks = $('[contenteditable="true"]');
    for (var i = 0; i < editableBlocks.length; i++) {
        CKEDITOR.inline(editableBlocks[i], {
            extraPlugins: 'sourcedialog',
            removePlugins: 'sourcearea'
        });
    }
}

//sorting functionality
$('#sort').on('click', function () {
    if ($(this).html() === 'Enable Sorting') {
        $("#sortable").sortable({
            revert: true
        });
        $('#sortable li').each(function () {
            $(this).addClass('move');
        });

        $(this).html('Disable Sorting');
    } else {
        $("#sortable").sortable("destroy");

        $('#sortable li').each(function () {
            $(this).removeClass('move');
        });

        $(this).html('Enable Sorting');
    }
});

//color picker functionality
$('.colorPick').minicolors({
    inline: false,
    letterCase: 'lowercase',
    opacity: false,
    theme: 'bootstrap',
    change: function (hex) {
        if (!hex)
            return;
        var ele = $(this).parent().next('.preview_html');
        if ($(ele).html() !== '') {
            $(this).parent().next('.preview_html').css({"background-color": hex});
        }
    }
});

//Darg and drop functionality
$(document).ready(function () {
    $(".draggable").draggable({
        connectToSortable: "#speakerPreview",
        helper: "clone",
        scroll: false,
        stop: function () {
            var tplId = $(this).attr('data-attr-id');
            var tplType = $(this).attr('data-attr-type');
            var valid = $(this).attr('data-attr-valid');

            if (tplType === 'speaker') {

                mergeSpeaker(tplId);
            } else if (tplType === 'Alerts') {

                mergeContents(tplType, tplId, valid);
            } else if (tplType === 'Announcements') {

                mergeContents(tplType, tplId, valid);
            } else if (tplType === 'Stories') {

                mergeContents(tplType, tplId, valid);
            } else if (tplType === 'share') {

                mergeShareLinks(tplId);
            } else if (tplType === 'contact') {

                mergeContactUs(tplId);
            } else if (tplType === 'custom') {

                mergeCustom(tplId);
            }
        },
        revert: "invalid"
    });
    $("ul, li").disableSelection();
});

$('.step').on('click', function () {
    var valid1 = validateStep1();

    if (valid1) {
        var step = $(this).attr('data-id');
        $('.tab').css('display', 'none');
        currentTab = parseInt(step);
        showTab(step);
    }
});

//----------***************Add the remove button to the container***************----------
//adding the del button on hover
$('.preview_html').on('mouseenter', '.module-container', function () {
    var html = '<div class="module-controls-container" style="top: ' + $(this).position().top + 'px"><!-- Controls -->' + '<div class="btn-module btn-module-delete"><i class="icon-close"></i></div>' + '<!-- /Controls --></div>';
    $(this).append(html);
});

//deleting the element on button click
$('.preview_html').on('click', '.btn-module-delete', function () {
    $(this).closest('.module-container').remove();
});

//removing the del button on blur
$('.preview_html').on('mouseleave', '.module-container', function () {
    $('div.module-controls-container').remove();
});


//----------***************Add the remove and copy button to the elements container***************----------
//adding the del button on hover
$('.preview_html').on('mouseenter', '.element-container', function () {
    var html = '<div class="element-controls-container" style="top: ' + $(this).position().top + '"><!-- Controls -->' + '<div class="btn-element btn-element-delete"><i class="icon-close"></i></div>' + '<!-- /Controls --></div>';
    $(this).append(html);
});

//deleting the element on button click
$('.preview_html').on('click', '.btn-element-delete', function () {
    $(this).closest('.element-container').remove();
});

//removing the del button on blur
$('.preview_html').on('mouseleave', '.element-container', function () {
    $('div.element-controls-container').remove();
});