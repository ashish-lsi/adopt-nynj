@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@push('after-styles')
<style>
    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .form-row.diversity_field label {
        float: left;
        padding: 6px;
    }

    span.multiselect-native-select {
        position: relative;
        float: left;
    }

    .multiselect-container>li>a>label.checkbox {
        margin: 0;
        padding: 5px 14px;
        color: #333;
    }

    .profile-right .blocks {
        margin: 0;
    }

    .my-post a {
        text-align: right;
    }
</style>
@endpush

@section('content')
<section class="profile-account-setting profile-account-post">
    <!-- Page Content -->
    <div class="container ">
        <div class="col-md-3 col-sm-5">
            <!--left col-->


            <div class="panel panel-default side-filter">
                <div class="panel-heading">Filter </div>
                <div class="panel-body">
                    <form action="">
                        <ul class="">
                            <li> <input type="radio" name="type" id="help_seeker_radio" {{Input::get("type")==0 ? 'checked':''}} value="0" class="radio_nav_btn"><label for="help_seeker_radio">Help Seeker</label> </li>
                            <li> <input type="radio" name="type" id="help_giver_radio" {{Input::get("type")==1 ? 'checked':''}} value="1" class="radio_nav_btn"> <label for="help_giver_radio">Providing help</label></li>
                            <li>
                                <select class="form-control select_value" name="category">
                                    <option value=""> All </option>
                                    @foreach ($categories as $category)

                                    <option value="{{ $category->category_id }}" {{Input::get("category")==$category->category_id ? 'selected':''}}> {{ $category->name }} </option>

                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label>From Date </label>
                                    <input type="date" name="from_date" value="{{Input::get('from_date')}}" class="form-control">
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label>To Date</label>
                                    <input type="date" name="to_date" value="{{Input::get('to_date')}}" class="form-control">

                                </div>
                            </li>

                            <li> <input type="text" name="location" value="{{Input::get('location')}}" class="form-control" placeholder="Location">
                            </li>
                            <input class="form-control " type="text" name="query" value="{{Input::get('query')}}" placeholder="Search" aria-label="Search">
                            <br>
                            <button class="btn btn-default my-2 my-sm-0" type="submit"><i class="fa fa-search"> </i>Search</button>
                        </ul>
                    </form>
                </div>

            </div>

            <div style="margin-top:20px;">
                <!-- <a href="{{route('frontend.user.seekerAnalysis')}}" class="btn btn-info width-100" role="button"> Help Seeker Analysis </a> -->
            </div>


        </div>
        <!--/col-3-->

        <div class="col-md-6 col-sm-7">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        <h4 class="post-comment"><i class="fa fa-pencil-square-o"> </i> My Post </h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                        <h4 class="post-comment"><i class="fa fa-pencil-square-o"> </i> My Interactions </h4>
                    </a>
                </li>

            </ul>


            <div class="tab-content" id="myTabContent">
                <div class="tab-pane   active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif

                    @if(count($posts) == 0 )

                    <div class="user-start-post">

                        <div class="col-sm-12 text-center">
                            <h3 class="post-comment"><i class="fa fa-pencil-square-o"> </i>{{__('Create a post')}} </h3>
                        </div>
                        <div style="clear:both"></div>

                    {{ html()->form('POST', route('frontend.user.dashboard'))->attribute('enctype', 'multipart/form-data')->attribute('class', 'show-post-comment')->open() }}

                    <input type="hidden" name="company_id" value="{{$logged_in_user->id}}">
                    <input type="hidden" name="enabled" value="1">

                    <div class="form-row">
                        <label>Enter Title</label>
                        <input type="text" class="form-control" maxlength="80" placeholder="Title" name="title" value="{{ old('title') }}" required>
                    </div>

                    <div class="form-row">
                        <label>Write Story</label>
                        <textarea id="textarea" name="article" cols="40" rows="3" maxlength="200" required class="form-control" placeholder="Story"> {{ old('article') }} </textarea>
                    </div>
                    <div class="form-row">
                        <label>Upload Your File </label>
                        <input type="file" class="form-control" name="file">
                    </div>

                    <div class="form-row" id="second_f">
                        <input type="text" class="form-control" placeholder="Address">
                    </div>

                    <div class="form-row userpr">
                        @if(auth()->user()->category_id == 0)
                        <label> <input type="radio" name="type" class="type" value="0" required> Seeking help</label>
                        <label> <input type="radio" name="type" class="type" value="1" required> Providing help</label>
                        <label> <input type="radio" name="type" class="type" value="2" required> General </label>
                        @elseif (auth()->user()->category_id == 1)
                        <label> <input type="radio" name="type" class="type" value="1" required> Providing help</label>
                        <label> <input type="radio" name="type" class="type" value="2" required> General </label>
                        @else
                        @endif
                    </div>

                    <div class="form-row diversity_field">
                        <label for="">Select Diversity</label>
                        <select name="diversity[]" id="diversity_dropdown" class="form-control" multiple="multiple" required>
                            @foreach ($diversity as $d)
                            <option value="{{ $d->id }}"> {{ $d->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-row  category_field">
                        <label for="">Select Category</label>
                        <select name="category_id" id="category_dropdown" class="form-control" required>
                            @foreach ($categories as $category)
                            <option value="{{ $category->category_id }}"> {{ $category->name }} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-row location_field">
                        <label for="">Select Location</label>
                        <input type="text" name="location" id="location" class="form-control">
                    </div>

                    <div class="form-row location_field">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">State</label><br>
                                <select name="state" id="State" class="form-control select2" data-ajax--cache="false">
                                    <option>Select State</option>
                                    @foreach($states as $state)
                                    <option value="{{$state->state}}">{{$state->state}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" name="state" id="state" class="form-control" placeholder="Type State"> -->
                            </div>
                            <div class="col-sm-4">
                                <label for="">City</label> <br>
                                <select name="city" id="City" class="form-control select2" data-ajax--cache="false">
                                    <option>Select City</option>
                                    @foreach($cities as $city)
                                    <option value="{{$city->city}}">{{$city->city}}</option>
                                    @endforeach
                                </select>
                                <!-- <input type="text" name="city" id="city" class="form-control" placeholder="Type City"> -->
                            </div>
                            <div class="col-sm-4">
                                <label for="">Pincode</label>
                                <input type="text" name="pincode" id="pincode" class="form-control" placeholder="Enter Pincode">
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info pull-right">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i> Submit
                    </button>
                    {{ html()->form()->close() }}
                </div>
                @endif

                @foreach ($posts as $post)

                <div class="user-post">
                    <div class="media">
                        <div class="user-post-details">
                            <div class="row">
                                <div class="company-logo-img ">

                                    @if( $post->avatar_location != "")

                                    <img src="{{ $post->avatar_location }}" class="img img-rounded img-fluid" />
                                    @else

                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />

                                    @endif

                                </div>
                                <div class="company-name">

                                    <a class="float-left" href="{{route('frontend.user.view-profile',$post->user->id)}}"><strong> {{ $post->user->company_name }}</strong></a>
                                    @if($post->user->address)
                                    <span> {{ $post->user->address->address }}, {{ $post->user->address->City }}, {{ $post->user->address->State}}, {{ $post->user->address->Pincode}}</span>
                                    @endif

                                </div>

                            </div>
                            <div class="home-page-post-details">
                                <h4><a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}"> {{$post->title}} </a></h4>

                                <p> {{ $post->article }}
                                    <!-- <a href="#"> readmore </a> -->
                                </p>
                            </div>

                        </div>
                        <div class="panel-footer">
                            @if($post->status == '1')
                            <span type="button" class=" btn btn-default btn-disabled f-left">
                                Closed
                            </span>
                            @endif
                            <span type="button" class=" btn btn-default btn-disabled">
                                <span class=" fa fa-calendar "> </span> {{$post->created_at->diffForHumans()}}
                            </span>
                            <!-- <span type="button" class=" btn btn-default btn-disabled ">
                           <span class="fa fa-clock-o"> </span> 4:28 pm
                       </span> -->

                            <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}" type="button" class="btn btn-default ">
                                <span class=" fa fa-eye "> </span> Show Comments
                            </a>
                            @if($post->company_id == auth()->user()->id)
                            <a href="{{route('frontend.user.edit-post',['post_id'=>$post->post_id])}}" type="button" class="btn btn-default <?= $post->status == 1 ? 'btn-disabled' : '' ?>">
                                <span class=" fa fa-eye "> </span> Edit Post
                            </a>
                            @endif



                        </div>
                    </div>
                </div>
                <hr />
                @endforeach

                {!! $posts->links() !!}

            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                @if( isset($interactions))


                @foreach($interactions as $int)
                <div class="user-post">
                    <div class="media">
                        <div class="user-post-details search-page">
                            <div class="row">

                                <div class="company-name search-page">
                                    @if($int->post->user)
                                    <a class="float-left post-title" href="{{route('frontend.user.view-profile',['user'=>$int->post->user->id])}}"><strong>{{ $int->post->user->company_name }}</strong>
                                        <span class="badge post-type time-of-post"> {{$int->post->created_at->diffForHumans()}}</span>
                                        <span class="badge post-type">{{($int->post->type==0)?'Help Seeker':'Providing help'}}</span>
                                    </a>
                                    @if($int->post->user->address)
                                    <span>{{ $int->post->location }}
                                        {{ ($int->post->city) ? ", ".$int->post->city :"" }}{{ ($int->post->state) ? ", ".$int->post->state :"" }}{{ ($int->post->pincode) ? ", ".$int->post->pincode :"" }} </span>

                                    @endif
                                    @endif

                                </div>

                            </div>
                            <div class="search-page-post-details">
                                <h4><a href="{{route('frontend.user.post-details',['post_id'=>$int->post->post_id])}}"> {{$int->post->title}} </a></h4>

                                <p>{{ $int->post->article }}
                                    <!-- <a href="#"> readmore </a> -->

                                </p>
                            </div>

                        </div>

                    </div>
                </div>
                @endforeach

                {!! $interactions->links() !!}

                @endif
            </div>
        </div>
    </div>

    <div class="col-md-3 hidden-sm">
        <!--left col-->

        <div class="profile-right">
            @if(count($achievements))
            <h2 class="title ">Our Achievements</h2>
            @foreach($achievements as $achievement)
            @if($achievement->img != "")
            <div class="blocks">
                <img src="{{ asset('public/storage/'.$achievement->img) }}" class="img-fluid" />
            </div>
            @endif
            <div class="blueBg blocks mb-15">
                <h3 class="title ">{{$achievement->title}}</h3>
                <p>{{$achievement->description}}</p>
                <!-- <a href="post-details.html" class="readMore blueText helveticaNeueLight">Read More <i class="fa fa-angle-double-down" aria-hidden="true"></i></a> -->
            </div>
            @endforeach
            @endif
        </div>
    </div>
    <!--/col-3-->
</div>
</section>


@endsection

@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $(".post-comment").click(function () {
            $(".show-post-comment").toggle();
        });
        $('#diversity_dropdown').multiselect();
        $('.type').on('click', function () {
            if ($(this).val() == 2) {
                toggleRequireField('hide');
                $('#category_id').removeAttr('required');
                $('#diversity_dropdown').removeAttr('required');
            } else if ($(this).val() == 0) {
                toggleRequireField('show');
                $('.diversity_field').hide();
                $('#diversity_dropdown').removeAttr('required');
            } else {
                $('#diversity_dropdown').attr('required', 'true');
                $('#category_id').attr('required', 'true');
                toggleRequireField('show');
            }
        });
    });

    $('#State').select2({
        ajax: {
            url: '{{route("frontend.get_state")}}',
            dataType: 'json'
        }
    });

    $('#City').select2({
        ajax: {
            url: '{{route("frontend.get_city")}}',
            dataType: 'json'
        }
    });


    //Location Auto complete 
    $('#location').autocomplete({
        source: '{{route("frontend.get_address")}}',
        minLength: 3,
        select: function (event, ui) {
            $('#location').val(ui.item.value);
            $('#pincode').val(ui.item.Pincode);
            $('#State').val(ui.item.State).trigger('change');
            $('#City').val(ui.item.City).trigger('change');
        }
    });

    function toggleRequireField(state) {
        if (state == "show") {
            $('.diversity_field').show();
            $('.location_field').show();
            $('.category_field').show();
        } else {
            $('.diversity_field').hide();
            $('.location_field').hide();
            $('.category_field').hide();
        }
    }

    function initialize() {
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
    }
</script>

@endpush
