@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')

<!-- Page Content -->
<section>
    <div class="notification">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="main-left-sidebar no-margin">
                        <div class="user-data full-width">
                            <div class="user-profile">
                                <div class="username-dt">
                                    <div class="usr-pic">
                                        @if( $logged_in_user->avatar_location != "")
                                        <img src="{{asset('public/storage/'. $logged_in_user->avatar_location) }}" alt="avatar">
                                        @else
                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" alt="avatar">
                                        @endif
                                    </div>
                                </div><!--username-dt end-->
                                <div class="user-specs">
                                    <div class="sd-title">
                                        <h3>{{ $logged_in_user->first_name }}</h3>
                                    </div>
                                </div>
                                <ul class="user-fw-status">
                                    <li class="no-bor">
                                        <a href="{{route('frontend.user.profile')}}">{{__('View Profile')}}</a>
                                    </li>
                                </ul>
                            </div><!--user-profile end-->
                        </div><!--user-data end-->
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="acc-setting">
                        <div class="nt-title">
                            <h2>Notifications</h2>
                            <a href="#" title="" class="mark-read">Mark All as Read</a>
                        </div>

                        <div class="notifications-list notification-container">
                            @if(count($notifications) == 0)
                            <div class="notfication-details">
                                <div class="notification-info">
                                    <h3>Currently there are no new notifications in your account.</h3>
                                </div><!--notification-info -->
                            </div><!--notfication-details end-->

                            @else
                            @foreach($notifications as $notification)
                            <div class="notfication-details">
                                <div class="noty-user-img">
                                    @if($notification->user->avatar_location != "")
                                    <img src="{{asset('public/storage/'. $notification->user->avatar_location) }}" class="img img-rounded img-fluid" />
                                    @else
                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
                                    @endif
                                </div>
                                <div class="notification-info">
                                    <h3>
                                        <a href="{{route('frontend.user.viewNotification',[$notification->id])}}">
                                            {{$notification->user->company_name}}

                                            <br>
                                            @if($notification->comment != null)
                                            Commented on your post <br>
                                            @endif

                                            <b>{{$notification->post->title}}</b>
                                        </a>
                                    </h3>
                                    <br>
                                    <span>{{$notification->created_at->diffForHumans()}}</span>
                                </div><!--notification-info -->
                            </div><!--notfication-details end-->
                            @endforeach
                            @endif
                        </div><!--notifications-list end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
