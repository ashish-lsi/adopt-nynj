@extends('frontend.layouts.app')

@section('title', $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:site_name" content="Adopt-a-Company" />
<meta property="og:image" content="http://www.adoptcompany.com/img/frontend/tti-adopt-logo.png" />
@stop

@section('content')
<section class="forum-page">
    <div class="container">
        <div class="forum-questions-sec">
            <div class="row">
                <div class="col-lg-8 no-pdd">
                    <div class="forum-post-view forum-post-details">
                        <div class="usr-question">
                            <div class="usr_img">
                                <img src="images/resources/usrr-img1.png" alt="">
                            </div>
                            <div class="usr_quest">
                                <h3> {{$post->title}}</h3>
                                <span><i class="fa fa-clock-o"></i>{{$post->created_at->diffForHumans()}}</span>
                                <span class="share_us">
                                    <i class="fa fa-share-alt"></i>
                                </span>
                                <span class="share_icon">
                                    {!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), $post->title,['target' => '_blank'])
                                    ->facebook()
                                    ->twitter()                            
                                    ->linkedin('Extra linkedin summary can be passed here')
                                    !!}
                                </span>

                                <span class="badge post_h_de {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}"> {{$post->type == 0 ? 'Help Seeker' : 'Help Provider' }}</span>

                                <p>{{$post->article}} </p>
                                <h4><b>Quantity:</b> {{$post->qnty}} </h4>
                                <div>
                                    <ul class="like-com" style="width: 100%;">
                                        <li>
                                            @foreach($post->attachements as $attach)
                                            @if(in_array($attach->extension,['jpg','jpeg','png']))
                                            <a class="example-image-link" href="{{asset('public/storage/'.$attach->location )}}" data-lightbox="example-1">
                                                <img src="{{asset('public/storage/thumb/'.$attach->thumb )}}">
                                            </a>
                                            @else
                                            <a class="example-image-link" target="_blank" href="{{asset('public/storage/'.$attach->location )}}">
                                                <img src="{{asset('public/img/'.$attach->extension.'.png' )}}">
                                            </a>
                                            @endif
                                            @endforeach
                                        </li>
                                    </ul>
                                </div>
                                <div class="company_detail">
                                    <div class="de                                                                                                        tails">
                                        <label>Category </label> <span class="span_col">:</span>
                                        <span>{{$post->category_id != 11 ? $post->category->name : $post->category_name}}</span>
                                    </div>
                                    <div class="details">
                                        <label>Location </label> <span class="span_col">:</span>
                                        <span> {{ ($post->state) ? $post->state : "" }}
                                            {{ ($post->city) ? ", ".$post->city : "" }}
                                            {{ ($post->pincode) ? ", ".$post->pincode : "" }}</span>
                                    </div>
                                </div>
                                <div class="post-comment-box">
                                    <h3></h3>
                                    <div class="user-poster">
                                        <div class="usr-post-img">
                                            <img src="images/resources/bg-img2.png" alt="">
                                        </div>

                                        <!--post_comment_sec end-->
                                    </div>
                                    <!--user-poster end-->
                                </div>
                                <!--post-comment-box end-->
                                <div class="comment-section">
                                    <input type="hidden" id="comment_loaded{{$post->post_id}}" value="0">
                                    <a data-id="{{$post->post_id}}" class="com"><i class="fa fa-comment-alt"></i> Comments</a>
                                    <div class="comment-sec">
                                        <div class="comment_box cb{{$post->post_id}} clearfix">
                                            <div class="col-lg-1 col-md-2 fl_lft">
                                                <div class="comment_icon"><img src="images/ny-img1.png" alt=""> </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="comment-section">
                                                    <div class="comment-sec">
                                                        <div class="tab-pane  active" id="comments-{{$post->post_id}}" role="tabpanel" aria-labelledby="home-tab"><br>
                                                        </div>
                                                    </div>
                                                    <!--comment-sec end-->
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <!--comment-sec end-->
                                </div>
                            </div>
                            <!--usr_quest end-->
                        </div>
                        <!--usr-question end-->
                    </div>
                    <!--forum-post-view end-->
                </div>
                <div class="col-lg-4">
                    <div class="main-left-sidebar no-margin">
                        <div class="user-data full-width">
                            <div class="user-profile">
                                <div class="username-dt">
                                    <div class="usr-pic">
                                        <img src="{{App\Helpers\Helper::getProfileImg($post->user->avatar_location)}}" alt="">
                                    </div>
                                </div>
                                <!--username-dt end-->
                                <div class="user-specs">
                                    <div class="sd-title no-bor">
                                        <h3><a href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}">{{$post->user->company_name}}</a></h3>
                                        <!-- <i class="la la-ellipsis-v"></i> -->
                                    </div>
                                </div>
                                <ul class="user-fw-status">
                                    <li>
                                        <h4>Address</h4>
                                        <span> {{$post->user->address->address ?? ''}} </span>
                                    </li>

                                    <li>
                                        <h4>Contact details </h4>
                                        <span> {{$post->user->email ?? ''}}</span>
                                    </li>
                                    <li>
                                        <h4>Address</h4>
                                        <span> xxxx,xxxxxxxxxxxx </span>
                                    </li>

                                    <li>
                                        <h4>Contact details </h4>
                                        <span> xxxxxxxxxxxxxx</span>
                                    </li>
                                </ul>
                            </div>
                            <!--user-profile end-->
                        </div>
                        <!--user-data end-->
                        <div class="suggestions full-width">
                            <div class="sd-title">
                                <h3>Post Location</h3>
                            </div>
                            <div class="suggestions-list suggestions-list-profile">
                                <div id="map"></div>
                            </div>

                            <div class="weather-desc" id="weather-desc">
                                <div class="sd-title">
                                    <h3>Weather alerts for this location</h3>
                                </div>
                                <div class="suggestions-list suggestions-list-profile suggestions-list-post">
                                    @if(isset($alertsdata->features) && count($alertsdata->features) > 0)
                                    @foreach ($alertsdata->features as $key => $alert)

                                    <li>{{$alert->properties->headline}}</li>

                                    @endforeach
                                    <br>
                                    {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                                    <input type="hidden" name="City" id="weather-city" value="{{$post->city}}" />
                                    <input type="hidden" name="State" id="weather-state" value="{{$post->state}}" />
                                    <input type="hidden" name="Pincode" id="weather-pincode" value="{{$post->pincode}}" />
                                    <input type="hidden" name="addr-lat" id="weather-lat" value="{{$post->lat}}" />
                                    <input type="hidden" name="addr-long" id="weather-long" value="{{$post->lang}}" />
                                    <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                                    <input type="hidden" name="addr-name" id="weather-addr-name" value="{{$post->location}}" />
                                    <input type="submit" value="Click here for details" class="label label-success">
                                    {{ Form::close() }}
                                    @else
                                    <p>Currently there are no alerts for this location!!</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--suggestions end-->
                    </div>
                    <!--main-left-sidebar end-->
                </div>
            </div>
        </div>
        <!--forum-questions-sec end-->
    </div>
</section>
<div class="post-popup2 job_post apply_box">
    <div class="post-project">
        <h3>Apply</h3>
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
        <div class="row form-container form">

            <div class="form-one">
                <div class="post-comment-box">
                    <!-- <h3></h3> -->
                    <div class="user-poster">

                        <div class="post_comment_sec">
                            <form action="{{route('frontend.user.applyPost')}}" method="post">
                                @csrf
                                <div class="input-group">
                                    <textarea placeholder="Please specify your requirement" name="body" required></textarea>
                                    <input type="hidden" name="post_id" value="{{$post->post_id}}">
                                    <input type="hidden" name="isApply" value="1">
                                </div>
                                <br>
                                <label>Please select your Quantity</label> <span class="qnty_error hidden"></span>
                                <br>
                                <div class="input-group mr-15">

                           <!-- <input class="form-control" name="qnty" id="apply_post_qnty" data-qnty="{{$post->qnty}}" /> -->
                                    <select name="qnty" class="form-control">
                                        @for( $i=1;$i<=$post->qnty;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </div>

                                <button type="submit">Apply now</button>
                                <!-- <button type="submit">Close</button> -->
                                <div class="input-tx input-effect input-tx-com">


                                </div>
                            </form>
                        </div>
                        </form>
                    </div>
                    <!--post_comment_sec end-->
                </div>
                <!--user-poster end-->
            </div>
        </div>
    </div>

</div>

<div class="post-popup2 job_post post_close">
    <div class="post-project">
        <h3>Post Close</h3>
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
        <div class="row form-container form">
            <div class="form-one">
                <div class="post-comment-box">
                    <!-- <h3></h3> -->
                    <div class="user-poster">

                        <div class="post_comment_sec">
                            <form action="{{route('frontend.user.closePostInd')}}" method="post">
                                @csrf
                                <input type="hidden" name="post_apply_id" id="post_apply_id" value="">
                                <input type="hidden" name="is_reply_to_id" id="is_reply_to_id_close_post" value="">

                                <br>
                                <label>Please select your quantity</label> <span class="qnty_error hidden"></span>
                                <br>
                                <div class="input-group mr-15">

                                    <select class="form-control" name="qnty" required>
                                        <option value="">Select Quantity</option>
                                        @for($i=1;$i<=$post->qnty;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <!-- <input class="form-control" name="qnty" id="close_post_qnty" data-qnty="{{$post->qnty}}" /> -->
                                </div>

                                <button type="submit">Close now</button>
                                <div class="input-tx input-effect input-tx-com">


                                </div>
                            </form>
                        </div>
                        </form>
                    </div>
                    <!--post_comment_sec end-->
                </div>
                <!--user-poster end-->
            </div>
        </div>
    </div>
</div>

@endsection
@push('after-styles')
<style type="text/css">
    #map {
        width: 340px;
        min-height: 300px;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }

    .user-post-details h4 {
        margin-top: 10px;
        font-size: 20px;
        font-weight: 700;
    }

    .hidden {
        display: none;
    }

    .show-comment {
        margin-top: 10px;
        margin-left: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .btn-file {
        padding: 10px 0 15px;
        float: left;
    }

    .my-post a {
        text-align: right;
        padding: 10px 0px;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .disabled-div {
        pointer-events: none;
        /* for "disabled" effect */
        opacity: 0.5;
        background: #CCC;
    }

    .disabled-lnk {
        pointer-events: none;
        cursor: default;
        text-decoration: none;
        color: black;
    }

    .btnContact {
        float: right;
        margin-left: 10px;
    }

    .comments-box {
        display: none;
        margin-top: 1px;
    }

    .down {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }
</style>
@endpush
@push('after-scripts')
<!-- {!! script(theme_url('js/lightbox-plus-jquery.min.js')) !!} -->
<script type="text/javascript">
    function toggleShareButton() {
        $('.share_button').toggleClass('hidden');
    }

    $(function () {
        //radio box validation
        $("input[name$='help']").click(function () {
            var test = $(this).val();
            $("div.desc").hide();
            $("#hlp" + test).show();
        });
        //radio box validation

        $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]')
                .on('click', function (event) {
                    var $panel = $(this).closest('.panel-google-plus');
                    $comment = $panel.find('.panel-google-plus-comment');
                    $comment.find('.btn:first-child').addClass('disabled');
                    $comment.find('textarea').val('');
                    $panel.toggleClass('panel-google-plus-show-comment');
                    if ($panel.hasClass('panel-google-plus-show-comment')) {
                        $comment.find('textarea').focus();
                    }
                });
        $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function (event) {
            var $comment = $(this).closest('.panel-google-plus-comment');
            $comment.find('button[type="submit"]').addClass('disabled');
            if ($(this).val().length >= 1) {
                $comment.find('button[type="submit"]').removeClass('disabled');
            }
        });
    });

    $('.new-comment').on('click', function () {
        var id = $(this).data('id');
        $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function () {});
    });

    $('#show-comment').on('click', function () {
        console.log($(this).closest('.comments-box').html());
        $(this).closest('.comments-box').toggle();
    });

    $('.new-comment-box').on('click', function () {
        var id = $(this).data('id');
        $('.show-comment[data-id="' + id + '"]').slideToggle('2000', "swing", function () {});
    });

    function bindReplyButtonEvent() {
        $('.show-comment').on('click', '.new-comment-1', function () {
            var id = $(this).data('id');
            $('.show-comment-1 [data-id="' + id + '"]').toggleClass('hidden');
        });
    }

    bindReplyButtonEvent();

    $(document).ready(function () {
        //        $('[data-toggle="tooltip"]').tooltip();
        $('.com').trigger('click');
        $('.commentForm').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            var self = this;
            $.ajax({
                url: $(self).attr('action'),
                type: 'POST',
                data: formData,
                success: function (data) {
                    $(self)[0].reset();
                    $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function () {});
                    $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
                    console.log(data);
                },
                processData: false,
                contentType: false
            });
        });

        //Init map
        initPostDetailsMap('{{$post->lat}}', '{{$post->lang}}');
    });

    function applyPost() {
        $('#applyModal').modal('toggle');
    }

    function donatePost() {
        $('#donateModal').modal('toggle');
    }

    function closePost() {
        var r = confirm("Are you sure you want to close this post?");
        if (r == true) {
            $('#closePostSeeker').submit();
        }
    }
    function deletePost() {
        var r = confirm("Are you sure you want to delete this post?");
        if (r == true) {
            $('#deletePost').submit();
        }
    }

    function validClose(type = '') {
        var formId = 'closePost' + type;
        if ($("#" + formId + " input:checkbox:checked").length == 0) {
            alert('Please select at least one!!');
            return false;
        } else {
            $("#" + formId).submit();
    }
    }

    function viewContact(ele) {
        var email = $(ele).attr('data-email');
        var contact = $(ele).attr('data-contact');

        var html = '<b>Company Name:</b> ' + $(ele).attr('data-name') + '</br>';
        html += '<b>First Name:</b> ' + $(ele).attr('data-fname') + '</br>';
        html += '<b>Last Name:</b> ' + $(ele).attr('data-lname') + '</br>';
        html += '<b>Email:</b> <a href="mailto:' + email + '">' + email + '</a></br>';
        html += '<b>Contact:</b> <a href="tel:' + contact + '">' + contact + '</a></br>';

        $('#contact-details').html(html);

        $('#contactModal').modal('toggle');
    }

    function showComments(id) {
        $('#comments-box-' + id).slideToggle('slow');
    }

    var wrapper = $('#doc-container'); //Input field wrapper
    var fieldHTML = getFieldHTML();
    //Once add button is clicked
    $(wrapper).on('click', '.add_button', function (e) {
        $(wrapper).append(fieldHTML);
        $('#counter').val(parseInt($('#counter').val()) + 1);
    });
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parent('td').parent('tr').remove(); //Remove field html
        $('#counter').val(parseInt($('#counter').val()) - 1);
    });

    function getFieldHTML() {
        var fieldHTML = '<tr><td><textarea name="quest[title][]" class="form-control" placeholder="Enter your question here" required></textarea></td><td><select name="quest[option_type][]" class="form-control" required><option value="1">Textbox</option><option value="2">Dropdown List</option><option value="3">Radio button</option></select></td><td><textarea name="quest[answers][]" class="form-control" placeholder="Enter your answers here in comma separated form"></textarea></td><td><input type="checkbox" name="quest[enabled][]"></td><td><a href="javascript:void(0);" class="add_button" title="Add field"><i class="nav-icon fas fa-plus"></i></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="nav-icon fas fa-minus"></i></a></td></tr>';
        return fieldHTML;
    }
</script>
@endpush