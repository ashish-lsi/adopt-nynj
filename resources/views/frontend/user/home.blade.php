@extends('frontend.layouts.app')
@section('content')
<main>
    <div class="main-section">
        <div class="container">
            <div class="main-section-data">
                <div class="row">
                    <div class="col-lg-3 col-md-4 pd-left-none no-pd">
                        <div class="main-left-sidebar no-margin">
                            <div class="user-data full-width">
                                <div class="user-profile">
                                    <div class="username-dt">
                                        <div class="usr-pic">
                                            @if(auth()->user()->avatar_location)
                                            <img src="{{App\Helpers\Helper::getProfileImg($logged_in_user->avatar_location) }} " alt="">
                                            @else
                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" />
                                            @endif
                                        </div>
                                    </div>
                                    <!--username-dt end-->
                                    <div class="user-specs">
                                        <div class="sd-title">
                                            <h3>{{$logged_in_user->company_name}}</h3>
                                            <!-- <i class="la la-ellipsis-v"></i> -->
                                        </div>
                                    </div>
                                    <ul class="user-fw-status">

                                        <li class="no-bor">
                                            <a href="{{route('frontend.user.view-profile',['user'=>auth()->user()->id])}}" title="">View Profile</a>
                                        </li>
                                    </ul>
                                </div>
                                <!--user-profile end-->

                            </div>
                            <!--user-data end-->
                            @if(Module::find('Weather')->isEnabled())
                            <div class="item">
                                <div class="suggestions full-width">
                                    <div class="sd-title">
                                        <h3>Weather Search</h3>
                                    </div>
                                    <!--sd-title end-->
                                    <div class="suggestions-list suggestions-list-weather">
                                        {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                                        <div class="form-group sn-field">
                                            {{ html()->text('address')
                                                    ->class('form-control map-input')
                                                    ->id('weather-addr')
                                                    ->placeholder(__('Search your location..'))
                                                    ->attribute('maxlength', 191)
                                                    ->required() }}
                                            <span><i class="fa fa-location-arrow" id="get-location-arrow-weather"></i></span>
                                            <input type="hidden" name="City" id="weather-city" value="0" />
                                            <input type="hidden" name="State" id="weather-state" value="0" />
                                            <input type="hidden" name="Pincode" id="weather-pincode" value="0" />
                                            <input type="hidden" name="addr-lat" id="weather-lat" value="" />
                                            <input type="hidden" name="addr-long" id="weather-long" value="" />
                                            <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                                            <input type="hidden" name="addr-name" id="weather-addr-name" />

                                            <div id="map" class="d-none"></div>
                                            <input type="submit" value="Get Weather" class="btn weather-btn">
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                    <!--suggestions-list end-->
                                </div>
                            </div>
                            @endif
                            <!--suggestions end-->
                        </div>
                        <!--main-left-sidebar end-->
                    </div>
                    <div class="col-lg-6 col-md-8 no-pd">
                        <div class="main-ws-sec">
                            <div class="post-topbar">
                                <div class="user-picy">
                                    <img src="images/resources/user-pic.png" alt="">
                                </div>
                                <div class="post-st">
                                    <ul>
                                        <li><a class="post_project" href="#" title="">Post a Project</a></li>
                                        <li><a class="post-jb active" href="#" title="">Post a Job</a></li>
                                    </ul>
                                </div>
                                <!--post-st end-->
                            </div>
                            <!--post-topbar end-->
                            <div class="posts-section">

                                <!-- <div class="clearfix"></div> -->
                                <div id="postList" class="home-podcast">
                                    @include('frontend.includes.postCompact')
                                </div>
                                <!--post-bar end-->

                                <!--post-bar end-->

                                <div class="process-comm hidden">
                                    <div class="spinner">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                </div>
                                <!--process-comm end-->
                            </div>
                            <!--posts-section end-->
                        </div>
                        <!--main-ws-sec end-->
                    </div>
                    <div class="col-lg-3 pd-right-none no-pd">
                        <div class="right-sidebar">

                            <div class="filter-secs">
                                <div class="sd-title">
                                    <h3>Find Post</h3>
                                </div>
                                <!--filter-heading end-->
                                <form action="{{route('frontend.user.home')}}">
                                    <div class="paddy">
                                        <div class="filter-dd">
                                            <input type="text" name="query" value="{{Input::get('query')}}" placeholder="Search" autocomplete="off">
                                        </div>
                                        <div class="filter-dd">
                                            <ul class="avail-checks">
                                                <li>
                                                    <input type="radio" name="type" value="0" id="helpseeker" {{Input::get('type') == 0 ? 'checked' : ''}}>
                                                    <label for="helpseeker">
                                                        <span></span>
                                                    </label>
                                                    <small>Help Seeker</small>
                                                </li>
                                                <li>
                                                    <input type="radio" name="type" value="1" id="providing_help" {{Input::get('type') == 1 ? 'checked' : ''}}>
                                                    <label for="providing_help">
                                                        <span></span>
                                                    </label>
                                                    <small>Providing help</small>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="filter-dd filter-dd-select">
                                            <select name="category[]" class="select-text chkmore" multiple="multiple">
                                                @foreach ($categories as $category)
                                                <option value="{{$category->category_id}}" @if(Input::get('category')) {{in_array($category->category_id, Input::get('category')) ? 'selected' : ''}} @endif> {{ $category->name }} </option>
                                                @endforeach
                                            </select>
                                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                        </div>
                                        <div class="filter-dd">
                                            <input type="text" value="{{Input::get('state')}}" placeholder="State" name="state">
                                        </div>
                                        <div class="filter-dd">
                                            <input type="text" value="{{Input::get('city')}}" placeholder="City" name="city">

                                        </div>
                                        <div class="filter-dd">
                                            <input type="text" name="pincode" placeholder="Zipcode" value="{{Input::get('pincode')}}">
                                        </div>
                                        <div class="">
                                            <div class="fl_lft">
                                                <button type="submit" class="btn btn-primary weather-btn mr-left">Submit</button>
                                            </div>
                                            <div class="fl_lft">
                                                <input type="reset" class="btn btn-primary weather-btn" value="Reset">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            @if(count($news))
                            <div class="widget widget-jobs">
                                <div class="sd-title">
                                    <h3>News and Alerts</h3>
                                </div>
                                @foreach($news as $new)
                                <div class="news_b clearfix">
                                    <a href="{{route('frontend.alertsView',$new->id)}}" class="kn_more">
                                        <div class="jobs-list">
                                            <div class="ed-opts">
                                                <span class="badge {{($new->type == 1 ? 'badge-primary' : ($new->type == 2 ? 'badge-secondary' : ($new->type == 3 ? 'badge-tiranry' : '')) ) }}"> {{($new->type == 1 ? 'Alerts' : ($new->type == 2 ? 'Announcement' : ($new->type == 3 ? 'Success Stories' : '')) ) }}</span>
                                            </div>
                                            <div class="pdf_data tab_data">
                                                <h6>{{$new->created_at->diffForHumans()}}</h6>
                                                <h3>{{$new->title}}</h3>
                                                <p>{{strip_tags($new->content)}}</p>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                @endforeach
                            </div>
                            @endif
                            <!--widget-jobs end-->


                        </div>
                        <!--right-sidebar end-->
                    </div>
                </div>
            </div><!-- main-section-data end-->
        </div>
    </div>
</main>




<div class="post-popup pst-pj">
    <div class="post-project">
        <h3>Post a project</h3>
        <div class="post-project-fields">
            <form>
                <div class="row">
                    <div class="col-lg-12">
                        <input type="text" class="form-control" name="title" placeholder="Title">
                    </div>
                    <div class="col-lg-12">
                        <div class="inp-field">
                            <select>
                                <option>Category</option>
                                <option>Category 1</option>
                                <option>Category 2</option>
                                <option>Category 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <input type="text" class="form-control" name="skills" placeholder="Skills">
                    </div>
                    <div class="col-lg-12">
                        <div class="price-sec">
                            <div class="price-br">
                                <input type="text" name="price1" placeholder="Price" class="form-control">
                                <i class="la la-dollar"></i>
                            </div>
                            <span>To</span>
                            <div class="price-br">
                                <input type="text" name="price1" placeholder="Price" class="form-control">
                                <i class="la la-dollar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <textarea name="description" placeholder="Description" class="form-control"></textarea>
                    </div>
                    <div class="col-lg-12">
                        <ul>
                            <li><button class="active" type="submit" value="post">Post</button></li>
                            <li><a href="#" title="">Cancel</a></li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
        <!--post-project-fields end-->
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
    </div>
    <!--post-project end-->
</div>
<!--post-project-popup end-->


<!--post-project-popup end-->

@stop
@push('after-scripts')
{!! script(theme_url('js/lightbox-plus-jquery.min.js')) !!}
<script type="text/javascript">
    $('#frmWeather').submit(function (e) {
        var country = $('#weather-country').val();
        var addr_lat = $('#weather-lat').val();

        if (country != 'United States') {
            alert('Only United States locations are allowed to register!!');
            $('#weather-addr').focus();
            return false;
        }

        if (addr_lat == '') {
            alert('Please select a valid location!!');
            $('#weather-addr').focus();
            return false;
        }
    });

    $('#frmCreatePost').submit(function (e) {
        var country = $('#addr-country').val();
        var addr_lat = $('#addr-lat').val();

        if (country != 'United States') {
            alert('Only United States locations are allowed to register!!');
            $('#address').focus();
            return false;
        }

        if (addr_lat == '') {
            alert('Please select a valid location!!');
            $('#address').focus();
            return false;
        }
    });
</script>

<script>
    var loc = location.href;
    if (loc.indexOf("?") === -1)
        loc += "?";
    else
        loc += "&";
    var currentUrl = loc;
    var page = 1;
    var noMorePost = false;
    $(window).scroll(function () {
        // console.log($(window).scrollTop() + $(window).height() );
        var scrollHeight = $(window).scrollTop() + $(window).height();
        if (scrollHeight + 1 >= $(document).height() && !noMorePost) {
            //Your code here\
            $('.process-comm').removeClass('hidden');
            $.ajax({
                url: currentUrl + 'page=' + (page + 1),
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if (data.count != 0) {
                        page++;
                        $('#postList').append(data.html);
                        $('.process-comm').addClass('hidden');
                    } else {
                        // console.log(data.hmtl);
                        $('#postList').append(data.html);
                        $('.process-comm').addClass('hidden');
                        noMorePost = true;
                    }

                    //console.log(data);
                }
            })
        }
    });
</script>
@endpush