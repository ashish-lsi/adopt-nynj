@extends('frontend.layouts.app')

@section('content')
<section class="cover-sec">
    <div class="profile_banner">
        <img src="/adoptFarm/images/resources/cover-img.jpg" alt="">

        <div class="user-pro-img clearfix">
            @if($user->avatar_location != "" || $user->avatar_location != null)
            <img src="{{App\Helpers\Helper::getProfileImg($user->avatar_location)}}" alt="avatar">
            @else
            <img src="/adoptFarm/images//avatar_2x.png" alt="avatar">
            @endif
        </div>

        <div class="edit-pr-box">
            <div class="container">
                <div class="pr-details">
                    <div class="pr-d-left">
                        <h2>
                            <a href="{{route('frontend.user.profile')}}">
                                {{$user->company_name}}
                            </a>
                        </h2>
                        <!--<label for="">{{$user->company_name}}</label>-->
                    </div>
                    @if($user->id == $loggedInUser->id)
                    <div class="edit-icon"> 
                        <a href="{{route('frontend.user.account')}}" class="btn btn-primary edit_pr"> 
                            Edit Profile
                        </a> 
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="profile_us_details clearfix">
        <div class="container">
            <div class="profile_us_tab">
                <ul class="nav nav-tabs" role="tablist" id="tabs1">
                    <li class="active" role="presentation"><a role="tab" data-toggle="tab" href="#profile" aria-controls="profile" class="active">Profile</a></li>
                    @if($user->id == $loggedInUser->id)
                    <li role="presentation"><a role="tab" data-toggle="tab" href="#address" aria-controls="address">Company Address</a></li>
                    <li role="presentation"><a role="tab" data-toggle="tab" href="#interact" aria-controls="interact">My Interactions</a></li>
                    <li role="presentation"><a role="tab" data-toggle="tab" href="#awards" aria-controls="awards">Awards & Achievements</a></li>
                    @endif
                </ul>

                @if ($message = Session::get('flash_success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>

                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="tab-content">

        <!-- ******************** Profile tab ******************** --> 
        <div class="tab-pane fade in active show" id="profile" role="tabpanel">
            <section class="profile-account-setting show_profile">
                <div class="container">
                    <div class="account-tabs-setting">
                        <div class="row">
                            <div class="col-lg-4 ac-col-lg-4 col-sm-12">
                                <div class="suggestions full-width">
                                    <div class="sd-title">
                                        <h3>User Details</h3>
                                    </div><!--sd-title end-->
                                    <div class="suggestions-list suggestions-list-profile">
                                        <div class="form-group">
                                            <div class="company_detail">
                                                <div class="details">
                                                    <label>User name</label> <span class="span_col">:</span>
                                                    <span>{{$user->full_name ?? '-'}}</span>
                                                </div>
                                                @if($user->id == $loggedInUser->id)

                                                @if($user->contact)
                                                <div class="details">
                                                    <label>Contact no</label> <span class="span_col">:</span>
                                                    <span><a href="tel:{{$user->contact}}"> {{$user->contact}} </a></span>
                                                </div>
                                                @endif

                                                @if($user->email)
                                                <div class="details">
                                                    <label>Email  </label> <span class="span_col">:</span>
                                                    <span><a href="mailto: {{$user->email}}"> {{$user->email}} </a></span>
                                                </div>
                                                @endif

                                                @endif

                                                @if($user->website)
                                                <div class="details">
                                                    <label>Website</label> <span class="span_col">:</span>
                                                    <span><a href="{{$user->website}}" target="_blank"> {{$user->website}} </a> </span>
                                                </div>
                                                @endif

                                                @if ($user->diversity_id != "")
                                                <div class="details">
                                                    <label>Diversity  Type</label> <span class="span_col">:</span>
                                                    <span>{{$user->diversityType->name}}</span>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div><!--suggestions-list end-->
                                </div>
                                <div class="suggestions full-width">
                                    <div class="sd-title">
                                        <h3>About Company</h3>
                                    </div><!--sd-title end-->
                                    <div class="suggestions-list suggestions-list-profile">
                                        <div class="form-group">
                                            <div class="company_detail">
                                                <div class="details">
                                                    <p>{{$user->description ?? '--'}} </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--suggestions-list end-->
                                </div>
                            </div>

                            <div class="col-lg-8">
                                <div class="main-ws-sec">
                                    <div class="posts-section">
                                        @include('frontend.includes.postCompact')
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div><!--account-tabs-setting end-->
                </div>
            </section>
        </div>
    
    

        @if($user->id == $loggedInUser->id)
        <!-- ******************** Company Address tab ******************** --> 
        <div class="tab-pane fade" id="address" role="tabpanel">
            <div class="container">
                <div class="account-tabs-setting">
                    <div class="row">
                        @if($userAddr)
                        <div class="col-lg-4 ac-col-lg-4">
                            <div class="suggestions full-width">
                                <div class="sd-title">
                                    <h3>Location details</h3>
                                </div><!--sd-title end-->
                                @if(is_object($userAddr) && count($userAddr)>0)
                                @foreach($userAddr as $k => $addr)
                                <div class="suggestions-list suggestions-list-profile" style="border-bottom: 1px solid #cecece;">
                                    <div class="form-group">
                                        <div class="company_detail">
                                            <div class="details">
                                                <label>Address</label> <span class="span_col">:</span>
                                                <span>{{$addr->address}}</span>
                                            </div>
                                            <div class="details">
                                                <label>City</label> <span class="span_col">:</span>
                                                <span>{{$addr->City}}</span>
                                            </div>
                                            <div class="details">
                                                <label>State</label> <span class="span_col">:</span>
                                                <span>{{$addr->State}}</span>
                                            </div>
                                            <div class="details">
                                                <label>Pincode</label> <span class="span_col">:</span>
                                                <span>{{$addr->Pincode}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="main-ws-sec">
                                <!--post-topbar end-->
                                <div class="posts-section">
                                    <div class="suggestions full-width">
                                        <div class="sd-title">
                                            <h3>Locate on map</h3>
                                        </div><!--sd-title end-->
                                        <div class="suggestions-list suggestions-list-profile">
                                            <div id="map"></div>
                                        </div><!--suggestions-list end-->
                                    </div>
                                </div><!--posts-section end-->
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div><!--account-tabs-setting end-->
        </div>

        <!-- ******************** My Interactions tab ******************** --> 
        <div class="tab-pane fade" id="interact" role="tabpanel">
            @php
            $posts = $interactions
            @endphp
            <div class="container">
                <div class="account-tabs-setting">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="main-ws-sec">
                                <div class="posts-section">
                                    @include('frontend.includes.postCompact')
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="awards" role="tabpanel">
            <div class="container">
                <div class="account-tabs-setting">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="edit-icon edit-icon-awards"> 
                                <a href="#" class="btn btn-primary edit_pr"> 
                                    Create new
                                </a> 
                            </div>
                        </div>
                        @if(count($achievements))
                        <div class="col-lg-4">
                            @foreach($achievements as $k => $achievement)
                            <div class="post-bar awards-edit" data-related="award{{$achievement->id}}" <?= $k == 0 ? 'style="background: rgb(255, 255, 255);"' : '' ?>>
                                <a href="javascript:void(0)">
                                    <div class="post_topbar">
                                        <div class="usy-dt">
                                            @if( $logged_in_user->avatar_location != "")
                                            <img class="profile" src="{{asset('public/storage/'. $logged_in_user->avatar_location) }}" alt="avatar">
                                            @else
                                            <img class="profile" src="https://image.ibb.co/jw55Ex/def_face.jpg" alt="avatar">
                                            @endif

                                            <div class="usy-name">
                                                <h3>{{$achievement->title}}</h3>
                                                <span>{{$achievement->description}}</span>
                                            </div>
                                        </div>
                                        <div class="ed-opts">
                                            <span class="badge badge-secondary">{{$achievement->type == 0 ? 'Award' : 'Achievement'}}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>

                        <div class="col-lg-8">
                            <div class="main-ws-sec  main-ws-sec-awards">
                                <div class="posts-section">
                                    @foreach($achievements as $achievement)
                                    <div class="post-bar" id="award{{$achievement->id}}">
                                        <div class="usy-dt">
                                            @if( $logged_in_user->avatar_location != "")
                                            <img class="profile" src="{{asset('public/storage/'. $logged_in_user->avatar_location) }}" alt="avatar">
                                            @else
                                            <img class="profile" src="https://image.ibb.co/jw55Ex/def_face.jpg" alt="avatar">
                                            @endif
                                            <div class="usy-name">
                                                <h3>{{$achievement->title}}</h3>
                                                <span><img src="images/clock.png" alt="">{{$achievement->created_at->diffForHumans()}}</span>
                                            </div>
                                        </div>
                                        <div class="post_topbar">
                                            <div class="ed-opts">
                                                <span class="badge badge-secondary">{{$achievement->type == 0 ? 'Award' : 'Achievement'}}</span>
                                                <a href="#" title="share" class="ed-opts-open"> <i class="la la-share-alt"></i> </a>
                                                <ul class="ed-options">
                                                    <li><a href="#" title=""> <i class="la la-facebook"></i> Facebook</a></li>
                                                    <li><a href="#" title=""><i class="la la-twitter-square"></i> Twitter</a></li>
                                                    <li><a href="#" title=""> <i class="la la-linkedin"></i> Linkedin</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="job_descp">
                                            <p>{{$achievement->description}}</p>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="awards_img">
                                            <ul class="like-com">
                                                <li>
                                                    @if($achievement->img != "")
                                                    <a class="example-image-link" target="_blank" href="{{ asset('public/storage/'.$achievement->img) }}">
                                                        <img src="{{ asset('public/storage/'.$achievement->img) }}">
                                                    </a>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col-lg-12">
                            <div class="post-bar">
                                <div class="post_topbar">
                                    Currently there are no awards to display
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="post-popup awards-popup">
            <div class="post-project">
                <h3>Awards & Achievements</h3>
                <div class="row form-container form">
                    <label class="form-control-label error-label">* Marked fields are required!!</label>
                    <form method="post" role="form" class="form-horizontal" action="{{route('frontend.user.achievements')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-one">
                            <div class="input-tx input-effect">
                                <input class="effect-16" type="text" name="title" placeholder="Title *" required>
                            </div>

                            <div class="input-tx input-effect">
                                <textarea class="effect-17" type="text" name="description"  placeholder="Description *" required></textarea>
                            </div>
                            <div class="input-tx input-effect">
                                <div class="seeking_help_select seeking_help_awards">
                                    <select name="type" class="select-text" required>
                                        <option value="">Select type</option>
                                        <option value="0">Awards</option>
                                        <option value="1">Achievements</option>
                                    </select>
                                    <span class="select-bar"></span>
                                </div>
                            </div>

                            <div class="input-tx input-effect input-effect-awards">
                                <div class="form-group">
                                    <div class="cv_upload ">
                                        <span class="wpcf7-form-control-wrap file-334">
                                            <input type="file" name="img" size="40" class="wpcf7-form-control wpcf7-file form-control" tabindex="6" aria-invalid="false"></span>
                                        <small></small>
                                        <label for="file-1">
                                            <span>upload</span>
                                            <strong>choose a file</strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="submit_btn submit_btn_awards">
                                <button type="submit" class="btn btn-primary weather-btn weather-btn-2">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <a href="#" title=""><i class="la la-times-circle-o"></i></a>
            </div>
            <!--post-project end-->
        </div>


        @endif
</section>

<style type="text/css">
    #map{
        min-height: 300px;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }

    li{
        list-style: none;
    }
</style>
@endsection

@push('after-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var locations = $.parseJSON('{!!json_encode($userAddr)!!}');
        $('script[src*="lightbox-plus-jquery.min.js"]').remove();

        //Init map
        showCompanyLocations(locations);
    });


    $(function () {
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollurl = $('body').scrollTop() || $('html').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollurl);
        });
    });

</script>
@endpush
