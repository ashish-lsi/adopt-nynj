@if(count($posts)> 0)
@foreach($posts as $post)
<div class="post-bar">
    <div class="post_topbar">
        <div class="usy-dt">
            <a href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}">
                <img class="profile" src="{{App\Helpers\Helper::getProfileImg( $post->user->avatar_location) }}" alt="">
            </a>
            <div class="usy-name">
                <h3> <a href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}">{{$post->user->company_name}} </a></h3>
                <span><img src="images/clock.png" alt="">{{($post->created_at) ? $post->created_at->diffForHumans() : ''}}</span>
            </div>
        </div>
        <div class="ed-opts">
            <span class="badge {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}"> {{($post->type==0)? __('Help Seeker'):__('Help Provider')}}</span>
            @if($post->status == 1)
            <span class="badge badge-tiranry">Closed</span>
            @endif
            <a href="#" title="share" class="ed-opts-open"> <i class="la la-share-alt"></i> </a>
            <ul class="ed-options">
		{!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), $post->title,['target' => '_blank'])
                ->facebook()
                ->twitter()
                ->linkedin()
                !!}
            </ul>
        </div>
    </div>
    <div class="epi-sec">
        <ul class="descp">
            <li><img src="images/icon8.png" alt=""><span>Epic Coder</span></li>
            <li><img src="images/icon9.png" alt=""><span>India</span></li>
        </ul>
        <ul class="bk-links">
            <li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
            <li><a href="#" title=""><i class="la la-envelope"></i></a></li>
        </ul>
    </div>
    <div class="job_descp">
        <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id])}}">
            <h3>{{$post->title}}</h3>
        </a>
        <ul class="job-dt">
            <li><a href="#" title="">Full Time</a></li>
            <li><span>$30 / hr</span></li>
        </ul>
        <p>Quantity: {{$post->qnty}}</p>
        <p class="job_descp_home">{{ $post->article }}
        <div class="collapse" id="viewdetails">
            <div class="post_detail-type">
                <div class="details">
                    <label>Category </label> <span class="span_col">:</span>
                    @if(isset($post->category->name))
                    <span>
                        {{$post->category_id != 11 ? $post->category->name : $post->category_name}}
                    </span>
                    @endif

                </div>
                <div class="details">
                    <label>Location </label> <span class="span_col">:</span>
                    @if($post->type != 2)
                    <span>
                        <!-- {{$post->location}} -->
                        {{ ($post->state) ? $post->state : "" }}
                        {{ ($post->city) ? ", ".$post->city : "" }}
                        {{ ($post->pincode) ? ", ".$post->pincode : "" }}
                    </span>
                    @endif
                </div>

            </div>
        </div>
        <ul class="like-com">
            <li>
                @foreach($post->attachements as $attach)
                @if(in_array($attach->extension,['jpg','jpeg','png']))
                <a class="example-image-link" href="{{asset('public/storage/'.$attach->location )}}" data-lightbox="example-1">
                    <img src="{{asset('public/storage/thumb/'.$attach->thumb )}}">
                </a>
                @else
                <a class="example-image-link" target="_blank" href="{{asset('public/storage/'.$attach->location )}}">
                    <img src="{{asset('public/img/'.$attach->extension.'.png' )}}">
                </a>
                @endif
                @endforeach
            </li>
        </ul>
    </div>
    <div class="job-status-bar">
        <ul class="like-com">
            <input type="hidden" id="comment_loaded{{$post->post_id}}" value="0">
            <li><a data-id="{{$post->post_id}}" class="com"><i class="fa fa-comment-alt"></i> Comments</a></li>
        </ul>
        @if(auth()->user()->id != $post->company_id)
        @endif
    </div>

    <div class="comment_box cb{{$post->post_id}} clearfix">

        <div class="col-lg-1 col-md-2 fl_lft">
            <div class="comment_icon"><img src="images/ny-img1.png" alt=""> </div>
        </div>
        @php
        $comments = [];
        if(auth()->user()->id != $post->company_id)
        $comments = $post->comments()->with('user','replies','replies.user')->where('company_id',auth()->user()->id)->get();
        elseif(auth()->user()->id == $post->company_id)
        $comments = $post->comments()->with('user','replies','replies.user')->where('post_id',$post->post_id)->get();
        @endphp
        <div class="col-sm-12">
            <div class="comment-section">
                <div class="comment-sec">
                    <div class="tab-pane  active" id="comments-{{$post->post_id}}" role="tabpanel" aria-labelledby="home-tab"><br>
                    </div>
                </div>
                <!--comment-sec end-->
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
@endforeach
@else
<div class="post-bar">
    Currently there are no active posts to display
</div>
@endif