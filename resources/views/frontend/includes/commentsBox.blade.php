<div>
    @foreach($comments as $comment)
    @php
    $reply_count = count($comment->replies);
    @endphp
    @php
    $user_post_apply = App\Models\UserPostApply::where(['post_id'=>$post->post_id,'user_id'=>$comment->user->id])->first();

    @endphp
    <div class="row">
        <div class="col-md-1 comment-profile no-pdd">
            @if($comment->user->avatar_location)
            <img src=" {{App\Helpers\Helper::getProfileImg($comment->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
            @else
            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
            @endif

        </div>
        <div class="col-md-10 comment-profile no-pdd">
            <div class="comment-text">
                <div class="clearfix disfx">
                    <div class="fr_left">
                        <a href="{{route('frontend.user.view-profile',['user'=>$comment->user->id])}}"><strong>{{$comment->user->company_name}}</strong></a>

                        <span><b>Quantity :</b> {{$user_post_apply->qnty ?? ''}}</span> <span class="sub_comm">{!!$comment->comment!!}</span>
                    </div>

                    @if($user_post_apply && $user_post_apply->approved === 0 && auth()->user()->id != $post->company_id)
                    <a class="btn btn-primary  createpost-btn" >Waiting For Approval</a>
                    @elseif($user_post_apply && $user_post_apply->approved === 0 && auth()->user()->id == $post->company_id)
                    <div class="fr_right">
                        <form action="{{route('frontend.user.approve')}}" method="post">
                            @csrf
                            <input type="hidden" name="post_apply_id" value="{{$user_post_apply->id}}">
                            <input type="hidden" name="is_reply_to_id" value="{{$comment->comment_id}}">

                            <input type="submit" class="btn btn-sm btn-success" style="float:right" value="Approve">
                        </form>
                    </div>
                    @elseif($user_post_apply && $user_post_apply->help_provided == 0 && auth()->user()->id == $post->company_id)
                    <a class="btn btn-primary postClose createpost-btn" data-post_id="{{$post->post_id}}" data-max_qnty='{{$post->qnty}}' data-is_reply_to_id="{{$comment->comment_id}}" data-post_apply_id="{{$user_post_apply->id}}">Close Post</a>
                    @elseif($user_post_apply && $user_post_apply->approved == 1 && $user_post_apply->help_provided == 0)
                    <a class="btn btn-primary  createpost-btn" >Approved</a>
                    @elseif($user_post_apply && $user_post_apply->approved == 1 && $user_post_apply->help_provided == 1)
                    <a class="btn btn-primary badge createpost-btn" >Closed</a>

                    @endif
                </div>

                @if($comment->attachment != "")
                <span><i class="fa fa-paperclip"></i> <a href="/download/{{$comment->attachment}}">{{$comment->attachment}} </a></span>
                @endif
            </div>

            <div class="comment-tool clearfix">
                <a class=" btn  ml-2 new-comment" data-id="{{$comment->comment_id}}"> <i class="fa fa-eye"></i>
                    {{$reply_count}} Reply</a>
                @if($post->status == 0)
                {{-- @if( auth()->user()->id == $post->company_id) --}}
                <a class=" btn  ml-2 new-comment-box" data-id="R{{$comment->comment_id}}"> <i class="fa fa-reply"></i> Reply</a>
                {{-- @endif --}}
                @endif
                <a class=" btn  ml-2" data-toggle="tooltip" title="Mon, June 13, 2019 at 4:28 pm"> <span class="fa fa-clock-o"> </span>
                    {{$comment->created_at->diffForHumans()}}</a>
            </div>

            <div class="show-comment hidden" data-id="R{{$comment->comment_id}}">
                <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                    <div class="input-group ">
                        <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                        @csrf
                        <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                        <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                        <input type="hidden" name="reply_user_id" value="{{ $comment->company_id }}" />
                        <!-- <span class="btn-file">
                                                    <input type="file" name="attach">
                                                </span> -->
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-sm weather-btn"><span class="glyphicon glyphicon-comment"></span> Comment</button>
                        </span>
                    </div>
                </form>
            </div>

            <div class="show-comment hidden" data-id="{{$comment->comment_id}}">

                @include('frontend.includes.replyBox')
            </div>
        </div>
    </div>
    @endforeach
</div>