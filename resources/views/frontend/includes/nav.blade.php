

<header>
    <div class="container no-pdd">
        <div class="header-data">
            <div class="logo">
                <a href="{{route('frontend.user.dashboard')}}" title=""><img src="{{ theme_url('images/TTI-Adopt-a-Company-logo.png')}}" alt=""></a>
            </div>
            <!--logo end-->

            @auth
            <div class="nav-bar">
                <div class="search-bar">
                    <form action="{{route('frontend.user.home')}}">
                        <div class="category-container search-d">
                            <div class="search-dropdown">
                                <div class="search-w">
                                    <div class="filter-secs-sec">
                                        <div class="paddy">
                                            <span><i class="fa fa-times"></i></span>
                                            <div class="filter-dd">
                                                <ul class="avail-checks">
                                                    <li>
                                                        <input type="radio" name="type" id="helpseeker_type" value="0"  {{Input::get('type') == 0 ? 'checked' : ''}}>
                                                        <label for="helpseeker_type">
                                                            <span></span>
                                                        </label>
                                                        <small>Help Seeker</small>
                                                    </li>
                                                    <li>
                                                        <input type="radio" id="providinghelp_type" name="type" value="1" {{Input::get('type') == 1 ? 'checked' : ''}}>
                                                        <label for="providinghelp_type">
                                                            <span></span>
                                                        </label>
                                                        <small>Providing help</small>
                                                    </li>
                                                    <div class="clearfix"></div>
                                                </ul>
                                            </div>
                                            <div class="filter-dd">
                                                <select name="category[]" class="select-text" >
                                                    <option value="" selected> Select Category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{ $category->category_id }}" @if(Input::get('category')) {{in_array($category->category_id, Input::get('category')) ? 'selected' : ''}} @endif> {{ $category->name }} </option>
                                                    @endforeach
                                                </select>
                                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                            </div>
                                            <div class="filter-dd">
                                                <input type="text" name="state" value="{{Input::get('state')}}" placeholder="State">
                                            </div>
                                            <div class="filter-dd">
                                                <input type="text" name="city" value="{{Input::get('city')}}" placeholder="City">

                                            </div>
                                            <div class="filter-dd">
                                                <input type="text" name="pincode" placeholder="Zipcode" value="{{Input::get('pincode')}}">

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="fl_lft search-btn-g clearfix">
                                                <button class="btn btn-primary weather-btn mr-left">Search</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="query" placeholder="Search..." autocomplete="off" value="{{Input::get('query')}}">
                            <i class="fa fa-ellipsis-h"></i>
                            <button type="submit"><i class="la la-search"></i></button>
                        </div>
                    </form>
                </div>
                <nav>
                    <ul>
                        <li>
                        <button class="btn btn-primary createpost-btn post-jb">Create a Post</button> 
							<a href="{{route('frontend.user.dashboard')}}" title="">	 <i class="fa fa-home"></i> </a>
                        </li>
                        <li>
                            <a href="#" title="" class="not-box-open">
                                <span>
                                    <i class="fa fa-bell">
                                    </i>
                                    <div class="notificaction" id="notificaction_count"> {{$notification_count}}</div>
                                </span>
                            </a>
                            <div class="notification-box clearfix noti" id="notification">
                                <div class="nt-title">
                                    <h4>Notifications</h4>
                                    <a href="#" title="" class="mark-read">Mark All as Read</a>
                                </div>
                                <div class="nott-list notification-container mCustomScrollbar">

                                    @if ($notification_count > 0)
                                    @foreach ($notifications as $notification)
                                    <div class="notfication-details">
                                        <a href="{{route('frontend.user.viewNotification',[$notification->id])}}">
                                            <div class="noty-user-img">
                                                @if($notification->user->avatar_location != "")
                                                <img src="{{App\Helpers\Helper::getProfileImg( $notification->user->avatar_location) }}" class="img img-rounded img-fluid" />
                                                @else
                                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
                                                @endif
                                            </div>
                                            <div class="notification-info">

                                                <h3>

                                                    {{$notification->user->company_name}}


                                                    @if($notification->comment != null)
                                                    Commented on your post 
                                                    @endif

                                                    <b>{{$notification->post->title}}</b>

                                                </h3>
                                                <span>{{$notification->created_at->diffForHumans()}}</span>
                                        </a>                                        </div>
                                    <!--notification-info -->
                                    </a>
                                </div>
                                @endforeach
                                @else
                                <div class="notfication-details">
                                    <div class="notification-info">
                                        <h3>Currently there are no new notifications in your account.</h3>
                                    </div>
                                    <!--notification-info -->
                                </div>
                                <!--notfication-details end-->
                                @endif
                            </div>
                            <!--nott-list end-->
                            <div class="view-all-nots">
                                <a href="{{route('frontend.user.notification')}}" class="text-center bold600">View All Notifications</a>
                            </div>
                            </div>
                            <!--notification-box end-->
                        </li>
                    </ul>
                </nav>
                <!--nav end-->
                <div class="menu-btn">
                    <a href="#" title=""><i class="fa fa-bars"></i></a>
                    <div class="nav-account-settings nav-account-settings-con">
                        <div class="nav_create_weather"></div>

                    </div>
                </div>
                <!--menu-btn end-->
                <div class="user-account">
                    <div class="user-info">
                        @if( $logged_in_user->avatar_location != "")
                        <img src="{{App\Helpers\Helper::getProfileImg( $logged_in_user->avatar_location) }}" alt="avatar">
                        @else
                        <a href="#">   <img src="https://image.ibb.co/jw55Ex/def_face.jpg" alt="avatar"></a>
                        @endif
                        <a href="#" title="{{ $logged_in_user->first_name }}">{{ $logged_in_user->first_name }}</a>
                        <i class="la la-sort-down"></i>
                    </div>
                    <div class="user-account-settingss" id="users">
                        <ul class="us-links">
                            <li><a href="{{route('frontend.user.profile')}}">{{__('Profile')}}</a></li>
                            <li><a href="{{route('frontend.user.profile')}}">{{__('My Post')}}</a></li>
                            @if ($logged_in_user->active == 1)
                            <li><a href="{{route('frontend.user.profile')}}#interact">{{__('My Interactions')}}</a></li>
                            @endif
                        </ul>
                        <h3 class="tc"><a href="{{route('frontend.auth.logout')}}">{{__('Logout')}}</a></h3>
                    </div>
                </div>
                <div class="weather-search">
                    <div class="weather-info">
                        <a href="#" title="">
                            <div id="openweathermap-widget-13"></div>
                        </a>
                        <i class="la la-sort-down"></i>
                    </div>
                    <div class="weather-setting" id="users1">
                        <div class="suggestions-list suggestions-list-weather">
                            {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeatherNav')) }}
                            <div class="form-group sn-field">
                                {{ html()->text('address')
                                    ->class('form-control map-input')
                                    ->id('weather-addr-nav')
                                    ->placeholder(__('Search your location..'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                                <!--<span><i class="fa fa-location-arrow" id="get-location-arrow-weather"></i></span>-->

                                <input type="hidden" name="City" id="weather-city-nav" value="0" />
                                <input type="hidden" name="State" id="weather-state-nav" value="0" />
                                <input type="hidden" name="Pincode" id="weather-pincode-nav" value="0" />
                                <input type="hidden" name="addr-lat" id="weather-lat-nav" value="" />
                                <input type="hidden" name="addr-long" id="weather-long-nav" value="" />
                                <input type="hidden" name="addr-country" id="weather-country-nav" value="United States" />
                                <input type="hidden" name="addr-name" id="weather-addr-name-nav" />
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            @else
            <!-- <div class="nav-bar">
            <div class="profile_us_tab" style="float: right">
                <ul class="nav nav-tabs">
                    <li class="active ml-5">
                        <a href="{{route('frontend.about')}}" class="active"> <i class="fa fa-info-circle"> </i> {{__('About Us')}}  </a>
                    </li>
                    <li class="active ml-2">
                        <a href="{{route('frontend.auth.login')}}" class="active"> <i class="fa fa-user"> </i> {{__('Login')}}  </a>
                    </li>
                    <li class="active ml-2">
                        <a href="{{route('frontend.auth.register')}}" class="active"> <i class="fa fa-user-plus"> </i> {{__('Sign up')}}   </a>
                    </li>
                </ul>
            </div>
        </div> -->
            @endauth
        </div>
    </div>
</header>
<!--header end -->