@foreach($comment->replies as $reply)
                                            <div class="card card-inner">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-1 comment-profile no-pdd">
                                                            @if($reply->user->avatar_location)
                                                            <img src=" {{App\Helpers\Helper::getProfileImg($reply->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                                                            @else
                                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                                                            @endif
                                                        </div>
                                                        <div class="col-md-11 comment-profile no-pdd">
                                                            <div class="comment-text">
                                                              <a href="{{route('frontend.user.view-profile',['user'=>$reply->user->id])}}"><strong>{{$reply->user->company_name}}</strong></a>

                                                                <span>{!!$reply->comment!!}</span>
                                                                @if($reply->attachment != "")
                                                                <span><i class="fa fa-paperclip"></i> <a href="/download/{{$reply->attachment}}">{{$reply->attachment}} </a></span>
                                                                @endif

                                                            </div>

                                                            <div class="comment-tool">
                                                                @if($post->status == 0)
                                                               {{-- @if(auth()->user()->id == $post->company_id) --}}
                                                                <a class=" btn  ml-2  new-comment-1 reply-btn" data-id="{{$reply->comment_id}}">
                                                                    <i class="fa fa-reply"></i> Reply</a>
                                                                    {{-- @endif --}}
                                                                @endif
                                                                <a class=" btn  ml-2" data-toggle="tooltip" title="{{$reply->created_at->diffForHumans()}}"> <span class="fa fa-clock-o">
                                                                    </span>{{$reply->created_at->diffForHumans()}}</a>
                                                            </div>

                                                            <div class="show-comment-1 hidden" data-id="{{$reply->comment_id}}">
                                                                <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                                                    @csrf
                                                                    <div class="input-group">
                                                                        <input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />
                                                                        <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                                                        <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                                                        <input type="hidden" name="reply_user_id" value="{{ $reply->company_id }}" />
                                                                        <!-- <span class="btn-file">
                                                                            <input type="file" name="attach">
                                                                        </span> -->
                                                                        <span class="input-group-btn">
                                                                            <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span>
                                                                                Comment</button>
                                                                        </span>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach