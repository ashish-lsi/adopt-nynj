@extends('frontend.layouts.app')

@section('title',  $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="http://dev.empowerveterans.us/post-details/{{$post->post_id}}" />
<meta property="og:type" content="Article" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:image" content="https://www.your-domain.com/path/image.jpg" />
@stop

@section('content')
@stop