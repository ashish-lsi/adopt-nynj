@extends('frontend.layouts.app')


@section('indexstyle')
<link rel="stylesheet" href='https://localhost/adopt-a-company/css/style.css' />
@stop
@if($post)
@section('title', $post->title)
@section('meta_description', '')
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.wp-post-details',['slug'=>str_slug($post->title),'id'=>$post->id]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->content}}" />
<meta property="og:site_name" content="Empowerveterans" />

<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" />

@stop
@endif
@section('content')
<div class="container">
    @if($post)
    <div class="row">
        @if($post->type=="1")
        <h1 class=" blueText post-details-head-text text-center"> Alerts </h1>
        @elseif($post->type=="2")
        <h1 class=" blueText post-details-head-text text-center"> Announcements </h1>
        @else
        <h1 class=" blueText post-details-head-text text-center"> Success Stories </h1>
        @endif
    </div>


    <div class="row">


        <div class="col-sm-8 ">
            <div class="post-details-img ">
                <h2 class="post-title">
                    {!! $post->title !!}
                    <br>

                </h2>
                <div class="my-post-footer-post-details">
                    <span> <i class="fa fa-clock-o"></i> Posted on {{$post->created_at->format('d-M-Y')}} </span>
                </div>

                @if($post->image != "")
                <img src="{{asset('storage/app/files/'.$post->image)}}" alt="{{$post->title}}">
                @endif
                <div class="post-details-para">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
        <div class="col-sm-4 ">

            <div class="side-bar-postdetails">
                <ul class="list-group list-group-flush side-list-style">

                    <h4>Latest Post</h4>

                    @foreach($related_posts as $r)
                    <li class="list-group-item">
                        <a href="{{route('frontend.wp-post-details',['slug'=>str_slug($r->title),'id'=>$r->id])}}">{{$r->title}}</a>
                    </li>

                    @endforeach
                </ul>


            </div>
        </div>
    </div>
    @else
    <h2>Post Not Found !</h2>
    @endif
</div>

@endsection