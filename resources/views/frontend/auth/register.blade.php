@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')

<div class="sign-in">
    <div class="wrapper">
        <div class="register-in-page">
            <div class="signin-popup">
                <div class="signin-pop">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="login-sec">
                                <!--                                <ul class="sign-control">
                                                                    <li>
                                                                        <a href="{{route('frontend.auth.login')}}" class="active"> <i class="fa fa-user"> </i> {{__('Login')}}  </a>
                                                                    </li>
                                                                    <li class="current">
                                                                        <a href="{{route('frontend.auth.register')}}" class="active"> <i class="fa fa-user-plus"> </i> {{__('Sign up')}}   </a>
                                                                    </li>
                                                                </ul>-->
                                <!--sign_in_sec end-->
                                <div class="sign_in_sec current" id="tab-2">
                                    <div class="dff-tab current" id="tab-3">
                                        <h3>Join US here...</h3>
                                        {{ html()->form('POST', route('frontend.auth.register_auto'))->open() }}

                                        <input type="hidden" name="confirm_code" value="{{Input::get('code')}}" />

                                        @if(Input::get('ie'))
                                        <input type="hidden" name="ie" value="{{Input::get('ie')}}" />
                                        @endif
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Vendor id</label>
                                                    <input class="form-control" maxlength="80" type="text" name="vendor_id" value="{{$user->vendor_id}}" required disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">First name</label>
                                                    <input class="form-control" maxlength="30" type="text" name="first_name" value="{{$user->first_name}}" required disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Last name</label>
                                                    <input class="form-control" maxlength="30" type="text" name="last_name" value="{{$user->last_name}}" required disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Company name</label>
                                                    <input class="form-control" maxlength="80" type="text" name="company_name" value="{{$user->company_name}}" required disabled="true">

                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Email id</label>
                                                    <input class="form-control" maxlength="80" type="text" name="email" value="{{$user->email}}" required disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Select company</label>
                                                    <select name="company_type" required class="form-control" disabled="true">
                                                        <option value="">Select</option>
                                                        @foreach ($companyTypes as $type)
                                                        <option value="{{ $type->company_type_id }}" {{$user->company_type == $type->company_type_id ? 'selected' : ''}}>{{ $type->name }}</option>
                                                        @endforeach
                                                    </select>

                                                    <span><i class="fa fa-ellipsis-h"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Password</label>
                                                    <input class="form-control" maxlength="80" placeholder="password" type="password" name="password" id="password" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Password confirmation</label>
                                                    <input class="form-control" maxlength="80" placeholder="password confirmation" type="password" name="password_confirmation" id="password-confirm" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Address</label>
                                                    <input type="text" name="address" placeholder="Address" value="{{$userAddr->address}}" required disabled="true" id="user-address">
                                                    <!--<span><i class="fa fa-location-arrow" id="get-location-arrow-post"></i></span>-->
                                                    <input type="hidden" name="addr_lat" id="addr-lat" value="{{$userAddr->lat}}" />
                                                    <input type="hidden" name="addr_long" id="addr-long" value="{{$userAddr->lang}}" />
                                                    <input type="hidden" name="addr-country" id="addr-country" value="United States" />
                                                    <input type="hidden" name="addr-name" id="addr-name" value="{{$userAddr->address}}"/>
                                                    <div id="map" class="d-none"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">State</label>
                                                    <input type="text" name="State" placeholder="State" value="{{$userAddr->State}}" id="State" required disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">City</label>
                                                    <input type="text" name="City" placeholder="City" value="{{$userAddr->City}}" id="City" required disabled="true">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="sn-field">
                                                    <label for="">Zipcode</label>
                                                    <input type="text" name="Pincode" placeholder="Zipcode" value="{{$userAddr->Pincode}}" id="Pincode" required disabled="true">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="checky-sec st2">
                                                    <div class="fgt-sec">
                                                        <input type="hidden" name="category" value="0">
                                                    </div>
                                                    <!--fgt-sec end-->
                                                </div>
                                            </div>
                                            @if(config('access.captcha.registration'))
                                            <div class="col-lg-6">
                                                <div class="checky-sec st2">
                                                    <div class="fgt-sec">
                                                        {!! Captcha::display() !!}
                                                        {{ html()->hidden('captcha_status', 'true') }}
                                                    </div>
                                                    <!--fgt-sec end-->
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-lg-12">
                                                {{ form_submit(__('labels.frontend.auth.register_button'))->class('btn-info ') }}
                                            </div>
                                        </div>
                                        {{ html()->form()->close() }}
                                    </div>
                                    <!--dff-tab end-->

                                    <!--dff-tab end-->
                                </div>
                            </div>
                            <!--login-sec end-->
                        </div>
                    </div>
                </div>
                <!--signin-pop end-->
            </div>
            <!--signin-popup end-->
        </div>
        <!--sign-in-page end-->
    </div>
</div>


@endsection

@push('after-scripts')

@if(config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif

<script type="text/javascript">
    $('form').submit(function (e) {
        var country = $('#addr-country').val();
        var addr_lat = $('#addr-lat').val();

        if (country != 'United States') {
            alert('Only United States locations are allowed to register!');
            return false;
        }

        if (addr_lat == '') {
            alert('Please select a valid location!!');
            $('#address').focus();
            return false;
        }

        var password = $('#password').val();
        var confirm = $('#password-confirm').val();

        if (password !== confirm) {
            alert("Password and Confirm Password is not same.");
            $('#password').focus();
            return false;
        }
    });

    //Get the user address google position on page load
    $(document).ready(function () {
        var address = $('#user-address').val();
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var place = results[0];
                $('#addr-long').val(place.geometry.location.lng());
                $('#addr-name').val(place.formatted_address);
                $('#addr-lat').val(place.geometry.location.lat());
            } else {
                alert("User address not found!!");
            }
        });
    });
</script>

@endpush