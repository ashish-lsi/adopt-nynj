@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')

<section class="profile-account-setting profile-account-post profile-account-company">
    <div class="container">
        <div class="account-tabs-setting filter-post-company">
            <div class="row">
                <div class="col-lg-3">
                    <div class="right-sidebar">
                        <div class="filter-secs">
                            <div class="sd-title">
                                <h3>Find company</h3>
                            </div><!--filter-heading end-->
                            <div class="paddy">
                                {{ Form::open(array('url' => route('frontend.user.findCompany'), 'method' => 'get', 'id' => 'frmSearch')) }}

                                <div class="filter-dd">
                                    <input name="q" type="text" value="{{Input::get('q')}}" placeholder="Search" aria-label="Search">
                                </div>
                                <div class="filter-dd">
                                    <ul class="avail-checks">
                                        <li>
                                            <input type="radio" name="c_type" value="0" {{Input::get('c_type') == 0 ? 'checked' :''}} id="help_seeker-side-search" class="radio_nav_btn">
                                            <label for="help_seeker-side-search">
                                                <span></span>
                                            </label>
                                            <small>Help Seeker</small>
                                        </li>
                                        <li>
                                            <input type="radio" name="c_type" value="1" {{Input::get('c_type') == 1 ? 'checked' :''}} id="help_givers-side-search" class="radio_nav_btn">
                                            <label for="help_givers-side-search">
                                                <span></span>
                                            </label>
                                            <small>Providing help</small>
                                        </li>
                                    </ul>
                                </div>
                                <div class="filter-dd">
                                    <form class="job-tp">
                                        <select name="diversity">
                                            <option value=""> Diversity Type</option>
                                            @foreach($diversity_type as $diversity1)
                                            <option value="{{$diversity1->id}}" {{Input::get('diversity') == $diversity1->id ? 'selected' : ''}} > {{$diversity1->name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    </form>
                                </div>

                                <div class="filter-dd">
                                    <form class="job-tp">
                                        <select name="company_type">
                                            <option value=""> Company Type</option>
                                            @foreach($company_type as $com_type)
                                            <option value="{{$com_type->company_type_id}}" {{Input::get('company_type') == $com_type->company_type_id ? 'selected' : ''}}> {{$com_type->name}}</option>
                                            @endforeach
                                        </select>
                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    </form>
                                </div>
                                <div class="filter-dd">
                                    <input name="location" type="text" value="{{Input::get('location')}}" placeholder="Location" aria-label="Location">
                                </div>
                                <div class="filter-dd">
                                    <input name="state" type="text" value="{{Input::get('state')}}" placeholder="State" aria-label="State">
                                </div>
                                <div class="filter-dd">
                                    <input name="city" type="text" value="{{Input::get('city')}}" placeholder="City" aria-label="City">
                                </div>

                                <div class="">
                                    <div class="fl_lft">
                                        <button class="btn btn-primary weather-btn mr-left" id="btnSubmit">Submit</button>
                                    </div>
                                    <div class="fl_lft">
                                        <button class="btn btn-primary weather-btn" id="btnReset">Reset</button>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 ">
                    <div class="filter_box clearfix">
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active show">
                                <div class="main-ws-sec">
                                    <div class="posts-section">
                                        <div class="clearfix"></div>
                                        @if(count($company_list))
                                        @foreach($company_list as $company)
                                        <div class="post-bar">
                                            <div class="post_topbar">
                                                <div class="usy-dt post-bar-company">
                                                    <a href="{{route('frontend.user.view-profile',['user'=>$company->id])}}">
                                                        @if( $company->avatar_location != "")
                                                        <img src="{{ asset('public/storage/'. $company->avatar_location) }}" class="profile" />
                                                        @else
                                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="profile" />
                                                        @endif
                                                    </a>
                                                    <div class="usy-name">
                                                        <a href="{{route('frontend.user.view-profile',['user'=>$company->id])}}">
                                                            <h3>{{ $company->company_name }}</h3>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="ed-opts">
                                                    <span class="badge badge-secondary">{{($company->category_id==0)?'Help Seeker':'Help Provider'}}</span>
                                                    <a href="#" title="share" class="ed-opts-open" > <i class="la la-share-alt"></i> </a>
                                                    <ul class="ed-options">
                                                        <li><a href="#" title=""> <i class="la la-facebook"></i> Facebook</a></li>
                                                        <li><a href="#" title=""><i class="la la-twitter-square"></i> Twitter</a></li>
                                                        <li><a href="#" title=""> <i class="la la-linkedin"></i> Linkedin</a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <!--<div class="job_descp">
                                                @if($company->address)
                                                <p>
                                                    {{ $company->address->address }}, {{ $company->address->City }}, {{ $company->address->State}}, {{ $company->address->Pincode}} 
                                                </p>
                                                @endif
                                                <p>
                                                    <a href="mailto: {{$company->email}}">{{$company->email}}</a>
                                                </p>
                                                <p>{{$company->description}}</p>
                                                <a href="http://{{$company->Website}}" target="_blank">{{$company->Website}}</a>
                                            </div>-->
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="post-bar">
                                            <div class="post_topbar">
                                                <p>Sorry!! We have found no companies with this search criteria. <br>Please try again with new search criteria.</p>
                                            </div>
                                        </div>
                                        @endif

                                    </div><!--posts-section end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--account-tabs-setting end-->
        </div>
    </div>
</section>
@endsection
@push('after-scripts')
<script>
    $(document).ready(function () {
        $('#btnReset').click(function () {
            $('input').each(function () {
                $(this).val('');
            });
            $('select').each(function () {
                $(this).val('');
            });
        })

        $('#btnSubmit').click(function () {
            $('#frmSearch').submit();
        })
    })

</script>
@endpush