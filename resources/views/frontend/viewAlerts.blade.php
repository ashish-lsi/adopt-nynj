@extends('frontend.layouts.app')

@section('title', $alerts->title)
@section('meta_description', $alerts->title)
@section('meta-fb')
<meta property="og:url" content="http://www.adoptcompany.com/alerts/{{$alerts->id}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$alerts->title}}" />
<meta property="og:description" content="{{$alerts->title}}" />
<meta property="og:site_name" content="Adopt-a-Company" />

<meta property="og:image" content="http://www.adoptcompany.com/img/frontend/tti-adopt-logo.png" />
@stop

@section('content')
<section class="forum-page">
    <div class="container">
        <div class="forum-questions-sec">
            <div class="row">
                <div class="col-lg-3 col-md-4 pd-left-none no-pd">
                    <div class="main-left-sidebar no-margin">
                        <div class="user-data full-width">
                            <div class="user-profile">
                                <div class="username-dt">
                                    <div class="usr-pic">
                                        @if(auth()->user()->avatar_location)
                                        <img src="{{App\Helpers\Helper::getProfileImg($logged_in_user->avatar_location) }} " alt="">
                                        @else
                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" />
                                        @endif
                                    </div>
                                </div>
                                <!--username-dt end-->
                                <div class="user-specs">
                                    <div class="sd-title">
                                        <h3>{{$logged_in_user->company_name}}</h3>
                                        <!-- <i class="la la-ellipsis-v"></i> -->
                                    </div>
                                </div>
                                <ul class="user-fw-status">

                                    <li class="no-bor">
                                        <a href="{{route('frontend.user.view-profile',['user'=>auth()->user()->id])}}" title="">View Profile</a>
                                    </li>
                                </ul>
                            </div>
                            <!--user-profile end-->

                        </div>
                        <!--user-data end-->
                        @if(Module::find('Weather')->isEnabled())
                        <div class="item">
                            <div class="suggestions full-width">
                                <div class="sd-title">
                                    <h3>Weather Search</h3>
                                </div>
                                <!--sd-title end-->
                                <div class="suggestions-list suggestions-list-weather">
                                    {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                                    <div class="form-group sn-field">
                                        {{ html()->text('address')
                                                    ->class('form-control map-input')
                                                    ->id('weather-addr')
                                                    ->placeholder(__('Search your location..'))
                                                    ->attribute('maxlength', 191)
                                                    ->required() }}
                                        <span><i class="fa fa-location-arrow" id="get-location-arrow-weather"></i></span>
                                        <input type="hidden" name="City" id="weather-city" value="0" />
                                        <input type="hidden" name="State" id="weather-state" value="0" />
                                        <input type="hidden" name="Pincode" id="weather-pincode" value="0" />
                                        <input type="hidden" name="addr-lat" id="weather-lat" value="" />
                                        <input type="hidden" name="addr-long" id="weather-long" value="" />
                                        <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                                        <input type="hidden" name="addr-name" id="weather-addr-name" />

                                        <div id="map" class="d-none"></div>
                                        <input type="submit" value="Get Weather" class="btn weather-btn">
                                    </div>
                                    {{ Form::close() }}
                                </div>
                                <!--suggestions-list end-->
                            </div>
                        </div>
                        @endif
                        <!--suggestions end-->
                    </div>
                    <!--main-left-sidebar end-->
                </div>
                <div class="col-lg-8 no-pdd">
                    <div class="forum-post-view forum-post-details">
                        <div class="usr-question">
                            <div class="usr_img">
                                <img src="images/resources/usrr-img1.png" alt="">
                            </div>
                            <div class="usr_quest">
                                <h3> {{$alerts->title}}</h3>
                                <span><i class="fa fa-clock-o"></i>{{$alerts->created_at->diffForHumans()}}</span>
                                <span class="share_us">
                                    <i class="fa fa-share-alt"></i>
                                </span>
                                <span class="share_icon">
                                    {!! Share::page(route('frontend.alertsView',[$alerts->id,str_slug($alerts->title)]), $alerts->title,['target' => '_blank'])
                                    ->facebook()
                                    ->twitter()                            
                                    ->linkedin()
                                    !!}
                                </span>

                                <div class="ed-opts">
                                    <span class="badge {{($alerts->type == 1 ? 'badge-primary' : ($alerts->type == 2 ? 'badge-secondary' : ($alerts->type == 3 ? 'badge-tiranry' : '')) ) }}"> {{($alerts->type == 1 ? 'Alerts' : ($alerts->type == 2 ? 'Announcement' : ($alerts->type == 3 ? 'Success Stories' : '')) ) }}</span>
                                </div>
                                <p>{!!$alerts->content!!} </p>

                            </div>
                            <!--usr_quest end-->
                        </div>
                        <!--usr-question end-->
                    </div>
                    <!--forum-post-view end-->
                </div>
            </div>
        </div>
        <!--forum-questions-sec end-->
    </div>
</section>
@endsection
