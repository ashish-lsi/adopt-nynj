@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Help Seekers'))

@section('content')
<div>
    <div class="col-sm-12">
        @if(session('msg'))
        <div class="alert alert-success">
            {{session('msg')}}
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Add New Alert</div>
            <div class="card-body">
                <div class="container">
                    <form method="post" action="{{route('admin.create_wp_success_stories')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-sm-12">
                                <label>Alert Title</label>
                                <input type="text" name="title" class="form-control">
                            </div>
                            <div class="col-sm-12">
                                <label>Select Image</label>
                                <input type="file" name="file" class="form-control">
                            </div>
                            <div class="col-sm-12">
                                <label>Content</label>
                                <textarea name="content" rows="10" id="editor" class="form-control"></textarea>
                            </div>
                        </div>

                        <br />
                        <div class="row">
                            <span class="input-group-btn">
                                &nbsp;&nbsp;
                                <input type="submit" class="btn btn-success" value="Create">
                                &nbsp;&nbsp;
                                <a class="btn btn-warning" href="{{route('admin.helpseeker')}}">Reset</a>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Alerts Post Details</div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                            <tr>
                                <th>Title </th>
                                <th>Content</th>
                                <th>status</th>
                                <th>category</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($posts) > 0)
                            @foreach ($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>{!! str_limit($post->content,200) !!}</td>
                                <td>{{$post->active ? 'Active' : 'Disabled'}}</td>
                                <td>{{$post->type}}</td>
                                <td>{{ !in_array($post->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $post->created_at->diffForHumans() : '-' }}</td>
                                <td><a class="btn btn-danger" href="{{route('admin.helpgiver.show', array('id'=>$post->post_id))}}">Delete</a>
                                    <a class="btn btn-info" href="{{route('admin.helpgiver.show', array('id'=>$post->post_id))}}">Edit</a>
                                    <a class="btn btn-success" href="{{route('admin.wp_alerts_view', array('id'=>$post->id))}}">View</a>
                                    {!! Share::page(route('frontend.wp-post-details',[str_slug($post->title),$post->id]), 'Share title')
                                    ->facebook()
                                    ->twitter()
                                    ->linkedin('Extra linkedin summary can be passed here')
                                    !!}
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6">No Records Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="float-right">
                        {!! $posts->links() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>
</div>
@endsection

@push('after-scripts')
<script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>

<script>
    ClassicEditor
        .create(document.querySelector('#editor'))
        .catch(error => {
            console.error(error);
        });
</script>

@endpush