@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('View Alert'))
@section('content')
<div class="card">
    <br>
    <div class="col">
        <div style="margin: 0px 10px 10px 15px; float: right">
            <a class="btn btn-primary" href="{{ route('admin.alerts.index', ['type' => $result->type]) }}"> Back</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>Alert title</th>
                    <td>{!! $result->title !!}</td>
                </tr>

                <tr>
                    <th>Image</th>
                    <td>
                        @if($result->image != "")
                        <img src="{{asset('storage/app/alerts/'.$result->image)}}" alt="{{$result->title}}">
                        @endif
                    </td>
                </tr>

                <tr>
                    <th>Description</th>
                    <td>
                        {!! $result->content !!}
                    </td>
                </tr>
                <tr>
                    <th>Share</th>
                    <td>{!! Share::page(route('frontend.wp-post-details',[str_slug($result->title),$result->id]), 'Share title')
                            ->facebook()
                            ->twitter()                           
                            ->linkedin('Extra linkedin summary can be passed here')
                            !!}</td>
                </tr>
            </table>
        </div>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection
