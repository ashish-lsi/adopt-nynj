@extends('backend.layouts.app')

@section('content')
<div class="card">
    <div class="col-sm-12">
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.alerts.create') }}?type={{$typeG}}"> Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        {{ $message }}
                    </div>
                    @endif
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i>Manage List</div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Title </th>
                                    <th>Content</th>
                                    <th>Status</th>
                                    <th>Category</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($posts) > 0)
                                @foreach ($posts as $post)
                                <tr>
                                    <td>{{$post->title}}</td>
                                    <td>{!! str_limit($post->content,200) !!}</td>
                                    <td>{{$post->active ? 'Active' : 'Disabled'}}</td>
                                    <td>{{$type[$post->type]}}</td>
                                    <td>{{ !in_array($post->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $post->created_at->diffForHumans() : '-' }}</td>
                                    <td>
                                        <form action="{{ route('admin.alerts.destroy',$post->id) }}" method="POST">
                                            <a class="btn btn-success" href="{{route('admin.alerts.show', array('id'=>$post->id))}}">View</a>
                                            <a class="btn btn-info" href="{{route('admin.alerts.edit', array('id'=>$post->id))}}">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                                        </form>
                                        {!! Share::page(route('frontend.wp-post-details',[str_slug($post->title),$post->id]), 'Share title')
                                        ->facebook()
                                        ->twitter()
                                        ->linkedin('Extra linkedin summary can be passed here')
                                        !!}
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6">No Records Found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="float-right">
                            {!! $posts->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div>
</div>
@endsection