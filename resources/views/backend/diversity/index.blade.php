@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Diversity Type'))
@section('content')


<div class="card" >
    <div class="col-sm-12">
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.diversity.create') }}"> Create</a>
            </div>
            <form style="margin: 10px 10px 10px 15px" method="get" id="filterForm">
            @csrf
                <a class="btn btn-primary" id="btnDownload" > Export Excel</a>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            {{ $message }}
                        </div>
                    @endif
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Diversity Type Details</div>
                    <div class="card-body">
                        <div style="float: right">Total records <b>: {{$results->total()}}</b></div>
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Diversity Name</th>
                                    <th>Description</th>
                                    <th>Enabled</th>
                                    <th>Created On</th>
                                    <th>Updated On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($results as $result)
                                <tr>
                                    <td>{{$result->name}}</td>
                                    <td>{{$result->description}}</td>
                                    <td>{{$result->enabled == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ !in_array($result->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->created_at->diffForHumans() : '-' }}</td>
                                    <td>{{ !in_array($result->updated_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->updated_at->diffForHumans() : '-' }}</td>
                                    <td>
                                        <form action="{{ route('admin.diversity.destroy',$result->id) }}" method="POST">
                                            <a class="btn btn-info" href="{{ route('admin.diversity.show', $result->id) }}">View</a>
                                            <a class="btn btn-primary" href="{{ route('admin.diversity.edit', $result->id) }}">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                                        </form>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {!! $results->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div> 
</div>
@endsection
