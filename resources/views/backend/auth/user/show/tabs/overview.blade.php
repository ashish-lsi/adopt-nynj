<div class="col">
    <div class="table-responsive">
        @if (isset($user->first_name))
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.name')</th>
                <td>{{ $user->first_name ? $user->first_name . ' ' . $user->last_name : '-' }}</td>
            </tr>

            <tr>
                <th>Company Name</th>
                <td>{{ $user->company_name ?? '-' }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.email')</th>
                <td>{{ $user->email ?? '-' }}</td>
            </tr>

            <tr>
                <th>Vendor/business license number</th>
                <td>{{ $user->vendor_id }}</td>
            </tr>

            <tr>
                <th>Provider/Seeker Type</th>
                <td>
                   @php
                    echo App\Http\Controllers\Backend\Auth\User\UserController::getCompanyType($user->company_type);
                   @endphp
                </td>
            </tr>

            <tr>
                <th>Diversity Type</th>
                <td>
                   @php
                    echo App\Http\Controllers\Backend\Auth\User\UserController::getDiversityType($user->diversity_id);
                   @endphp
                </td>
            </tr>

            <tr>
                <th>Industry Type</th>
                <td>
                   @php
                    echo App\Http\Controllers\Backend\Auth\User\UserController::getOrgServes($user->organization_serves);
                   @endphp
                </td>
            </tr>

            <tr>
                <th>Company Address</th>
                <td>{{ $user->address ? $user->address . ',' . $user->City . ',' . $user->State . ',' . $user->Pincode : '-' }}</td>
            </tr>

            <tr>
                <th>Website</th>
                <td>{{ $user->Website ?? '-' }}</td>
            </tr>

            <tr>
                <th>Provider/Seeker Details</th>
                <td>{{ $user->descs ?? '-' }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.status')</th>
                <td>{{ $user->active == 1 ? 'Active' : 'Not Active'}}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.confirmed')</th>
                <td>{{ $user->confirmed == 1 ? 'Yes' : 'No'}}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.timezone')</th>
                <td>{{ $user->timezone ?? '-' }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.last_login_at')</th>
                <td>{{ $user->last_login_at ?? '-' }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.last_login_ip')</th>
                <td>{{ $user->last_login_ip ?? '-' }}</td>
            </tr>
        </table>
<!--        <div class="container">
            <h4>X Diversity Documents X</h4>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>URL</th>
                    </tr>
                </thead>
                <tbody id="doc-container">
                    @if (count($userDocs) > 0)
                    @foreach ($userDocs as $doc)
                    <tr id="main-row">
                        <td>{{$doc->title}}</td>
                        <td>{{$doc->description}}</td>
                        <td><a href="{{$doc->url}}" target="_blank">Click here</a></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5" align='center'>No records found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>-->
        @else
        <tr>
            <td colspan="5" align='center'>No records found</td>
        </tr>
        @endif
    </div>
</div><!--table-responsive-->
<style type="text/css">
    .container {
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px;
        border:2px solid black;
    }
    .container h4{
        width:250px;
        margin-top:-33px !important;
        margin-left:5px;
        background:white;
    }
</style>
