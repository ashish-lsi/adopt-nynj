@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
@include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Company Management Search</div>
                <div class="card-body">
                    <div class="container">
                        {{ Form::open(array('url' => route('admin.auth.user.index'),'id'=>'frmSearch', 'method'=>'GET')) }}
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Company Name</label>
                                {!! Form::text('search[company_name]', $searchQuery['company_name'] ?? '', ['class' => 'form-control', 'placeholder' => 'Company name']) !!}
                            </div>    
                            <div class="col-sm-3">
                                <label>E-mail</label>
                                {!! Form::text('search[email]', $searchQuery['email'] ?? '', ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                            </div>
                            <div class="col-sm-3">
                                <label>Provider/Seeker Type</label>
                                {!! Form::select('search[company_type]', $companyTypeList, $searchQuery['company_type'] ?? null, ['class' => 'form-control', 'placeholder' => 'Please select']) !!}
                            </div> 
                            <div class="col-sm-3">
                                <label>Diversity Type</label>
                                {!! Form::select('search[diversity_id]', $diversityList, $searchQuery['diversity_id'] ?? null, ['class' => 'form-control', 'placeholder' => 'Please select']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>User Category</label>
                                {!! Form::select('search[category_id]', array('Help Seeker', 'Help Provider'), $searchQuery['diversity_id'] ?? null, ['class' => 'form-control', 'placeholder' => 'Please select']) !!}
                            </div>
                            <div class="col-sm-3">
                                <label>Confirmed</label>
                                {!! Form::select('search[confirmed]', array('No', 'Yes'), $searchQuery['confirmed'] ?? null, ['class' => 'form-control', 'placeholder' => 'Please select']) !!}
                            </div>
                            <div class="col-sm-3">
                                <label>Activated</label>
                                {!! Form::select('search[active]', array('No', 'Yes'), $searchQuery['active'] ?? null, ['class' => 'form-control', 'placeholder' => 'Please select']) !!}
                            </div> 
                        </div>
                        <br/>
                        <div class="row">
                            <span class="input-group-btn">
                                &nbsp;&nbsp;
                                <input type="submit" class="btn btn-success" value="Search" id="btnSearch">
                                &nbsp;&nbsp;
                               
                                <input type="button"  class="btn btn-success" id="btnDownload" value="Download Excel" >
                                &nbsp;&nbsp;
                                <a class="btn btn-warning" href="{{route('admin.auth.user.index')}}">Reset</a>
                            </span>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.users.management') }} 
                    {{-- <small class="text-muted">{{ __('labels.backend.access.users.active') }}</small> --}}
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>@lang('labels.backend.access.users.table.company_name')</th>
                                <th>@lang('labels.backend.access.users.table.email')</th>
                                <th>Provider/Seeker Type</th>
                                <th>Diversity Type</th>
                                <th>Active</th>
                                <th>@lang('labels.backend.access.users.table.last_updated')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                            @foreach($users as $user)
                            @if($user->roles_label == "User") 
                            <tr>
                                <td>{{ $user->company_name ?? '-' }}</td>                              
                                <td>{{ $user->email ?? '-' }}</td>
                                <td>@php
                                    echo App\Http\Controllers\Backend\Auth\User\UserController::getCompanyType($user->company_type);
                                    @endphp</td>
                                <td>@php
                                    echo App\Http\Controllers\Backend\Auth\User\UserController::getDiversityType($user->diversity_id);
                                    @endphp</td>
                                <td>{!! $user->active ? '<span class="badge badge-success">yes</span>' :'<span class="badge badge-danger">no</span>' !!} </td>
                                <td>{{ $user->updated_at->diffForHumans() }}</td>
                                <td>{!! $user->action_buttons !!}</td>
                            </tr>
                             @endif 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row">
            <div class="col-7">
                <div class="float-left">
{{-- {!! $users->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $users->total()) }} --}}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $users->render() !!}
                    <?php
                    $searchQuery1['search'] = $searchQuery;
                    ?>
                    {!! $users->appends($searchQuery1)->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
<script type="text/javascript">
    $("#btnSearch").click(function () {
        $('form').attr('action', '<?php echo route('admin.auth.user.index') ?>')
    });

    $("#btnDownload").click(function () {
        $('form').attr('action', '<?php echo route('admin.auth.user.exportExcel') ?>');
        $('#frmSearch').submit();
    });
    </script>
    @endpush