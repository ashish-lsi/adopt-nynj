@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Open Request'))
@section('content')
<div>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Open Request search</div>
            <div class="card-body">
                <div class="container">
                    {{ Form::open(array('url' => route('admin.openrequest'),'id'=>'filterForm','method'=>'GET')) }}
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Diversity type</label>
                            {!! Form::select('diversityList', $diversityList, $searchQuery['diversity_type'] ?? null, ['class' => 'form-control', 'name' => 'search[diversity_type]', 'placeholder' => 'Please select']) !!}
                        </div>    
                        <div class="col-sm-3">
                            <label>Category</label>
                            {!! Form::select('categoriesList', $categoriesList, $searchQuery['category'] ?? null, ['class' => 'form-control', 'name' => 'search[category]', 'placeholder' => 'Please select']) !!}
                        </div>
                        <div class="col-sm-3">
                        <label>Provider/Seeker Type</label>
                            {!! Form::select('companyTypeList', $companyTypeList, $searchQuery['company_type'] ?? null, ['class' => 'form-control', 'name' => 'search[company_type]', 'placeholder' => 'Please select']) !!}
                        </div> 
                        <div class="col-sm-3">
                            <label>State</label>
                            {!! Form::text('search[state]', $searchQuery['state'] ?? '', ['class' => 'form-control', 'placeholder' => 'State']) !!} 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <label>City</label>
                            {!! Form::text('search[city]', $searchQuery['city'] ?? '', ['class' => 'form-control', 'placeholder' => 'City']) !!}
                        </div> 
                        <div class="col-sm-3">
                            <label>Zipcode</label>
                            {!! Form::text('search[pin]', $searchQuery['pin'] ?? '', ['class' => 'form-control', 'placeholder' => 'Zipcode']) !!}
                        </div> 
                        <div class="col-sm-3">
                            <label>From Date </label>
                            {!! Form::date('search[from_date]', $searchQuery['from_date'] ?? '', ['class' => 'form-control']) !!}
                        </div> 
                        <div class="col-sm-3">
                            <label>To Date </label>
                            {!! Form::date('search[to_date]', $searchQuery['to_date'] ?? '', ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-sm-3">
                            <label>Company name</label>
                            {!! Form::text('search[comp_name]', $searchQuery['comp_name'] ?? '', ['class' => 'form-control', 'placeholder' => 'Company name']) !!}
                        </div> 
                        <div class="col-sm-3">
                            <label>Sort by</label>
                            <select class="form-control" name="search[sort_by]">
                                <option selected="selected" value="">Please select</option>
                                <option value="reqCnt-DESC" {{(isset($searchQuery['sort_by']) && $searchQuery['sort_by'] == 'reqCnt-DESC') ? 'selected="selected"' : ''}}>Request Count Descending</option>
                                <option value="reqCnt-ASC" {{(isset($searchQuery['sort_by']) && $searchQuery['sort_by'] == 'reqCnt-ASC') ? 'selected="selected"' : ''}}>Request Count Ascending</option>
                            </select>
                        </div> 
                    </div>
                    <br/>
                    <div class="row">
                        <span class="input-group-btn">
                            &nbsp;&nbsp;
                            <input type="button" class="btn btn-success" value="Search" onclick="submitForm()">
                            &nbsp;&nbsp;
                            <input type="button"  class="btn btn-success" id="btnDownload" value="Download Excel" >

                            &nbsp;&nbsp;
                            <a class="btn btn-warning" href="{{route('admin.openrequest')}}">Reset</a>
                        </span>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-->

    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Open Requests Chart
                        <a class="btn btn-success f-right ml-1" href="#" onclick="downloadDom(document.getElementById('exportImage'), 'Openrequest')"><i class="fa fa-file-image-o"></i></a>

                        <a class="btn btn-success f-right" href="#" id="btnChartDataDownload"><i class="fa fa-file-excel-o"></i></a>
                    </div>
                    <div class="card-body" id="exportImage">
                        <div class="chart-wrapper">
                            <canvas id="canvas-2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Categories Chart
                        <a class="btn btn-success f-right ml-1" href="#" onclick="downloadDom(document.getElementById('exportImage1'), 'Openrequest-category')"><i class="fa fa-file-image-o"></i></a>

                        <a class="btn btn-success f-right" href="#" id="btnChartDataDownload"><i class="fa fa-file-excel-o"></i></a>

                    </div>
                    <div class="card-body" id="exportImage1">
                        <div class="chart-wrapper">
                            <canvas id="canvas-5"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="chart">
                    <div id="bar-chart-1"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Open Requests Details</div>
                    <div class="card-body">
                        <div style="float: right">Total records <b>: {{$posts->total()}}</b></div>
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th class="title-1">Company Name</th>
                                    <th class="title-1">Post title</th>
                                    <th class="title-1">Quantity</th>
                                    <th class="title-1">Location</th>
                                    <th class="title-1">Category</th>
                                    <th class="title-1">Request Count</th>
                                    <th class="title-1">Open Since</th>
                                    <th class="title-1">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($posts) > 0)
                                @foreach ($posts as $post)
                                <tr>
                                    <td>{{$post->company_name}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->qnty}}</td>
                                    <td>{{$post->location ?? '-'}}</td>
                                    <td>{{$post->categoryName ?? '-'}}</td>
                                    <td>{{$post->reqCnt}}</td>
                                    <td>{{ $post->created_at->diffForHumans() }}</td>
                                    <td>
                                        <a class="btn btn-warning" target="_blank" href="{{route('frontend.user.post-details',['post_id'=>$post->post_id,'post_title'=>$post->title])}}" title="View Live Post">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a class="btn btn-info" href="{{route('admin.openrequest.show', array('id'=>$post->post_id))}}" title="View Post">
                                            <i class="fa fa-vcard"></i>
                                        </a>
                                        <form action="{{ route('admin.helpseeker.deletePost', array('id'=>$post->post_id)) }}" method="POST">
                                            @csrf
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger" title="Delete Post">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="6">No Records Found</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="float-right">
                            <?php
                            $searchQuery1['search'] = $searchQuery;
                            ?>
                            {!! $posts->appends($searchQuery1)->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->            

    </div>
</div> 
@endsection


@push('after-scripts')

<script type="text/javascript">
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
    Chart.defaults.global.animationEasing = "easeOutQuart";
    $(function() {
    var pieChart = new Chart($('#canvas-5'), {
    type: 'pie',
            data: {
            labels: [{!! "'".$chartpia->implode('cname', "', '")."'" !!}],
                    datasets: [{
                    data: [{!! $chartpia->implode('total', ", ") !!}],
                    backgroundColor: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00','#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00'],
                    hoverBackgroundColor:['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00','#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00']
                    }]
            },
            options: {
            responsive: true,
                    legend: {
                    display: false,
                    },
                    plugins:{
                    labels:[{
                    render: 'value',
                            fontColor: '#fff',
                    },
                    {
                    render: 'label',
                            //arc: true,
                            fontColor: '#000',
                            position: 'outside',
                            outsidePadding: 4,
                    }]
                    }
            }
    }); // eslint-disable-next-line no-unused-vars

    var barChart = new Chart($('#canvas-2'), {
    type: 'horizontalBar',
            data: {
            labels: [{!! "'".$chartbar->implode('month', "', '")."'" !!}],
                    datasets: [{
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                            borderColor: 'rgba(220, 220, 220, 0.8)',
                            highlightFill: 'rgba(220, 220, 220, 0.75)',
                            highlightStroke: 'rgba(220, 220, 220, 1)',
                            data:[{!! $chartbar->implode('total', ", ") !!}],
                            label: 'Open Requests'
                    }]
            },
            options: {
            responsive: true,
                    scales: {
                    xAxes: [{
                    ticks: {
                    beginAtZero: true
                    }
                    }]
                    }
            }
    }); // eslint-disable-next-line no-unused-vars
    });
</script>
@endpush
