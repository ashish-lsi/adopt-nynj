@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Edit Provider/Seeker Type'))
@section('content')
<div class="card">
    <div class="col">
        <br>
        <form action="{{ route('admin.organizationServes.update',$result->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>Name</th>
                        <td><input type="text" name="name" value="{{ $result->name }}"  class="form-control" placeholder="Name"></td>
                    </tr>

                    <tr>
                        <th>Description</th>
                        <td><textarea class="form-control" style="height:150px" name="description" placeholder="Description">{{ $result->description }}</textarea></td>
                    </tr>
                    <tr>
                        <th>Enabled</th>
                        <td>
                            <label class="radio-inline"><input type="radio" name="enabled" value="1" <?php echo ($result->active == 1) ? 'checked' : '' ?>>Yes</label>
                            <label class="radio-inline"><input type="radio" name="enabled" value="0" <?php echo ($result->active == 0) ? 'checked' : '' ?>>No</label>
                        </td>
                    </tr>
                    <td colspan="2" align='center'>
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="Reset" class="btn btn-danger">Reset</button>
                        <a class="btn btn-warning" href="{{ route('admin.organizationServes.index') }}">Cancel</a>
                    </td>
                </table>
            </div>
        </form>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection
