@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Categories'))
@section('content')


<div class="card" >
    <div class="col-sm-12">
        <div class="row">
            <div style="margin: 10px 10px 10px 15px">
                <a class="btn btn-primary" href="{{ route('admin.newsletter.create') }}"> Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        {{ $message }}
                    </div>
                    @endif
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Newsletter List</div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Created On</th>
                                    <th>Updated On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($results as $result)
                                <tr>
                                    <td>{{$result->name}}</td>
                                    <td>{{ !in_array($result->created_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->created_at->diffForHumans() : '-' }}</td>
                                    <td>{{ !in_array($result->updated_at, [null, '0000-00-00 00:00:00', '-0001-11-30 00:00:00'])  ? $result->updated_at->diffForHumans() : '-' }}</td>
                                    <td>
                                        <form action="{{ route('admin.newsletter.destroy',$result->id) }}" method="POST">
                                            <a target="_blank" class="btn btn-info" href="{{asset('storage/app/newsletter/final/'.$result->preview_file)}}">View</a>
                                            <a class="btn btn-primary" href="{{ route('admin.newsletter.edit', $result->id) }}">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">Delete</button>
                                            <a class="btn btn-warning" href="{{ route('admin.newsletter.downloadzip', $result->id) }}">Download zip</a>
                                        </form>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="float-right">
                            {!! $results->links() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
    </div> 
</div>
@endsection
