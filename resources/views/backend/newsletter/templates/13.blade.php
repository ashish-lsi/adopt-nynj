<!-- CONTACT -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <!-- Background -->
        <td align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 28px; font-weight: 900; color: #383838; letter-spacing: 2px; line-height: 32px;" class="medium-editor-element">
                        <p contenteditable="true">CONTACT US</p>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                        <p contenteditable="true">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec nisi sed diam ultricies tempus. Nullam et ligula sodales, blandit arcu sit amet, varius felis.</p>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td>
                        <table class="table600" width="600" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#f3f3f3" style="border-radius: 5px;">
                            <tr>
                                <td align="center">
                                    <table class="table-container" width="550" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="full-width element-container" width="80" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tr>
                                                        <td data-editable="image" height="80" align="center" valign="middle">
                                                            <p contenteditable="true"><img src="{{asset('storage/app/newsletter/templates/default/img/icon64-7.png')}}" alt="icon" width="64" height="64" /></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- SPACE -->

                                                <table class="full-width element-container" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tr>
                                                        <td width="1" height="15" style="font-size: 15px; line-height: 15px;">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- END SPACE -->

                                                <table class="full-width element-container" width="440" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tr class="element-container">
                                                        <td data-editable="text" style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" class="medium-editor-element">
                                                            <p contenteditable="true">CONTACT INFO</p>
                                                        </td>
                                                    </tr>
                                                    <tr class="element-container">
                                                        <td height="10" style="font-size: 1px; line-height: 10px;">
                                                        </td>
                                                    </tr>
                                                    <tr class="element-container">
                                                        <td data-editable="text" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                                            <p content class="element-container"editable="true">Address: 123 Street Name , City | Phone: 123-456-7890<br /> Email: name@website.com | You can <a href="#" style="text-decoration: none; color: #a0cf50;">unsubscribe</a> here.</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END CONTACT -->