<style type="text/css">
    .fa {
        padding: 0px;
        width: 40px;
        margin: 5px 2px;
        border-radius: 60%;
    }

    .fa:hover {
        opacity: 0.7;
    }
</style>

<div contenteditable="true" class="element-container">
    <h3 style="text-align: center;">This is a template heading</h3>
</div>
<div style="text-align: center" class="element-container">
    @if ($txtFb != '') 
    <a href="{{$txtFb}}" class="fa" target="_blank">
        <img src='{{asset("storage/app/newsletter/templates/img/fb-squre.png")}}' width="70px">
    </a>
    @endif

    @if ($txtTwitter != '')
    <a href="{{$txtTwitter}}" class="fa" target="_blank">
        <img src='{{asset("storage/app/newsletter/templates/img/twitter-square.png")}}' width="70px">
    </a>
    @endif

    @if ($txtGoogle != '')
    <a href="{{$txtGoogle}}" class="fa" target="_blank">
        <img src='{{asset("storage/app/newsletter/templates/img/plus-square.png")}}' width="70px">
    </a>
    @endif

    @if ($txtYoutube != '')
    <a href="{{$txtYoutube}}" class="fa" target="_blank">
        <img src='{{asset("storage/app/newsletter/templates/img/youtube-square.png")}}' width="70px">
    </a>
    @endif

    @if ($txtWhatsup != '')
    <a href="https://wa.me/{{$txtWhatsup}}" class="fa" target="_blank">
        <img src='{{asset("storage/app/newsletter/templates/img/whatsapp-square.png")}}' width="70px">
    </a>
    @endif
</div>
