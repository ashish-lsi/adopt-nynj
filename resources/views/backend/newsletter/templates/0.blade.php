<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <title>Maverick - Email Template</title>
        <style type="text/css">

            @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,600);
            @import url(https://fonts.googleapis.com/css?family=Lato:400,700,900);

            .ReadMsgBody { width: 100%; background-color: #ffffff; }
            .ExternalClass { width: 100%; background-color: #ffffff; }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            html { width: 100%; }
            body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; }
            table { border-spacing: 0; border-collapse: collapse; }
            table td { border-collapse: collapse; }
            .yshortcuts a { border-bottom: none !important; }
            img { display: block !important; }
            a { text-decoration: none; color: #a0cf50; }

            /* Media Queries */

            @media only screen and (max-width: 640px) {
                body { width: auto !important; }
                table[class="table600"] { width: 450px !important; }
                table[class="table-container"] { width: 90% !important; }
                table[class="container2-2"] { width: 47% !important; text-align: left !important; }
                table[class="full-width"] { width: 100% !important; text-align: center !important; }
                img[class="img-full"] { width: 100% !important; height: auto !important; }
            }

            @media only screen and (max-width: 479px) {
                body { width: auto !important; }
                table[class="table600"] { width: 290px !important; }
                table[class="table-container"] { width: 82% !important; }
                table[class="container2-2"] { width: 100% !important; text-align: left !important; }
                table[class="full-width"] { width: 100% !important; text-align: center !important; }
                img[class="img-full"] { width: 100% !important; }
            }

        </style>

    </head>

    <body marginwidth="0" marginheight="0" style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" offset="0" topmargin="0" leftmargin="0">
        <div class="module-container" data-module="article_full">	<!-- ARTICLE FULL -->
            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td data-bgcolor="Main" align="center" bgcolor="#ffffff">
                            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">

                                <tbody>
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>			

                                    <tr>
                                        <td data-editable="image" align="center">
                                            <img class="img-full" src="http://localhost:8000/newsletter/templates/default/img/img600.jpg" alt="img" width="600" height="215">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td height="30" style="font-size: 1px; line-height: 30px;">&nbsp;</td>
                                    </tr>	

                                    <tr>
                                        <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 28px; font-weight: 900; color: #383838; letter-spacing: 2px; line-height: 32px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="6" medium-editor-index="7ab3d8ed-d7ac-831c-8de4-f7106a8f0eb0" data-placeholder="Type your text">
                                            Speaker's Message</td>
                                    </tr>

                                    <tr>
                                        <td height="30" style="font-size: 1px; line-height: 30px;">&nbsp;</td>
                                    </tr>	

                                    <tr>
                                        <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="9" medium-editor-index="caea33d4-88e4-c13f-c7f2-b956c91eb8a6" data-placeholder="Type your text">
                                            {{$message}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="40" style="font-size: 1px; line-height: 40px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- END ARTICLE FULL -->
        </div>
        <div class="module-container" data-module="article_left">	<!-- ARTICLE LEFT -->
            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td align="center" bgcolor="#ffffff">
                            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>

                                    @foreach ($alerts as $post)
                                    <tr>
                                        <td>
                                            <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                <tbody>
                                                    <tr>
                                                        <td data-editable="image" align="center">
                                                            <img class="img-full" src="{{$post['img']}}" alt="img" width="287" height="155">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <!-- SPACE -->
                                            <table class="full-width" width="1" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                <tbody>
                                                    <tr>
                                                        <td width="1" height="40" style="font-size: 40px; line-height: 40px;"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- END SPACE -->

                                            <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                <tbody>
                                                    <tr>
                                                        <td data-editable="text" style="font-family: 'Lato', sans-serif; font-size: 20px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="199" medium-editor-index="a7f41e70-9fcd-bf76-c2cb-6b8e602c08eb" data-placeholder="Type your text">
                                                            {{$post['title']}}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height="20" style="font-size: 1px; line-height: 20px;">&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td data-editable="text" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="202" medium-editor-index="a8d21a37-0e87-ed2e-e2bf-d20a216d8cd2" data-placeholder="Type your text">
                                                            {{$post['description']}}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height="15" style="font-size: 1px; line-height: 15px;">&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody></table>
            <!-- END ARTICLE LEFT --></div><div class="module-container" data-module="article_right">	<!-- ARTICLE RIGHT -->
            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td align="center" bgcolor="#ffffff">
                            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>
                                    @foreach ($announcements as $post)
                                    <tr>
                                        <td>
                                            <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                <tbody>
                                                    <tr>
                                                        <td data-editable="image" align="center">
                                                            <img class="img-full" src="{{$post['img']}}" alt="img" width="287" height="155">
                                                        </td>
                                                    </tr>

                                                </tbody></table>

                                            <!-- SPACE -->
                                            <table class="full-width" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                <tbody>
                                                    <tr>
                                                        <td width="1" height="40" style="font-size: 40px; line-height: 40px;"></td>
                                                    </tr>
                                                </tbody></table>
                                            <!-- END SPACE -->

                                            <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">

                                                <tbody>
                                                    <tr>
                                                        <td data-editable="text" style="font-family: 'Lato', sans-serif; font-size: 20px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="154" medium-editor-index="6ee9329b-173a-3071-dcdc-d64359b349b1" data-placeholder="Type your text">
                                                            {{$post['title']}}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height="20" style="font-size: 1px; line-height: 20px;">&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td data-editable="text" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="157" medium-editor-index="fb6ebbe6-e555-0c6b-4ea0-7e9608335384" data-placeholder="Type your text">
                                                            {{$post['description']}}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height="15" style="font-size: 1px; line-height: 15px;">&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- END ARTICLE RIGHT --></div><div class="module-container" data-module="article_2">	<!-- ARTICLE 2 -->
            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td align="center" bgcolor="#ffffff">
                            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">

                                <tbody>
                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            @foreach ($stories as $post)
                                            <table class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                <tbody>
                                                    <tr>
                                                        <td data-editable="image" align="left">
                                                            <img class="img-full" src="{{$post['img']}}" alt="img" width="287" height="192">
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height="20" style="font-size: 1px; line-height: 20px;">&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td data-editable="text" align="left" style="font-family: 'Lato', sans-serif; font-size: 20px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="279" medium-editor-index="467aa0a9-f583-1534-498c-7a4767f6f626" data-placeholder="Type your text">
                                                            {{$post['title']}}
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height="20" style="font-size: 1px; line-height: 20px;">&nbsp;</td>
                                                    </tr>

                                                    <tr>
                                                        <td data-editable="text" align="left" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" contenteditable="true" spellcheck="true" data-medium-editor-element="true" class="medium-editor-element" role="textbox" aria-multiline="true" data-medium-editor-editor-index="282" medium-editor-index="65f8033d-5da6-2794-8c0e-f306c2a600f5" data-placeholder="Type your text">
                                                            {{$post['description']}}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <td height="50" style="font-size: 1px; line-height: 50px;">&nbsp;</td>
                                    </tr>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody></table>
            <!-- END ARTICLE 2 --></div>
    </body>
</html>