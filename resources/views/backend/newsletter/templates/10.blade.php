<!-- FEATURES 3 -->
<style type="text/css">
    .round-image{
        border-radius: 50%;
    }
</style>
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="full-width" width="183" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td data-editable="image" align="center">
                                    <div contenteditable="true">
                                        <img src="<?= isset($data[0]->image) ? asset('storage/app/alerts/' . $data[0]->image) : asset('storage/app/newsletter/templates/img/icon64-1.png') ?>" alt="icon" width="64" height="64" class="round-image" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="25" style="font-size: 1px; line-height: 25px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 15px; font-weight: 700; color: #383838; line-height: 24px; letter-spacing: 1px;" class="medium-editor-element">
                                    <div contenteditable="true"><?= $data[0]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="25" style="font-size: 1px; line-height: 25px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true">
                                        <?= isset($data[0]->content) ? ($data[0]->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->

                        <table class="full-width" width="24" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td width="24" height="30" style="font-size: 30px; line-height: 30px;">
                                </td>
                            </tr>
                        </table>
                        <!-- END SPACE -->

                        <table class="full-width" width="183" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td data-editable="image" align="center">
                                    <div contenteditable="true">
                                        <img src="<?= isset($data[1]->image) ? asset('storage/app/alerts/' . $data[1]->image) : asset('storage/app/newsletter/templates/img/icon64-1.png') ?>" alt="icon" width="64" height="64" class="round-image" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="25" style="font-size: 1px; line-height: 25px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 15px; font-weight: 700; color: #383838; line-height: 24px; letter-spacing: 1px;" class="medium-editor-element">
                                    <div contenteditable="true"><?= $data[1]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="25" style="font-size: 1px; line-height: 25px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true">
                                        <?= isset($data[1]->content) ? ($data[1]->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->

                        <table class="full-width" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td width="1" height="30" style="font-size: 30px; line-height: 30px;">
                                </td>
                            </tr>
                        </table>
                        <!-- END SPACE -->

                        <table class="full-width" width="183" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td data-editable="image" align="center">
                                    <div contenteditable="true">
                                        <img src="<?= isset($data[2]->image) ? asset('storage/app/alerts/' . $data[2]->image) : asset('storage/app/newsletter/templates/img/icon64-1.png') ?>" alt="icon" width="64" height="64" class="round-image" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="25" style="font-size: 1px; line-height: 25px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 15px; font-weight: 700; color: #383838; line-height: 24px; letter-spacing: 1px;" class="medium-editor-element">
                                    <div contenteditable="true"><?= $data[2]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="25" style="font-size: 1px; line-height: 25px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true">
                                        <?= isset($data[2]->content) ? ($data[2]->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END FEATURES 3 --> 