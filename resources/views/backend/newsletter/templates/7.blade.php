<!-- ARTICLE 2 IMAGE -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- IMAGE ARTICLE BG -->
                        <table data-bgcolor="Featured" bgcolor="#f3f3f3" class="full-width" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-radius: 5px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr>
                                <td>
                                    <table class="table-container" width="250" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tr class="element-container">
                                            <td height="30" style="font-size: 1px; line-height: 30px;">
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td data-editable="image" align="center">
                                                <div contenteditable="true">
                                                    <img class="img-full" src="<?= isset($data[0]->image) ? asset('storage/app/alerts/' . $data[0]->image) : asset('storage/app/newsletter/templates/img/img250-1.jpg') ?>" alt="img" width="250" height="167" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 900; color: #383838; line-height: 24px; letter-spacing: 1px;" class="medium-editor-element">
                                                <div contenteditable="true"><?= $data[0]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="30" style="font-size: 1px; line-height: 30px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->

                        <table class="full-width" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td width="1" height="40" style="font-size: 40px; line-height: 40px;">
                                </td>
                            </tr>
                        </table>
                        <!-- END SPACE -->

                        <!-- IMAGE ARTICLE BG -->

                        <table data-bgcolor="Featured" bgcolor="#f3f3f3" class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-radius: 5px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr>
                                <td>
                                    <table class="table-container" width="250" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tr class="element-container">
                                            <td height="30" style="font-size: 1px; line-height: 30px;">
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td data-editable="image" align="center">
                                                <div contenteditable="true">
                                                    <img class="img-full" src="<?= isset($data[1]->image) ? asset('storage/app/alerts/' . $data[1]->image) : asset('storage/app/newsletter/templates/img/img250-1.jpg') ?>" alt="img" width="250" height="167" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 16px; font-weight: 900; color: #383838; line-height: 24px; letter-spacing: 1px;" class="medium-editor-element">
                                                <div contenteditable="true"><?= $data[1]->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="25" style="font-size: 1px; line-height: 25px;">
                                            </td>
                                        </tr>
                                        <tr class="element-container">
                                            <td height="30" style="font-size: 1px; line-height: 30px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END ARTICLE 2 IMAGE -->