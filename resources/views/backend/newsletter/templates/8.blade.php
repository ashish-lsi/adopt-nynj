<!-- ARTICLE RIGHT -->
@php
if(isset($data)){
    $data = $data[0];
}
@endphp
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="full-width element-container" width="287" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr>
                                <td data-editable="image" align="center">
                                    <div contenteditable="true">
                                        <img class="img-full" src="<?= isset($data->image) ? asset('storage/app/alerts/' . $data->image) : asset('storage/app/newsletter/templates/img/img287-5.jpg') ?>" alt="img" width="287" height="155" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- SPACE -->

                        <table class="full-width element-container" width="1" align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr>
                                <td width="1" height="40" style="font-size: 40px; line-height: 40px;">
                                </td>
                            </tr>
                        </table>
                        <!-- END SPACE -->

                        <table class="full-width" width="287" align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                            <tr class="element-container">
                                <td data-editable="text" style="font-family: 'Lato', sans-serif; font-size: 20px; font-weight: 900; color: #383838; letter-spacing: 1px; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true"><?= $data->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="20" style="font-size: 1px; line-height: 20px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td data-editable="text" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                                    <div contenteditable="true">
                                        <?= isset($data->content) ? ($data->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                                    </div>
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td height="15" style="font-size: 1px; line-height: 15px;">
                                </td>
                            </tr>
                            <tr class="element-container">
                                <td style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 600; color: #a0cf50; line-height: 24px;">
                                    <div contenteditable="true">
                                        <a data-color="Link" data-editable="link" href="#" style="text-decoration: none; color: #a0cf50;">Read More</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END ARTICLE RIGHT -->