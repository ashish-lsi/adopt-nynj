<!-- ARTICLE FULL -->
@php
if(isset($data)){
    $data = $data[0];
}
@endphp
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td data-bgcolor="Main" align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="image" align="center">
                        <div contenteditable="true">
                            <img class="img-full" src="<?= isset($data->image) ? asset('storage/app/alerts/' . $data->image) : asset('storage/app/newsletter/templates/img/img600.jpg') ?>" alt="img" width="600" height="215" />
                        </div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 28px; font-weight: 900; color: #383838; letter-spacing: 2px; line-height: 32px;" class="medium-editor-element">
                        <div contenteditable="true"><?= $data->title ?? 'PREPARE FOR NEW EXPERIENCE' ?></div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td height="30" style="font-size: 1px; line-height: 30px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                        <div contenteditable="true">
                            <?= isset($data->content) ? ($data->content) : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit odio at sodales aliquet. Aliquam erat volutpat. Aliquam eget lectus lacinia' ?>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END ARTICLE FULL -->