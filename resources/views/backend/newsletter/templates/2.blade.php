<!-- HEADLINE AND CONTENT -->

<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="table600" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr class="element-container">
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                        <div contenteditable="true">
                            <h3 style="text-align: center;">This is a template heading</h3>
                        </div>
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Lato', sans-serif; font-size: 28px; font-weight: 900; color: #383838; letter-spacing: 2px; line-height: 32px;" class="medium-editor-element">
                        <div contenteditable="true">TEMPLATE HEADING HERE...</div>
                    </td>
                </tr>
                <tr>
                    <td height="35" style="font-size: 1px; line-height: 35px;">
                    </td>
                </tr>
                <tr class="element-container">
                    <td data-editable="text" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: 400; color: #999999; line-height: 24px;" class="medium-editor-element">
                        <div contenteditable="true"><?= isset($txtMessage) && $txtMessage != '' ? $txtMessage : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec nisi sed diam ultricies tempus. Nullam et ligula sodales, blandit arcu sit amet, varius felis.' ?></div>
                    </td>
                </tr>
                <tr>
                    <td height="50" style="font-size: 1px; line-height: 50px;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- END HEADLINE AND CONTENT -->