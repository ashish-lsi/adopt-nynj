@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('Create Newsletter Template'))

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-3 ml-2">
                    Create Newsletter Template
                </h4>
              
            </div><!--col-->

            {{ Form::open(array('url' => route('admin.newsletter.store'))) }}
            <!--  ----------------------------************ Step 1 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small > Step 1 - Speaker's Message</small>
                </h4>
           
                <label class="form-control-label error-label">* Marked fields are required!!</label>
                <br/>

                <div class="form-group row newsletter-form">
				
                    <label class="form-control-label template-name">Template Name :</label>
                    <div class="col-md-6">{!! Form::text('txtName', '', ['class' => 'form-control required', 'id' => 'txtName']) !!} </div>
				
                </div>

                <div class="form-group row">
               
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if($tpl->content_type == 1)
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="speaker" data-attr-valid="">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="speakerPreview"></div>
                    </div>
                  
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right">
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(1)">Next</button>
                    </div><!--col-->
                </div>                
            </div>

            <!--  ----------------------------************ Step 2 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small>Step 2 - Alerts</small>
                    <label class=" lblWarn">Click on next button to skip this step</label>
                </h4>
                <hr>
                <div class="form-group row newsletter-form">
                    <label class=" form-control-label template-name">Select Alerts :</label>
                    <div class="col-md-6">
                        {!! Form::select('txtAlerts[]', $alerts, null, ['class' => 'form-control select2', 'id' => 'txtAlerts', 'multiple' => 'multiple']) !!}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if($tpl->content_type == 4)
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="Alerts" data-attr-valid="{{$tpl->content_valid}}">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="AlertsPreview"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right" id="firstBox">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(2)">Next</button>
                    </div><!--col-->
                </div>
            </div>

            <!--  ----------------------------************ Step 3 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small>Step 3 - Announcements</small>
                    <label class=" lblWarn">Click on next button to skip this step</label>
                </h4>
                <hr>
                <div class="form-group row">
                    <label class="form-control-label template-name">Select Announcements :</label>
                    <div class="col-md-6">
                        {!! Form::select('txtAnnouncements[]', $announcements, null, ['class' => 'form-control select2', 'id' => 'txtAnnouncements', 'multiple' => 'multiple']) !!}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if($tpl->content_type == 4)
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="Announcements" data-attr-valid="{{$tpl->content_valid}}">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="AnnouncementsPreview"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right" id="firstBox">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(3)">Next</button>
                    </div><!--col-->
                </div>
            </div>

            <!--  ----------------------------************ Step 4 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small >Step 4 - Success Stories</small>
                    <label class=" lblWarn">Click on next button to skip this step</label>
                </h4>
                <hr>
                <div class="form-group row">
                    <label class=" form-control-label template-name">Select Success Stories :</label>
                    <div class="col-md-6">
                        {!! Form::select('txtStories[]', $stories, null, ['class' => 'form-control select2', 'id' => 'txtStories', 'multiple' => 'multiple']) !!}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if($tpl->content_type == 4)
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="Stories" data-attr-valid="{{$tpl->content_valid}}">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="StoriesPreview"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right" id="firstBox">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(4)">Next</button>
                    </div><!--col-->
                </div>
            </div>

            <!--  ----------------------------************ Step 5 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small >Step 5 - Select a custom template</small>
                    <label class=" lblWarn">Click on next button to skip this step</label>
                </h4>
                <hr>
                <div class="form-group row">
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if(!in_array($tpl->content_type, [2,3]))
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="custom" data-attr-valid="{{$tpl->content_valid}}">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="customPreview"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right" id="firstBox">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(5)">Next</button>
                    </div><!--col-->
                </div>
            </div>

            <!--  ----------------------------************ Step 6 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small>Step 6 - Share Links</small>
                    <label class=" lblWarn">Click on next button to skip this step</label>
                </h4>   
                <hr>
                <div class="form-group row">
                    <label class="col-md-2 form-control-label template-name">Enter Details :</label>
                    <div class="col-md-3">
                        {!! Form::text('txtFb', '', ['class' => 'form-control', 'placeholder' => "Facebook Link...", 'id' => 'txtFb']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('txtTwitter', '', ['class' => 'form-control', 'placeholder' => "Twitter Link...", 'id' => 'txtTwitter']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('txtGoogle', '', ['class' => 'form-control', 'placeholder' => "Google+ Link...", 'id' => 'txtGoogle']) !!}
                    </div>
                    <label class="col-md-2 form-control-label">&nbsp;</label>
                    <div class="col-md-3">
                        {!! Form::text('txtYoutube', '', ['class' => 'form-control', 'placeholder' => "Youtube Link...", 'id' => 'txtYoutube']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::text('txtWhatsup', '', ['class' => 'form-control', 'placeholder' => "What's up number...", 'id' => 'txtWhatsup']) !!}
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if(in_array($tpl->content_type, [2]))
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="share" data-attr-valid="">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="sharePreview"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right" id="firstBox">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(6)">Next</button>
                    </div><!--col-->
                </div>
            </div>

            <!--  ----------------------------************ Step 7 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step">
                    <small >Step 7 - Contact us</small>
                    <label class=" lblWarn">Click on next button to skip this step</label>
                </h4>
                <hr>
                <div class="form-group row">
                    <div class="col-md-3 boxdiv sidediv">
                        <br>
                        <label class="form-control-label">Drag the templates to edit</label>
                        <hr>
                        @foreach ($newsletterTpl as $tpl)
                        @if(in_array($tpl->content_type, [3]))
                        <label class="box2 draggable" data-attr-id="{{$tpl->id}}" data-attr-type="contact" data-attr-valid="">
                            <b>{{$tpl->name}}</b>
                            <br>
                            <img src="{{asset('storage/app/newsletter/templates/preview/'.$tpl->preview_file)}}" class="tplImage">
                        </label>
                        @endif
                        @endforeach
                    </div>
                    <div class="col-md-9 boxdiv">
                        <br>
                        <input type="text" class="form-control colorPick" readonly="true" data-control="wheel" size="7" placeholder="change background color...">
                        <div class="preview_html" id="contactPreview"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" style="text-align: right" id="firstBox">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" class="btn btn-success nextBtn" onclick="nextPrev(7)">Next</button>
                    </div><!--col-->
                </div>
            </div>

            <!--  ----------------------------************ Step 8 ************---------------------------- -->
            <div class="tab ml-4">
                <h4 class="card-title mb-0 newsletter-step"><small>Template Preview</small></h4>
                <hr>
                <div class="col-md-10" style="padding: 0 0 10px 40px;">
                    <a class="btn btn-primary" href="javascript:void(0)" id="sort">Enable Sorting</a>
                </div><!--col-->
                <div id="preview_final"></div>
                <input type="hidden" name="tplHtml" id="tplHtml">

                <div class="form-group row">
                    <br>
                    <div class="col-md-12" style="text-align: right;margin-top: 10px;">
                        <button type="button" class="btn btn-default" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <a class="btn btn-success" href="javascript:void(0)" id="btnSubmit">Save</a>
                        <a class="btn btn-warning" href="{{route('admin.newsletter.index')}}">Cancel</a>
                    </div>
                </div>
            </div>

            <!-- Circles which indicates the steps of the form: -->
            <div class=" ml-4" style="text-align:center;margin-top:40px;">
                <span class="step" data-id="0"></span>
                <span class="step" data-id="1"></span>
                <span class="step" data-id="2"></span>
                <span class="step" data-id="3"></span>
                <span class="step" data-id="4"></span>
                <span class="step" data-id="5"></span>
                <span class="step" data-id="6"></span>
                <span class="step" data-id="7"></span>
            </div>
            {{ Form::close() }}
        </div><!--row-->
    </div><!--card-body-->
</div><!--table-responsive-->

{{ style('css/jquery.minicolors.css') }}
{{ style('css/newsletter.css') }}
@endsection

@push('after-scripts')
{!! script('js/ckeditor/ckeditor.js') !!}
{!! script('js/jquery.minicolors.min.js') !!}
<script type="text/javascript">
    var mergeSpeakerMsgUrl = '{{route("admin.newsletter.mergeSpeakerMsg")}}';
    var mergeContentsUrl = '{{route("admin.newsletter.mergeContents")}}';
    var mergeShareLinksUrl = '{{route("admin.newsletter.mergeShareLinks")}}';
    var mergeContactUsUrl = '{{route("admin.newsletter.mergeContactUs")}}';
    var mergeCustomUrl = '{{route("admin.newsletter.mergeCustom")}}';
    var mergeCustomUrl = '{{route("admin.newsletter.mergeCustom")}}';
</script>
{!! script('js/backend/newsletter.js') !!}
@endpush