@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('View Newsletter Template'))
@section('content')
<div class="card">
    <br>
    <div class="col">
        <div style="margin: 0px 10px 10px 15px; float: right">
            <a class="btn btn-primary" href="{{ route('admin.newsletter.index') }}"> Back</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>Newsletter Name</th>
                    <td>{{ $result->name }}</td>
                </tr>

                <tr>
                    <th>Created On</th>
                    <td>{{$result->created_at->diffForHumans() }}</td>
                </tr>

                <tr>
                    <th>Updated On</th>
                    <td>{{$result->updated_at->diffForHumans() }}</td>
                </tr>

                <tr>
                    <th>Template Preview</th>
                    <td>{!! $result->html !!}</td>
                </tr>
            </table>
        </div>
    </div><!--table-responsive-->
</div><!--table-responsive-->
@endsection
