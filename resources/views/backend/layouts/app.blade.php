<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}-us">
@endlangrtl

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style(mix('css/backend.css')) }}
    {{ style('css/select2.min.css') }}
    {{ style('css/font-awesome.min.css') }}
    <style>
        #social-links ul li {
            display: inline-block;
            font-size: 31px;
            padding: 0 10px;
        }

        .card-body img {
            width: 100%;
        }

        #social-links ul {
            padding: 0;
        }

        .card-body .table thead tr th:nth-child(2) {
            width: 310px;
        }

        table.table.table-responsive-sm.table-bordered.table-striped.table-sm tbody tr td .btn {
            padding: 3px 7px;
        }

        #social-links ul li {
            display: inline-block;
            font-size: 20px;
            padding: 6px 6px;
        }

        .sidebar .nav-dropdown-toggle {
            position: relative;
            cursor: pointer;
        }
        .f-right{
            float: right;
        }

        table.table.table-responsive-sm.table-bordered.table-striped.table-sm tbody tr td:first-child {
            width: 250px;
        }
        .card-body{
            background-color: white;
        }
    </style>
    @stack('after-styles')
</head>

<body class="{{ config('backend.body_classes') }}">
    @include('backend.includes.header')

    <div class="app-body">
        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.logged-in-as')
            {!! Breadcrumbs::render() !!}

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div>
                    <!--content-header-->

                    @include('includes.partials.messages')
                    @yield('content')
                </div>
                <!--animated-->
            </div>
            <!--container-fluid-->
        </main>
        <!--main-->

        @include('backend.includes.aside')
    </div>
    <!--app-body-->

    @include('backend.includes.footer')

    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script('js/backend/backend.js') !!}
    {!! script('js/backend/chart/moment-with-locales.min.js') !!}
    {!! script('js/backend/chart/Chart.min.js') !!}
    {!! script('js/backend/chart/chartjs-plugin-labels.min.js') !!}
    {!! script('js/select2.min.js') !!}
    {!! script('js/backend/dom-to-image.min.js') !!}
    {!! script('js/backend/FileSaver.min.js') !!}
    {!! script('js/jquery-ui.js') !!}
    
    <script src="{{ asset('public/js/share.js') }}"></script>
    @stack('after-scripts')

    <script>
        $('#btnDownload, #btnChartDataDownload').on('click', function() {
            $('#filterForm').append('<input type="hidden" name="action_type" value="export" id="action_type" /> ');
            $('#filterForm').submit();
        });
        function submitForm(){
           // alert('hii');
            $('#action_type').remove();
            $('#filterForm').submit();
        }

        function downloadDom(dom,filename) {
            domtoimage.toJpeg(dom,{ quality: 1 })
                .then(function(blob) {
                    window.saveAs(blob, filename+'.jpeg');
                });
        }
    </script>
</body>

</html>
