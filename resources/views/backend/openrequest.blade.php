@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div>
       
            
            <div class="col-sm-12">
              
              <div class="row">
              <div class="col-sm-6">
              {{-- <div class="chart">
              <div id="piechart"></div>
              </div> --}}
              <div class="card">
                    <div class="card-header">Open Requests Chart
                     
                    </div>
                    <div class="card-body">
                      <div class="chart-wrapper">
                        <canvas id="canvas-2"></canvas>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                    {{-- <div class="chart">
              <div id="piechart-1"></div>
              </div> --}}

              <div class="card">
                    <div class="card-header">Categories Chart
                     
                    </div>
                    <div class="card-body">
                      <div class="chart-wrapper">
                        <canvas id="canvas-5"></canvas>
                      </div>
                    </div>
                  </div>
              </div>
              </div>
              
         
            
            
            
            <div class="row">
            <div class="col-sm-12">
                  <div class="chart">
            <div id="bar-chart-1"></div>
            </div>
            </div>
            </div>
            
            <div class="row">
                    <div class="col-lg-12">
                      <div class="card">
                        <div class="card-header">
                          <i class="fa fa-align-justify"></i> Open Requests Details</div>
                        <div class="card-body">
                          <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                              <tr>
                                <th>Company Name</th>
                                <th>Post title</th>
                                <th>location</th>
                                <th>category</th>
                                <th>Created</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                             
                             @foreach ($posts as $post)
                             <tr>
                                    <td>{{$post->company_name}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->City}}, {{$post->Pincode}}</td>
                                    <td>{{$post->categoryName}}</td>
                                    <td>{{ $post->created_at->diffForHumans() }}</td>
                                    <td><a href="#{{$post->post_id}}">View</a></td>
                             </tr>
                             @endforeach
                            </tbody>
                          </table>
                          <div class="float-right">
                                {!! $posts->links() !!}
                            </div>
                                
                         


                        </div>
                      </div>
                    </div>
                    <!-- /.col-->
                  </div>
                  <!-- /.row-->            
            
            </div>
         </div> 
@endsection


@push('after-scripts')
    <script>
        Chart.defaults.global.responsive      = true;
        Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
        Chart.defaults.global.animationEasing = "easeOutQuart";
    </script>
    <script>
        $(function() {
            var pieChart = new Chart($('#canvas-5'), {
                type: 'pie',
                data: {
                  labels: [{!! "'" . $chartpia->implode('cname', "', '") . "'" !!}],
                  datasets: [{
                    data: [{!! $chartpia->implode('total', ", ") !!}],
                    backgroundColor: ['#3366cc', '#dc3912', '#ff9900','#109618','#990099','#0099c6','#dd4477','#66aa00'],
                    hoverBackgroundColor:['#3366cc', '#dc3912', '#ff9900','#109618','#990099','#0099c6','#dd4477','#66aa00']
                  }]
                },
                options: {
                  responsive: true,
                  
                    legend: {
                    display: false,
                    },
                }
              }); // eslint-disable-next-line no-unused-vars

              var barChart = new Chart($('#canvas-2'), {
                type: 'horizontalBar',
                data: {
                  labels: [{!! "'" . $chartbar->implode('month', "', '") . "'" !!}],
                  datasets: [{
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    borderColor: 'rgba(220, 220, 220, 0.8)',
                    highlightFill: 'rgba(220, 220, 220, 0.75)',
                    highlightStroke: 'rgba(220, 220, 220, 1)',
                    data:[{!! $chartbar->implode('total', ", ") !!}],
                    label: 'Open Requests' 
                  }]
                },
                options: {
                  responsive: true,
                  scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                  }
                }
              }); // eslint-disable-next-line no-unused-vars
        });
    </script>
@endpush
