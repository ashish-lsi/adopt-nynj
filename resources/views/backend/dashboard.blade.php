@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div>


    <div class="col-sm-12">
        <!-- /.row-->
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->help_seeker }}</div>
                        <div><a href="{{ route('admin.helpseeker') }}"> Help Seekers </a> </div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{$counts[0]->help_seeker }}%" aria-valuenow="{{$counts[0]->help_seeker }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->help_giver }}</div>
                        <div> <a href="{{ route('admin.helpgiver') }}">Help Providers </a> </div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: {{ $counts[0]->help_giver }}%" aria-valuenow="{{ $counts[0]->help_giver }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->open_request }}</div>
                        <div> <a href="{{ route('admin.openrequest') }}"> Open Requests </a></div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-info" role="progressbar" style="width: {{ $counts[0]->open_request }}%" aria-valuenow="{{ $counts[0]->open_request }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-value">{{ $counts[0]->close_request }}</div>
                        <div><a href="{{ route('admin.closerequest') }}">Closed Requests </a></div>
                        <div class="progress progress-xs my-2">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $counts[0]->close_request }}%" aria-valuenow="{{ $counts[0]->close_request }}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <!-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> -->
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->


        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">Help Chart
                        <a class="btn btn-success f-right ml-1" href="#" onclick="downloadDom(document.getElementById('exportImage'), 'dashboard-help')"><i class="fa fa-file-image-o"></i></a>
                        <a class="btn btn-success f-right" href="<?php echo e(route("admin.dashboard.downloadchart")); ?>" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                    </div>
                    <div class="card-body" id="exportImage">
                        <div class="chart-wrapper">
                            <canvas id="canvas-2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">

                <div class="card">
                    <div class="card-header">Categories Chart
                        <a class="btn btn-success f-right ml-1" href="#" onclick="downloadDom(document.getElementById('exportPng'), 'dashboard-catgory')">
                            <i class="fa fa-download"></i>
                        </a>
                        <a class="btn btn-success f-right" href="<?php echo e(route("admin.dashboard.downloadchart")); ?>" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                    </div>
                    <div class="card-body" id="exportPng">
                        <div class="chart-wrapper">
                            <canvas id="canvas-5"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="chart">
                    <div id="bar-chart-1">
                        <canvas id="barchart-full"></canvas>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Provider/Seeker Details</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>

                                    <th>@lang('labels.backend.access.users.table.company_name')</th>
                                    <th>@lang('labels.backend.access.users.table.email')</th>
                                    <th>Provider/Seeker Type</th>
                                    <th>@lang('labels.backend.access.users.table.confirmed')</th>
                                    <th>@lang('labels.backend.access.users.table.last_updated')</th>
                                    <th>@lang('labels.general.actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($logged_in_user->isAdmin())                            
                                @foreach($users as $user)

                                @if($user->roles_label == "User")
                                <tr>
                                    <td>{{ $user->company_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{!! $user->diversity_type ?? '-' !!}</td>
                                    <td>{!! $user->confirmed_label !!} </td>
                                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                                    <td>{!! $user->action_buttons !!}</td>
                                </tr>
                                @endif
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->

    </div>
</div>
@endsection


@push('after-scripts')
@if($categoryChart){
<script>
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
    Chart.defaults.global.animationEasing = "easeOutQuart";
    $(function(global) {
        Chart.defaults.global.plugins.labels = [];
        var pieChart = new Chart($('#canvas-5'), {
            type: 'pie',
            data: {
                labels: {!! json_encode($categoryChart["cname"])!!},
                datasets: [{
                    data: {{json_encode($categoryChart["total"])}},
                    backgroundColor: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00','#109618', '#990099', '#0099c6', '#dd4477', '#66aa00','#109618', '#990099', '#0099c6', '#dd4477', '#66aa00'],
                    hoverBackgroundColor: ['#3366cc', '#dc3912', '#ff9900', '#109618', '#990099', '#0099c6', '#dd4477', '#66aa00']
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false,
                },
                plugins: {
                    labels: [{
                            render: 'value',
                            fontColor: '#fff',
                        },
                        {
                            render: 'label',
                            //arc: true,
                            fontColor: '#000',
                            position: 'outside',
                            outsidePadding: 4,
                        }
                    ]
                }
            }
        }); // eslint-disable-next-line no-unused-vars

        var barChart = new Chart($('#canvas-2'), {
            type: 'bar',
            data: {
                labels: [{!!"'".$seekerChart->implode('month', "', '"). "'"!!}],
                datasets: [{
                    backgroundColor: 'rgba(220, 220, 220, 0.5)',
                    borderColor: 'rgba(220, 220, 220, 0.8)',
                    highlightFill: 'rgba(220, 220, 220, 0.75)',
                    highlightStroke: 'rgba(220, 220, 220, 1)',
                    data: [{!!$seekerChart->implode('total', ", ")!!}],
                    label: 'Help Seekers'
                }, {
                    backgroundColor: 'rgba(151, 187, 205, 0.5)',
                    borderColor: 'rgba(151, 187, 205, 0.8)',
                    highlightFill: 'rgba(151, 187, 205, 0.75)',
                    highlightStroke: 'rgba(151, 187, 205, 1)',
                    data: [{!!$giverChart->implode('total', ", ") !!}],
                    label: 'Help Providers'
                }]
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }); // eslint-disable-next-line no-unused-vars


        window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};
var Samples = global.Samples || (global.Samples = {});
Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function(seed) {
			this._seed = seed;
		},

		rand: function(min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},
}
    window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(-100, 100));
	};
        var barChartData = {
			labels: {!!json_encode(array_keys($helpSeekerArray))!!},
			datasets: [{
				label: 'Help Seekers',
				backgroundColor: window.chartColors.green,
				data: {!!json_encode(array_values($helpSeekerArray))!!},
			}, {
				label: 'Help Providers',
				backgroundColor: window.chartColors.yellow,
				data: {!!json_encode(array_values($helpGiverArray))!!},
			}, 
            ]

		};


        var ctx = document.getElementById('barchart-full').getContext('2d');
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                title: {
                    display: true,
                    text: 'Category Charts'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                // responsive: true,
                scales: {
                    // xAxes: [{
                    //     stacked: true,
                    // }],
                    // yAxes: [{
                    //     stacked: true
                    // }]
                }
            }
        });

    });
</script>
@endif
@endpush