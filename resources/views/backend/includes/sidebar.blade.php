<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-hourglass"></i> @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/helpseeker*')) }}" href="{{ route('admin.helpseeker') }}">
                    <i class="nav-icon far fa-hand-paper"></i> Help Seekers
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/helpgiver*')) }}" href="{{ route('admin.helpgiver') }}">
                    <i class="nav-icon far fa-handshake"></i> Help Providers
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/openrequest*')) }}" href="{{ route('admin.openrequest') }}">
                    <i class="nav-icon far fa-folder-open"></i> Open Requests
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/closerequest*')) }}" href="{{ route('admin.closerequest') }}">
                    <i class="nav-icon fas fa-check"></i> Closed Requests
                </a>
            </li>

            @if ($logged_in_user->isAdmin())
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                    <i class="nav-icon far fa-building"></i> Company Details

                    @if ($pending_approval > 0)
                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                    @endif
                </a>
            </li>
            @endif
            @if($logged_in_user->hasRole("administrator|editorial"))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/newsletter*')) }}" href="{{ route('admin.newsletter.index') }}">
                    <i class="nav-icon far fa-envelope"></i> Newsletter Template
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/companyType*')) }}" href="{{ route('admin.companyType.index') }}">
                    <i class="nav-icon far fa-clone"></i> Company Type
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/organizationServes*')) }}" href="{{ route('admin.organizationServes.index') }}">
                    <i class="nav-icon far fa-clone"></i> Industry Type
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/category*')) }}" href="{{ route('admin.category.index') }}">
                    <i class="nav-icon far fa-address-card"></i> Category
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/diversity*')) }}" href="{{ route('admin.diversity.index') }}">
                    <i class="nav-icon fa fa-users" aria-hidden="true"></i> Diversity Type
                </a>
            </li>

            <li class="nav-item nav-dropdown ">
                <a class="nav-link nav-dropdown-toggle ">
                    <i class="nav-icon icon-user"></i>Content Management
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.alerts.index')}}?type=1">
                            <i class="nav-icon far fa-bell"></i> Alerts
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.alerts.index')}}?type=2">
                            <i class="nav-icon fa fa-bullhorn" aria-hidden="true"></i> Anouncement
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('admin.alerts.index')}}?type=3">
                            <i class="nav-icon fa fa-thumbs-o-up" aria-hidden="true"></i> Success Stories
                        </a>
                    </li>
                </ul>
            </li>

            @endif





            @if ($logged_in_user->isAdmin())
            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/auth*')) }}" href="#">
                    <i class="nav-icon icon-user"></i> @lang('menus.backend.access.title')

                    @if ($pending_approval > 0)
                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                    @endif
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.adminDetails') }}">
                            @lang('User Management')

                            @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                            @lang('labels.backend.access.roles.management')
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            {{--
            <li class="divider"></li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'open') }}">
            <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/log-viewer*')) }}" href="#">
                <i class="nav-icon icon-list"></i> @lang('menus.backend.log-viewer.main')
            </a>

            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}" href="{{ route('log-viewer::dashboard') }}">
                        @lang('menus.backend.log-viewer.dashboard')
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('log-viewer::logs.list') }}">
                        @lang('menus.backend.log-viewer.logs')
                    </a>
                </li>
            </ul>
            </li> --}}
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
<!--sidebar-->