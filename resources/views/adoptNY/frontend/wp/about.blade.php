@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))

@section('content')
<div class="container pr_ab">
<br>
<br>
<br>
<br>
<br>


<p>
<span class="hed_bld">Adopt a company is a product of 9/11 where we learned to help each other and now it is helping businesses support each other.</span> <br><br>
<span class="hed_bld">My partner Al and I are here to support you. He tested this product during 9/11 and now we have this for everyone.</span> <br><br>
<span class="hed_bld">We are here to support you.</span>
</p>


<h2>Martha Montoya</h2>  
<h3>President</h3>


<h6>Info:</h6>
<div class="details">
    <label>Email</label> <span class="dot_a">:</span> <a href="mailto:info@techtoolsinnovation.com">info@techtoolsinnovation.com</a>
</div>
<div class="details">
    <label>Website </label> <span class="dot_a">:</span> <a href="https://techtoolsinnovation.com/" target="_blank">https://techtoolsinnovation.com/</a>
</div>
<div class="details">
    <label>Company phone number </label> <span class="dot_a">:</span> <a href="tel:7142328660"> 714.232.8660</a>
</div>

</div>
<pre>

</pre>
@stop