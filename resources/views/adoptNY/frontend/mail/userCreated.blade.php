
<div id="mailsub" class="notification" align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


                <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                    <tr><td>
                            <!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"> </div>
                        </td></tr>
                    <!--header -->
                    <tr><td align="center" bgcolor="#fff">

                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="left">
                                        <div class="mob_center_bl" style="float: left; display: inline-block; width:100%;">
                                            <table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse;">
                                                <tr><td align="center" valign="middle">
                                                        <!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
                                                        <table width="115" border="0" cellspacing="0" cellpadding="0" >
                                                            <tr><td align="left" valign="top" class="mob_center">
                                                                    <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
                                                                        <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                                        <img src="{{ url(theme_url('images/TTI-Adopt-a-Company-logo.png'))}}" 
                                                                        width="195" height="37"alt="adopt logo" border="0" style="display: block;" /></font></a>

                                                                </td></tr>
                                                        </table>						
                                                    </td></tr>
                                            </table></div>
                                    </td>
                                </tr>
                            </table>
                            <!-- padding --><div style="height: 30px; line-height: 50px; font-size: 10px;"> </div>
                        </td></tr>
                    <!--header END-->

                    <!--content 1 -->
                    <tr><td align="center" bgcolor="#f5f5f5">
                            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">
                                        <!-- padding --><div style=" line-height: 60px; font-size: 10px;"> </div>
                                        <div style="line-height: 44px;">
                                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                                <strong> {{$user->company_name}} </strong> Created New Account
                                            </span></font>
                                        </div>
                                        <!-- padding --><div style=" font-size: 10px;"> </div>
                                    </td></tr>
                                    <tr>
                                    <td align="center">
                                        <!-- padding --><div style=" font-size: 10px;"> </div>
                                        <div style="">
                                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size:20px;">
                                            <p>Click <a href="{{route('frontend.approve.user',['token'=>$user->confirmation_code])}}">here</a> to approve. </p>
                                            <p>Click <a href="{{route('admin.auth.user.show',['user'=>$user->id])}}">here</a> to review their profile. </p>
                                            </font>
                                        </div>
                                        <!-- padding --><div style=" font-size: 10px;"> </div>
                                    </td>
                                    </tr>


                            </table>
                           


                        </td></tr>
                    <!--content 1 END-->




                    <!--footer -->
                    <tr><td class="iage_footer" align="center" bgcolor="#ffffff">
                            <!-- padding --><div style="height: 0px; line-height: 80px; font-size: 10px;"> </div>	

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr><td align="center">

                                    <br>
                                            <br>
                                            <br>
                                            <br>
                                            <small style="font-size: 14px;line-height: 20px;">
                                                All products and/or services are received “as is",
                                                 “with all faults” and “as available” without warranty of any kind,
                                                 either express or implied, including, but not limited to,
                                                  the implied warranties of merchantability, fitness for a particular purpose, 
                                                  accuracy, or non-infringement.
                                            </small>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                        <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                        <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                            {{date('Y')}} © {{app_name()}}. ALL Rights Reserved.
                                        </span></font>				
                                    </td></tr>			
                            </table>

                            <!-- padding --><div style="height: 0px; line-height: 30px; font-size: 10px;"> </div>	
                        </td></tr>
                    <!--footer END-->
                    <tr><td>
                            <!-- padding --><div style="height: 0px; line-height: 80px; font-size: 10px;"> </div>
                        </td></tr>
                </table>
                <!--[if gte mso 10]>
                </td></tr>
                </table>
                <![endif]-->

            </td></tr>
    </table>
</div> 

