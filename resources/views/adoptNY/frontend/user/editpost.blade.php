@extends('frontend.layouts.app')
@section('title', $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:site_name" content="Empowerveterans" />
<meta property="og:image" content="http://dev.empowerveterans.us/img/frontend/tti-adopt-logo.png" />
@stop
@section('content')
<section class="forum-page ">
    <div class="container">
        <div class="forum-questions-sec">
            <div class="row">
                <div class="col-lg-8">
                    @if(session('success')!= "")
                    <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                    <div class="forum-post-view forum-post-view-edit">
                        <div class="usr-question">
                            <div class="usr_quest">
                                <h3>Edit post</h3>
                            </div>
                            {{ html()->form('POST', route('frontend.user.dashboard_new'))->attribute('enctype', 'multipart/form-data')->attribute('class', 'show-post-comment')->attribute('id', 'frmCreatePost')->open() }}
                            <div class="suggestions-list suggestions-list-weather">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="hidden"  class="form-control" name="post_id" value="{{$post->post_id}}">
                                            <label for="">Title</label>
                                            <input type="text" autofocus="true" class="form-control" name="title" onfocus="var temp_value = this.value; this.value = ''; this.value = temp_value" placeholder="Title" value="{{$post->title}}">
                                        </div>
                                        <div class="col-lg-12">
                                            <label for="">Description</label>

                                            <textarea class="form-control" name="article" id="" cols="30" rows="10" value="{{$post->article}}">{{$post->article}}</textarea>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Quantity</label>
                                            <input type="number" class="form-control" name="qnty" min="1" placeholder="Enter quantity more than 0" value="{{$post->qnty}}">
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="cpp-fiel">
                                                <label for="">Select Category</label>
                                                <select name="category_id" class="select-text" required>
                                                    <option value="" selected> Select Category</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{ $category->category_id }}" {{$post->category_id == $category->category_id ? 'selected' :''}}> {{ $category->name }} </option>
                                                    @endforeach
                                                </select>

                                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                            </div>
                                        </div>


                                        <div class="col-lg-6">
                                            <label for="">Location</label>
                                            <input type="text" class="form-control" name="location" placeholder="location" value="{{$post->location}}">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">State</label>
                                            <input type="text" class="form-control" name="State" placeholder="State" value="{{$post->state}}">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">City</label>
                                            <input type="text" class="form-control" name="City" placeholder="City" value="{{$post->city}}">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="">Zipcode</label>
                                            <input type="text" class="form-control" name="Pincode" placeholder="Zipcode" value="{{$post->pincode}}">
                                        </div>
                                        <div class="col-lg-6 mr-15">
                                            <div class="cus_radio input-tx-s">
                                                <ul>
                                                    <li> <input type="radio" value="0" id="helpSeekerType" name="type" {{$post->type == 0 ? "checked" : ''}}>
                                                        <label for="helpSeekerType">Seeking help</label></li>
                                                    <li> <input type="radio" value="1" id="providingHelpType" name="type" {{$post->type == 1 ? "checked" : ''}}>
                                                        <label for="providingHelpType">Providing help</label></li>

                                                    <div class="clearfix"></div>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mr-15">
                                            <div class="input-tx input-effect">
                                                <div class="form-group">
                                                    <div class="cv_upload ">
                                                        <span class="wpcf7-form-control-wrap file-334">
                                                            <input type="file" name="attachements[]" size="40" class="wpcf7-form-control wpcf7-file form-control" tabindex="6" aria-invalid="false">
                                                        </span>
                                                        <small></small>
                                                        <label for="file-1">
                                                            <span>upload</span>
                                                            <strong>choose a file</strong>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="input-tx input-effect nomr-top">
                                                <!-- <label>Attachements</label> -->
                                                @foreach($post->attachements as $attachment)
                                                <div class="form-group">
                                                    <a href="{{$attachment->location}}">{{$attachment->location}}</a> <a href=""> &emsp;<i class="fa fa-times"></i></a>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>


                                        <div class="col-lg-12 mr-20">
                                            <button class="btn btn-primary createpost-btn" type="submit" id="btnSubmit">Submit</button>
                                            <button class="btn btn-primary createpost-btn" type="reset">Reset</button>
                                        </div>
                                    </div>

                                </div>
                                {{ html()->form()->close() }}

                            </div>

                        </div>
                        <!--forum-post-view end-->
                    </div>

                </div>
            </div>
        </div>
        <!--forum-questions-sec end-->
    </div>
</section>
@endsection

@push('after-styles')
<style type="text/css">
    #map {
        width: 340px;
        min-height: 300px;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }

    .user-post-details h4 {
        margin-top: 10px;
        font-size: 20px;
        font-weight: 700;
    }

    .hidden {
        display: none;
    }

    .show-comment {
        margin-top: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .btn-file {
        padding: 10px 0 15px;
        float: left;
    }

    .my-post a {
        text-align: right;
        padding: 10px 0px;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .disabled-div {
        pointer-events: none;
        /* for "disabled" effect */
        opacity: 0.5;
        background: #CCC;
    }

    .disabled-lnk {
        pointer-events: none;
        cursor: default;
        text-decoration: none;
        color: black;
    }

    .btnContact {
        float: right;
        margin-left: 10px;
    }

    .comments-box {
        display: none;
        margin-top: 1px;
    }

    .down {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }
</style>
@endpush