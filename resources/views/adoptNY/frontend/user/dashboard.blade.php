@extends('frontend.layouts.app')
@section('content')
<div class="dashboard_container">
    <div class="row">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger">
            <p>{{$errors->first()}}</p>
        </div>
        @endif
        <div class="dash_left">
            @if(Module::find('Weather')->isEnabled())
            <div class="item no_item">
                <div class="item_third_left">
                    <div class="create_post_btn">
                        <a href="#" class="btn btn-primary createpost-btn color-11 post-jb" onclick="$('#providingHelpType').prop('checked', true);">
                            Donate
                        </a>
                    </div>
                </div>
                <div class="item_third_right">
                    <div class="create_post_btn">
                        <a href="#" class="btn btn-primary createpost-btn color-11 post-jb" onclick="$('#helpSeekerType').prop('checked', true);">
                            Find help
                        </a>
                    </div>
                </div>
            </div>


            @endif



            <div class="item">
                <div class="right-sidebar">
                    <div class="filter-secs">
                        <div class="sd-title">
                            <h3>Find company</h3>
                        </div>
                        <!--filter-heading end-->
                        <div class="paddy">
                            {{ Form::open(array('url' => route('frontend.user.findCompany'), 'method' => 'get')) }}
                            <div class="">
                                <div class="filter-dd">
                                    <input name="q" class="form-control" type="text" placeholder="Search for a company.." required>
                                </div>
                                <div class="fl_lft">
                                    <button class="btn btn-default weather-btn mr-left" type="submit">Submit</button>
                                </div>
                                <div class="fl_lft">
                                    <button class="btn btn-default weather-btn" type="reset">Reset</button>
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="item count_num">
                <div class="main_bx">
                    <div>
                        <label>Items requested</label> <span class="requested_btn"> {{$requested}} </span>
                    </div>

                </div>
            </div>


            <div class="item count_num">
                <div class="main_bx">

                    <div>
                        <label>Items donated</label> <span class="donated_btn"> {{$donated}} </span>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="suggestions full-width">
                    <div class="sd-title">
                        <h3>Weather Search</h3>
                    </div>
                    <!--sd-title end-->
                    <div class="suggestions-list suggestions-list-weather">
                        {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                        <div class="form-group sn-field">
                            {{ html()->text('address')
                                    ->class('form-control map-input')
                                    ->id('weather-addr')
                                    ->placeholder(__('Search your location..'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            <span><i class="fa fa-location-arrow" id="get-location-arrow-weather"></i></span>

                            <input type="hidden" name="City" id="weather-city" value="0" />
                            <input type="hidden" name="State" id="weather-state" value="0" />
                            <input type="hidden" name="Pincode" id="weather-pincode" value="0" />
                            <input type="hidden" name="addr-lat" id="weather-lat" value="" />
                            <input type="hidden" name="addr-long" id="weather-long" value="" />
                            <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                            <input type="hidden" name="addr-name" id="weather-addr-name" />

                            <div id="map" class="d-none"></div>
                            <input type="submit" value="Get Weather" class="btn btn-default weather-btn">
                        </div>
                        {{ Form::close() }}
                    </div>
                    <!--suggestions-list end-->
                </div>
            </div>
        </div>

        <div class="dash_center">
            <div class="item help_s category_bl">
                <div class="sd-title">
                    <h3>Category </h3>
                    <form action="" id="filterForm">
                        <select name="cat_filter" onchange="$('#filterForm').submit();" id="">
                            <option value="a-z" {{Input::get('cat_filter') == "a-z" ? 'selected' :'' }}>A-Z</option>
                            <option value="z-a" {{Input::get('cat_filter') == "z-a" ? 'selected' :'' }}>Z-A</option>
                        </select>

                    </form>

                </div>
                <div class="post-bar">

                    @php
                    $fixed_count = 10;
                    $labels = ['btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger','btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger','btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger','btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger','btn-default', 'btn-primary', 'btn-success', 'btn-info', 'btn-warning', 'btn-danger']
                    @endphp
                    <div class="fixed">

                        <div class="row">
                            @foreach($popularCats as $key => $cat)
                            @if($key < $fixed_count) <div class="col-sm-6 create_post_btn">
                                <a class=" btn {{$labels[$key]}}" href="{{route('frontend.user.home')}}?category[]={{$cat->category_id}}">
                                    <span class="label {{$labels[$key]}}">
                                        {{$cat->name_clean}}({{$cat->aggregate}})
                                    </span>
                                </a>
                            </div>
                            @endif
                            @endforeach
                        </div>

                    </div>
                    <div class="expand">
                        <div class="row">
                            @foreach($popularCats as $key => $cat)
                            @if($key >= $fixed_count)
                            <div class="col-sm-6 create_post_btn">
                                <a class=" btn {{$labels[$key]}}" href="{{route('frontend.user.home')}}?category[]={{$cat->category_id}}">
                                    <span class="label {{$labels[$key]}}">
                                        {{$cat->name_clean}}({{$cat->aggregate}})
                                    </span>
                                </a>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @if(count($popularCats)>10)
                    <div class="show_more_data">Show More</div>
                    @endif

                    <div class="show_less_data">Show Less</div>
                </div>
            </div>
            <div class="item">
                <div class="right-sidebar">
                    <div class="">
                        <div class="main_bx main_bx_pr_box">
                            <div class="item">
                                <div class="post-bar">
                                    <div class="post_topbar">
                                        <div class="usy-dt post-bar-company com-dt">
                                            <a href="https://kingcounty.adoptcompany.com/profile/2">
                                                <img src="https://kingcounty.adoptcompany.com/public/storage/15760721011562852332pexels-photo-302804 (1).jpeg" class="profile">
                                            </a>
                                            <div class="usy-name">
                                                <a href="https://kingcounty.adoptcompany.com/profile/2">
                                                    <h3>{{ auth()->user()->company_name}}</h3>

                                                </a>
                                                <div class="donate_reques">
                                                    <label>Donated</label> <span> : </span> <span> {{$helpCnt[1]->aggregate ?? 0}} </span>
                                                </div>
                                                <div class="donate_reques">
                                                    <label>Requested</label> <span> : </span> <span> {{$helpCnt[0]->aggregate ?? 0}} </span>
                                                </div>
                                                <span>The original boundaries of King County were defined in December 22, 1852 as follows:
                                                    Commencing at the northeast corner of Pierce County, thence along the Cascade Mountains to a parallel passing</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item help_s">
                @if(count($news))
                <div class="widget widget-jobs">
                    <div class="sd-title">
                        <h3>News and Alerts</h3>
                    </div>
                    <div class="news_con clearfix mCustomScrollbar">
                        @foreach($news as $new)
                        <div class="news_b clearfix">

                            <div class="jobs-list">
                                <div class="ed-opts">
                                    <span class="badge {{($new->type == 1 ? 'badge-primary' : ($new->type == 2 ? 'badge-secondary' : ($new->type == 3 ? 'badge-tiranry' : '')) ) }}"> {{($new->type == 1 ? 'Alerts' : ($new->type == 2 ? 'Announcement' : ($new->type == 3 ? 'Success Stories' : '')) ) }}</span>
                                </div>
                                <div class="pdf_data tab_data">
                                    <h6>{{$new->created_at->format('m-d-Y')}}</h6>
                                    <!-- <a href="{{route('frontend.alertsView',$new->id)}}" class="kn_more"> -->
                                    <h3>{{$new->title}}</h3>
                                    <!-- </a> -->

                                    <p>{!! str_limit($new->content,200) !!}</p>
                                    @if(strlen($new->content) > 200)
                                    <p style="text-align: right"><a class="" href="{{route('frontend.alertsView',$new->id)}}"> Read more </a></p>
                                    @endif
                                </div>
                            </div>

                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>


        <div class="dash_right">
            <div class="item">
                <div class="right-sidebar">
                    <div class="filter-secs">
                        <div class="sd-title">
                            <h3>Available Donated Items</h3>
                        </div>
                        <div class="clearfix"></div>
                        <!--filter-heading end-->

                        <div id="contain">

                            @foreach($posts as $key=>$post)
                            <div class="dona_con">
                                <h2>
                                    <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id,'post_title'=>str_slug($post->title)])}}">
                                        {{$post->title}}
                                    </a>
                                </h2>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <a class="twitter-timeline" data-lang="en" data-width="278" data-height="400" data-theme="light" href="https://twitter.com/NYNJCouncil?ref_src=twsrc%5Etfw">Tweets by KingCountyWA</a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>

            <div class="item">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FNYNJMSDC%2F&tabs=timeline&width=278&height=350&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=245610095492594" width="278" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
            </div>
            <div class="item">
                <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                <div class="elfsight-app-b7d59dd0-d7a3-4068-824e-7625768f73bf"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="post-popup pst-pj" id="termsPop">
    <div class="post-project">
        <h3>Terms & Conditions</h3>
        <div class="post-project-fields">
            <p>
                Thank you for your donation to businesses in need. By clicking on the ‘OK’ button, you agree that Technology Tools Innovation LLC and its affiliates, such as The New York and New Jersey MSDC, assumes no responsibility whatsoever for any of the products or services exchanged from this site. 
            </p>
            <a class="btn btn-warning" style="text-align: center" id="cancel">Ok</a>
        </div>
        <!--post-project-fields end-->
        <a href="#" title=""><i class="la la-times-circle-o"></i></a>
    </div>
    <!--post-project end-->
</div>

<div class="post-popup pst-pj" id="notifPop">
    <div class="notification_popup">
        <div class="notification_title">
            <h3>Pending Actions</h3>
            <div class="close_con" id="closePop">
                <i class="la la-times-circle-o"></i>
            </div>
        </div>
        <div class="notification_details_container mCustomScrollbar">
            @if ($pending_actions_count > 0)
            @foreach ($pending_actions as $action)
            <div class="notification_details">
                <div class="notf">
                    @if($action->user->avatar_location != "")
                    <img src="{{App\Helpers\Helper::getProfileImg( $action->user->avatar_location) }}" class="img img-rounded img-fluid" />
                    @else
                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />
                    @endif
                </div>
                <div class="notf_de">
                    <p>
                        <b>{{$action->user->company_name}}</b>
                        company is interested in the item you posted in 
                        <a href="/post-details/{{$action->post_id}}/{{str_slug($action->post->title)}}" target="_blank">{{$action->post->title}}</a>
                    </p>
                    <h4>Do you want to approve?</h4>
                    <p>{{$action->created_at->diffForHumans()}}</p>
                </div>
                <div class="notf_yn">
                    @php
                    $user_post_apply = App\Models\UserPostApply::where(['post_id'=>$action->post_id,'user_id'=>$action->user_id])->first();
                    @endphp
                    <input type="hidden" name="post_apply_id" value="{{$user_post_apply->id}}">
                    <input type="hidden" name="is_reply_to_id" value="{{$action->comment_id}}">
                    <input type="hidden" name="notif_id" value="{{$action->id}}">
                    <span>
                        <input type="button" class="btn btn-sm btn-success btn-submit" name="submit" value="Yes">
                    </span>
                    <span>
                        <input type="button" class="btn btn-sm btn-danger btn-submit" name="submit" style="background: red;" value="No">
                    </span>
                </div>
                <div class="clearfix"></div>
            </div>
            @endforeach
            @else
            <div class="notfication-details">
                <div class="notf_de">
                    <h3>Currently there are no pending actions in your account.</h3>
                </div>
                <!--notification-info -->
            </div>
            <!--notfication-details end-->
            @endif
        </div>
    </div>
</div>
@stop

@push('after-scripts')
<script type="text/javascript">
                            $(document).ready(function () {
                                if (localStorage.getItem("dashboard_popup") == undefined || localStorage.getItem("dashboard_popup") == "false") {
                                    $("#termsPop").addClass("active");
                                    $(".wrapper").addClass("overlay");
                                }

                                localStorage.setItem("dashboard_popup", true);
                            });

                            $('#cancel').on('click', function () {
                                $("#termsPop").removeClass("active");
                                $("#notifPop").addClass("active");
                            });

                            $('#closePop').on('click', function () {
                                $("#notifPop").removeClass("active");
                                $(".wrapper").removeClass("overlay");
                            });

                            $('.btn-submit').on('click', function () {
                                var btnEle = $(this);
                                var post_apply_id = $(btnEle).closest('div.notf_yn').find('input[name="post_apply_id"]').val();
                                var is_reply_to_id = $(btnEle).closest('div.notf_yn').find('input[name="is_reply_to_id"]').val();
                                var notif_id = $(btnEle).closest('div.notf_yn').find('input[name="notif_id"]').val();
                                var action = $(btnEle).val();
                                var parent = $(btnEle).parent().parent().parent();
                                $('.loader-container').css('z-index', '999999');

                                var act = 'Rejected';
                                if (action == 'Yes') {
                                    var act = 'Approved';
                                }

                                $.ajax({
                                    url: '{{route("frontend.user.approve")}}',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {post_apply_id: post_apply_id, is_reply_to_id: is_reply_to_id, notif_id: notif_id, submit: action},
                                    success: function (data) {
                                        var html = '<div class="alert-info alert-dismissable" style="padding: 10px;">Request ' + act + ' Successfully!!!</div>';
                                        $(parent).html(html);
                                    },
                                    error: function (xhr) {
                                        alert('Some error occurred. Please try again!!');
                                    },
                                    beforeSend: function (xhr) {
                                        var html = '<b>Please Wait....</b>'
                                        $(btnEle).parent().parent().html(html);
                                    }
                                })
                            });
</script>
@endpush