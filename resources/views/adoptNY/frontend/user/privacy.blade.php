@extends('frontend.layouts.app')
<title>Privacy Policy - @yield('title', app_name())</title>

@section('content')
<style>
    ul.list-styled { padding:0 0 0 18px !important;}
    .list-styled li{  list-style-type: inherit !important; text-align:justify;}
    .privacy-policy h3{font-size:16px; font-weight:700; line-height:24px;}
    .update-time {float:left;width:100%}
    .update-time small {
        padding: 0 0 0 15px;
        margin-top: -8px;
    }
    .privacy-policy p{text-align:justify;}
</style>

<div class="container " >
    <div class=" col-sm-12 privacy-policy">
        <div class="page-header">
            <h1> Technology Tools Innovation LLC Privacy Policy </h1>
            <div class="update-time">
                <small class="pull-right "> Last Updated: 9 September 2019 </small>
            </div>
        </div>

        <p> This Privacy Policy (“Policy”) describes the manner in which Technology Tools Innovation LLC (“[TTI]”, “we,” or “us”) and our website at<a href="https://techtoolsinnovation.com/" target="_blank"> www.techtoolsinnovation.com </a> (the “Site”), as well as all related websites, networks, applications, and other services provided by us and on which a link to this Policy is displayed (collectively, together with the Site, our “Service”) use and collect data from individuals. This Policy, which is incorporated into and is subject to the [TTI] Terms of [Use / Service], describes the information that we gather from you on the Service, how we use and disclose such information, and the steps we take to protect such information. </p>

        <h3> Information We Collect on the Service: </h3>
        <ul class="list-styled">
            <li>	<b>User-provided Information.</b>  When you use the Service, we may collect information about you, including your name, email address, mailing address, mobile phone number, credit card or other billing information, your date of birth, geographic area, or preferences and we may link this information with other information about you. You may provide us with information in various ways on the Service. For example, you provide us with information when you register for an account, use the Service, make a purchase on the Service, or send us customer service-related requests.</li>

            <li><b>	Cookies and Automatically Collected Information.</b> When you use the Service, we may send one or more cookies – small text files containing a string of alphanumeric characters – to your device.  We may use both session cookies and persistent cookies. A session cookie disappears after you close your browser. A persistent cookie remains after you close your browser and may be used by your browser on subsequent visits to the Service. Please review your web browser “Help" file to learn the proper way to modify your cookie settings. Please note that if you delete, or choose not to accept, cookies from the Service, you may not be able to utilize the features of the Service to their fullest potential.  [We may use third party cookies on our Service as well.  For instance, we use Google Analytics to collect and process certain analytics data.  Google provides some additional privacy options described at www.google.com/policies/privacy/partners/ regarding Google Analytics cookies.  [TTI] does not process or respond to web browsers’ “do not track” signals or other similar transmissions that indicate a request to disable online tracking of users who visit our Site or who use our Service.] [NTD: Also describe MixPanel, Flurry, or other relevant third-party services if applicable.  Change DNT if inappropriate.]</li>

            <li>We may also automatically record certain information from your device by using various types of technology, including “clear gifs" or “web beacons.” This automatically collected information may include your IP address or other device address or ID, web browser and/or device type, the web pages or sites that you visit just before or just after you use the Service, the pages or other content you view or otherwise interact with on the Service, and the dates and times that you visit, access, or use the Service. We also may use these technologies to collect information regarding your interaction with email messages, such as whether you opened, clicked on, or forwarded a message. This information is gathered from all users, and may be connected with other information about you.</li>

            <li><b>	[Location Information.</b>  We may obtain information about your physical location, such as by use of GPS and other geolocation features in your device, or by inference from other information we collect (for example, your IP address indicates the general geographic region from which you are connecting to the Internet).] [NTD: Also describe other relevant categories of data, e.g., biometrics/healthkit data, financial data, communications data, etc.]</li>

            <li>	<b>Third Party Web Beacons and Third Party Buttons.</b> We may display third-party content on the Service, including third-party advertising.  Third-party content may use cookies, web beacons, or other mechanisms for obtaining data in connection with your viewing of the third party content on the Service. Additionally, we may implement third party buttons (such as Facebook “like” or “share” buttons) that may function as web beacons even when you do not interact with the button. Information collected through third-party web beacons and buttons is collected directly by these third parties, not by [TTI]. Information collected by a third party in this manner is subject to that third party’s own data collection, use, and disclosure policies. </li>
            <li>	<b>Information from Other Sources.</b> We may obtain information from third parties and sources other than the Service, such as our partners and advertisers. </li>
        </ul>
        <h3> How We Use the Information We Collect. We use information we collect on the Service in a variety of ways in providing the Service and operating our business, including the following:</h3>
        <ul class="list-styled">


            <li>	We use the information that we collect on the Service to operate, maintain, enhance and provide all features of the Service, to provide services and information that you request, to respond to comments and questions and otherwise to provide support to users, and to process and deliver entries and rewards in connection with promotions that may be offered from time to time on the Service. </li>

            <li>	We use the information that we collect on the Service to understand and analyze the usage trends and preferences of our users, to improve the Service, and to develop new products, services, features, and functionality. </li>

            <li>	We may use your email address or other information we collect to contact you for administrative purposes such as customer service or to send communications, including updates on promotions and events, relating to products and services offered by us and by third parties. </li>

            <li><b>	We may use cookies and automatically collected information to: </b>(i) personalize our Service, such as remembering information about you so that you will not have to re-enter it during your visit or the next time you visit the Service; (ii) provide customized advertisements, content, and information; (iii) monitor and analyze the effectiveness of the Service and third-party marketing activities; (iv) monitor aggregate site usage metrics such as total number of visitors and pages viewed; and (v) track your entries, submissions, and status in any promotions or other activities on the Service.</li>
        </ul>
        <h3>When We Disclose Information. Except as described in this Policy, we will not disclose information about you that we collect on the Service to third parties without your consent. We may disclose information to third parties if you consent to us doing so, as well as in the following circumstances:</h3>
        <ul class="list-styled">


            <li>	Any information that you voluntarily choose to include in a publicly accessible area of the Service will be available to anyone who has access to that content, including other users.</li> 

            <li>	We work with third party service providers to provide website, application development, hosting, maintenance, and other services for us. These third parties may have access to or process information about you as part of providing those services for us. Generally, we limit the information provided to these service providers to that which is reasonably necessary for them to perform their functions, and we require them to agree to maintain the confidentiality of such information.</li>

            <li>	We may disclose information about you if required to do so by law or in the good-faith belief that such action is necessary to comply with state and federal laws, in response to a court order, judicial or other government subpoena or warrant, or to otherwise cooperate with law enforcement or other governmental agencies. </li>

            <li>	We also reserve the right to disclose information about you that we believe, in good faith, is appropriate or necessary to: (i) take precautions against liability; (ii) protect ourselves or others from fraudulent, abusive, or unlawful uses or activity; (iii) investigate and defend ourselves against any third-party claims or allegations; (iv) protect the security or integrity of the Service and any facilities or equipment used to make the Service available; or (v) protect our property or other legal rights (including, but not limited to, enforcement of our agreements), or the rights, property, or safety of others.</li>

            <li>	Information about our users may be disclosed and otherwise transferred to an acquirer, successor, or assignee as part of any merger, acquisition, debt financing, sale of assets, or similar transaction, or in the event of an insolvency, bankruptcy, or receivership in which information is transferred to one or more third parties as one of our business assets.</li>

            <li>	We may make certain aggregated, automatically-collected, or otherwise non-personal information available to third parties for various purposes, including: (i) compliance with various reporting obligations; (ii) for business or marketing purposes; or (iii) to assist such parties in understanding our users’ interests, habits, and usage patterns for certain programs, content, services, advertisements, promotions, and/or functionality available through the Service.</li>
        </ul>

        <h3> Your Choices </h3>

        <p>You may, of course, decline to share certain information with us, in which case we may not be able to provide to you some of the features and functionality of the Service. You may update, correct, or delete your account information and preferences at any time by accessing your account preferences page on the Service. If you wish to access or amend any other personal information, we hold about you, you may contact us at info@techtoolsinnovation.com. Please note that while any changes you make will be reflected in active user databases within a reasonable period of time, we may retain all information you submit for backups, archiving, prevention of fraud and abuse, analytics, satisfaction of legal obligations, or where we otherwise reasonably believe that we have a legitimate reason to do so. </p>

        <p>If you receive commercial email from us, you may unsubscribe at any time by following the instructions contained within the email. You may also opt out from receiving commercial email from us by sending your request to us by email info@techtoolsinnovation.com or by writing to us at the address given at the end of this policy. We may allow you to view and modify settings relating to the nature and frequency of promotional communications that you receive from us in user account functionality on the Service.</p>

        <p>Please be aware that if you opt out of receiving commercial email from us or otherwise modify the nature or frequency of promotional communications you receive from us, it may take up to ten business days for us to process your request, and you may continue receiving promotional communications from us during that period. Additionally, even after you opt out from receiving commercial messages from us, you will continue to receive administrative messages from us regarding the Service.</p>

        <h3>Third-Party Services</h3>

        <p>The Service may contain features or links to websites and services provided by third parties. Any information you provide on third-party sites or services is provided directly to the operators of such services and is subject to those operators’ policies, if any, governing privacy and security, even if accessed through the Service. We are not responsible for the content or privacy and security practices and policies of third-party sites or services to which links or access are provided through the Service. We encourage you to learn about third parties’ privacy and security policies before providing them with information.</p>

        <h3>Children’s Privacy</h3>

        <p>Protecting the privacy of young children is especially important. Our Site is a general audience site not directed to children under the age of 13, and we do not knowingly collect personal information from children under the age of 13 without obtaining parental consent. </p>

        <h3>Data Security</h3>

        <p>We use certain physical, managerial, and technical safeguards that are designed to improve the integrity and security of information that we collect and maintain. Please be aware that no security measures are perfect or impenetrable.  We cannot and do not guarantee that information about you will not be accessed, viewed, disclosed, altered, or destroyed by breach of any of our physical, technical, or managerial safeguards.  </p>

        <h3>International Visitors</h3>

        <p>The Service is hosted in the United States [and is intended for visitors located within the United States]. If you choose to use the Service from the European Union or other regions of the world with laws governing data collection and use that may differ from U.S. law, then please note that you are transferring your personal information outside of those regions to the United States for storage and processing. Also, we may transfer your data from the U.S. to other countries or regions in connection with storage and processing of data, fulfilling your requests, and operating the Service. By providing any information, including personal information, on or to the Service, you consent to such transfer, storage, and processing. </p>

        <h3>Changes and Updates to this Policy </h3>

        <p>Please revisit this page periodically to stay aware of any changes to this Policy, which we may update from time to time. If we modify this Policy, we will make it available through the Service, and indicate the date of the latest revision. In the event that the modifications materially alter your rights or obligations hereunder, we will make reasonable efforts to notify you of the change. For example, we may send a message to your email address, if we have one on file, or generate a pop-up or similar notification when you access the Service for the first time after such material changes are made. Your continued use of the Service after the revised Policy has become effective indicates that you have read, understood and agreed to the current version of this Policy.</p>

        <h3>Your California Privacy Rights</h3>

        <p>Residents of California have the right to request a disclosure describing what types of personal information we have shared with third parties for their direct marketing purposes, and with whom we have shared it, during the preceding calendar year.  You may request a copy of that disclosure by contacting us at info@techtoolsinnovation.com.</p>

        <h3>How to Contact Us</h3>

        <p>Please contact us with any questions or comments about this Policy, information we have collected or otherwise obtained about you, our use and disclosure practices, or your consent choices by email at info@techtoolsinnovation.com. </p>
        <address> 
            Tech Tools Innovation, <br>
            7444 E Chapman,<br>
            Unit G Orange CA 92869.<br>
        </address> 

    </div>
</div>
@endsection