@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')

<!-- Page Content -->
<div class="container ">



  <div class="col-sm-12">
    <div class="row">

      <div class="col-sm-12 ">

        <h3 align="center">Help Seeker Analysis</h3>
        <form class="seeker-analy">

          <div class="row">
            <div class="col-sm-12">
              <h4> Filter </h4>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>From </label>
                <input type="date" class="form-control">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>To </label>
                <input type="date" class="form-control">

              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>Categories</label>
                <select class="form-control" name="" id="location1" style="width: 170px;">
                  <option value="">Financial Solution</option>
                  <option value="">Tax Services</option>
                  <option value="">Attorney </option>
                  <option value="">Computers </option>
                  <option value="">Printer </option>
                  <option value="">WareHouse </option>
                  <option value="" selected="">Desk Space </option>
                  <option value="">Tank </option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>Location</label>
                <select class="form-control" name="" id="location1" style="width: 181px;">
                  <option value="">Arcadia</option>
                  <option value="">Malibu</option>
                  <option value="">West Covina</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>Search</label>
                <input type="text" placeholder="company name" class="form-control">
              </div>
            </div>

            <div class="col-md-1">
              <div class="submit-btn-seek-analy">

                <p class="text-right"><a href="#" class="btn btn-info " role="button"> <i class="glyphicon glyphicon-filter"> </i>Submit </a> </p>
              </div>
            </div>
          </div>






        </form>

      </div>

    </div>




    <div class="row">
      <div class="col-sm-12">
        <div class="p-inbox">

          <div class="list-group">

            <div class="priority-box">
              <a class="list-group-item list-heading">
                <span class="name span_pri" style="font-weight: bold">Company Name</span> <span style="font-weight: bold"> Post Title</span>

                <span style="font-weight: bold; float:right">Time</span>
              </a>
            </div>

            <div class="priority-box">
              <a href="post-details.html" class="list-group-item">
                <span class="name span_pri">Antec</span> <span>7 people private Cabin desk space</span>

                <span class="badge badge-warning">Now</span>
              </a>
            </div>



            <div class="priority-box">
              <a href="post-details.html" class="list-group-item">
                <span class="name span_pri">Autodesk</span> <span>300 sqft. open space </span>

                <span class="badge badge-warning">1:29 PM</span>
              </a>
            </div>
            <div class="priority-box">
              <a href="post-details.html" class="list-group-item">
                <span class="name span_pri">C1rca</span> <span>10 people desk space with 2 private cabin</span>

                <span class="badge badge-warning">3:45 PM</span>
              </a>
            </div>
            <div class="priority-box">
              <a href="post-details.html" class="list-group-item">
                <span class="name span_pri">Elanex</span> <span>25 people desk space with intercom and conference room</span>

                <span class="badge badge-warning">7 May</span>
              </a>
            </div>



          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div id="map" style="height: 400px; width:100%;"> </div>
      </div>
    </div>


  </div>
</div>


@endsection

@push('after-scripts')


<script type="text/javascript">
  // Load google charts
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  // Draw the chart and set the chart values
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 8],
      ['Eat', 2],
      ['TV', 4],
      ['Gym', 2],
      ['Sleep', 8]
    ]);

    // Optional; add a title and set the width and height of the chart
    var options = {
      'title': 'My Average Day',
      'width': 550,
      'height': 400
    };

    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
  }
</script>

<script type="text/javascript">
  // Load google charts
  google.charts.load('current', {
    'packages': ['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);

  // Draw the chart and set the chart values
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['Work', 8],
      ['Eat', 2],
      ['TV', 4],
      ['Gym', 2],
      ['Sleep', 8]
    ]);

    // Optional; add a title and set the width and height of the chart
    var options = {
      'title': 'My Average Day',
      'width': 550,
      'height': 400
    };

    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.PieChart(document.getElementById('piechart-1'));
    chart.draw(data, options);
  }
</script>

<script>
  google.charts.load('current', {
    packages: ['corechart', 'bar']
  });
  google.charts.setOnLoadCallback(drawAxisTickColors);

  function drawAxisTickColors() {
    var data = google.visualization.arrayToDataTable([
      ['City', 'Total Help', 'Total Request'],
      ['New York City, NY', 8175000, 8008000],
      ['Los Angeles, CA', 3792000, 3694000],
      ['Chicago, IL', 2695000, 2896000],
      ['Houston, TX', 2099000, 1953000],
      ['Philadelphia, PA', 1526000, 1517000]
    ]);

    var options = {
      title: 'Request from U.S. Cities',
      chartArea: {
        width: '50%'
      },
      hAxis: {
        title: 'Total Help and request',
        minValue: 0,
        textStyle: {
          bold: true,
          fontSize: 12,
          color: '#4d4d4d'
        },
        titleTextStyle: {
          bold: true,
          fontSize: 18,
          color: '#4d4d4d'
        }
      },
      vAxis: {
        title: 'City',
        textStyle: {
          fontSize: 14,
          bold: true,
          color: '#848484'
        },
        titleTextStyle: {
          fontSize: 14,
          bold: true,
          color: '#848484'
        }
      }
    };
    var chart = new google.visualization.BarChart(document.getElementById('bar-chart-1'));
    chart.draw(data, options);
  }
</script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">
  var locations = [
    ['Bondi Beach', -33.890542, 151.274856, 4],
    ['Coogee Beach', -33.923036, 151.259052, 5],
    ['Cronulla Beach', -34.028249, 151.157507, 3],
    ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
    ['Maroubra Beach', -33.950198, 151.259302, 1]
  ];

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(-33.92, 151.25),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow();

  var marker, i;

  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  }
</script>


@endpush