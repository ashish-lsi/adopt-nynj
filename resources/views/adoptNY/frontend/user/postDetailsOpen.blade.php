@extends('frontend.layouts.app')

@section('title', $post->title)
@section('meta_description', $post->article )
@section('meta-fb')
<meta property="og:url" content="{{url(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]))}}" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$post->title}}" />
<meta property="og:description" content="{{$post->article}}" />
<meta property="og:site_name" content="Adopt-a-Company" />
<meta property="og:image" content="https://nynjmsdc.adoptcompany.com/adoptNY/images/TTI-Adopt-a-Company-logo.png" />
@stop

@section('content')
<section class="forum-page">
    <div class="container">
        <div class="forum-questions-sec">
            <div class="row">
                <div class="col-lg-8 no-pdd">
                    <div class="forum-post-view forum-post-details">
                        <div class="usr-question">
                            <div class="usr_img">
                                <img src="images/resources/usrr-img1.png" alt="">
                            </div>
                            <div class="usr_quest">
                                <h3> {{$post->title}}</h3>
                                <span><i class="fa fa-clock-o"></i>{{$post->created_at->diffForHumans()}}</span>
                                <span class="share_us">
                                    <i class="fa fa-share-alt"></i>
                                </span>
                                <span class="share_icon">
                                    {!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), $post->title,['target' => '_blank'])
                                    ->facebook()
                                    ->twitter()                            
                                    ->linkedin('Extra linkedin summary can be passed here')
                                    !!}
                                    <li>
                                        <a href="mailto:?subject={{$post->title}}&amp;body={{route('frontend.social_post_details',[$post->post_id,str_slug($post->title)])}}" title="Share by Email">
                                            <span class="fa fa-envelope"></span>
                                        </a>
                                    </li>
                                </span>

                                <span class="badge {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}"> {{$post->type == 0 ? 'Help Seeker' : 'Help Giver' }}</span>

                                <span class="badge post_h_de {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}"> {{$post->type == 0 ? 'Help Seeker' : 'Help Provider' }}</span>

                                <p>{{$post->article}} </p>
                                <div class="category_left_in">
                                    <h4><b>Quantity:</b> {{$post->qnty}} </h4>
                                    <div>
                                        <ul class="like-com" style="width: 100%;">
                                            <li>
                                                @foreach($post->attachements as $attach)
                                                @if(in_array($attach->extension,['jpg','jpeg','png']))
                                                <a class="example-image-link" href="{{asset('public/storage/'.$attach->location )}}" data-lightbox="example-1">
                                                    <img src="{{asset('public/storage/thumb/'.$attach->thumb )}}">
                                                </a>
                                                @else
                                                <a class="example-image-link" target="_blank" href="{{asset('public/storage/'.$attach->location )}}">
                                                    <img src="{{asset('public/img/'.$attach->extension.'.png' )}}">
                                                </a>
                                                @endif
                                                @endforeach
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="company_detail">
                                        <div class="details">
                                            <label>Category </label> <span class="span_col">:</span>
                                            <span>{{$post->category_id != 11 ? $post->category->name : $post->category_name}}</span>
                                        </div>
                                        <div class="details">
                                            <label>Location </label> <span class="span_col">:</span>
                                            <span> {{ ($post->state) ? $post->state : "" }}
                                                {{ ($post->city) ? ", ".$post->city : "" }}
                                                {{ ($post->pincode) ? ", ".$post->pincode : "" }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="category_right_donate">
                                    @if($post->type == 1)
                                    <div class="applybt">
                                        <a href="{{route('frontend.auth.login')}}">
                                            <span>Get it here</span>
                                        </a>
                                    </div>
                                    @endif
                                    @if($post->type == 0)
                                    <div class="applybt">
                                        <a href="{{route('frontend.auth.login')}}">
                                            <span>Donate</span>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                                <div class="post-comment-box">
                                    <h3></h3>
                                    <div class="user-poster">
                                        <div class="usr-post-img">
                                            <img src="images/resources/bg-img2.png" alt="">
                                        </div>
                                        <!--post_comment_sec end-->
                                    </div>
                                    <!--user-poster end-->
                                </div>
                            </div>
                            <!--usr_quest end-->
                        </div>
                        <!--usr-question end-->
                    </div>
                    <!--forum-post-view end-->
                </div>
                <div class="col-lg-4">
                    <div class="main-left-sidebar no-margin">
                        <div class="user-data full-width">
                            <div class="user-profile">
                                <div class="username-dt">
                                    <div class="usr-pic">
                                        <img src="{{App\Helpers\Helper::getProfileImg($post->user->avatar_location)}}" alt="">
                                    </div>
                                </div>
                                <!--username-dt end-->
                                <div class="user-specs">
                                    <div class="sd-title no-bor">
                                        <h3><a href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}">{{$post->user->company_name}}</a></h3>
                                        <div class="details">
                                            @php
                                            $org_serve = App\OrganizationServes::whereIn('id',explode(',',$post->user->organization_serves))->get();
                                            @endphp
                                            @if( count($org_serve)>0)
                                            @php
                                            $srver = "";
                                            @endphp
                                            @foreach($org_serve as $os)
                                            @php
                                            $srver .= $os->name.",";
                                            @endphp
                                            @endforeach
                                            <p>{{rtrim($srver, ',')}}</p>
                                            @endif

                                            <div class="company_details">
                                                <h4>Company Details: </h4>
                                                <span>{{$post->user->description}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="user-fw-status">
                                    <li>
                                        <h4>Address</h4>
                                        <span> {{$post->user->address->address ?? ''}} </span>
                                    </li>

                                    <li>
                                        <h4>Contact details </h4>
                                        <span> {{$post->user->email ?? ''}}</span>
                                    </li>
                                    <li>
                                        <h4>Address</h4>
                                        <span> xxxx,xxxxxxxxxxxx </span>
                                    </li>

                                    <li>
                                        <h4>Contact details </h4>
                                        <span> xxxxxxxxxxxxxx</span>
                                    </li>

                                    <li class="no-bor">
                                        <div class="applybt-show show-contact">
                                            <a href="{{route('frontend.auth.login')}}">
                                                <button class="btn btn-primary createpost-btn">Show contact details</button>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!--user-profile end-->
                        </div>
                        <!--user-data end-->
                        <div class="suggestions full-width">
                            <div class="sd-title">
                                <h3>Post Location</h3>
                            </div>
                            <div class="suggestions-list suggestions-list-profile">
                                <div id="map"></div>
                            </div>

                            <div class="weather-desc" id="weather-desc">
                                <div class="sd-title">
                                    <h3>Weather alerts for this location</h3>
                                </div>
                                <div class="suggestions-list suggestions-list-profile suggestions-list-post">
                                    @if(isset($alertsdata->features) && count($alertsdata->features) > 0)
                                    @foreach ($alertsdata->features as $key => $alert)

                                    <li>{{$alert->properties->headline}}</li>

                                    @endforeach
                                    <br>
                                    {{ Form::open(array('url' => route('show_weather'), 'id'=>'frmWeather')) }}
                                    <input type="hidden" name="City" id="weather-city" value="{{$post->city}}" />
                                    <input type="hidden" name="State" id="weather-state" value="{{$post->state}}" />
                                    <input type="hidden" name="Pincode" id="weather-pincode" value="{{$post->pincode}}" />
                                    <input type="hidden" name="addr-lat" id="weather-lat" value="{{$post->lat}}" />
                                    <input type="hidden" name="addr-long" id="weather-long" value="{{$post->lang}}" />
                                    <input type="hidden" name="addr-country" id="weather-country" value="United States" />
                                    <input type="hidden" name="addr-name" id="weather-addr-name" value="{{$post->location}}" />
                                    <input type="submit" value="Click here for details" class="label label-success">
                                    {{ Form::close() }}
                                    @else
                                    <p>Currently there are no alerts for this location!!</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!--suggestions end-->
                    </div>
                    <!--main-left-sidebar end-->
                </div>
            </div>
        </div>
        <!--forum-questions-sec end-->
    </div>
</section>
@endsection

@push('after-styles')
<style type="text/css">
    #map {
        width: 340px;
        min-height: 300px;
        height: 100%;
        margin: 0px;
        padding: 0px;
    }

    .user-post-details h4 {
        margin-top: 10px;
        font-size: 20px;
        font-weight: 700;
    }

    .hidden {
        display: none;
    }

    .show-comment {
        margin-top: 10px;
        margin-left: 10px;
    }

    .show-comment button {
        margin: 0px;
    }

    .comment-profile {
        margin-right: 0;
        padding-right: 0;
    }

    .comment-profile img {
        width: 40px;
        float: right;
    }

    .comment-text {
        padding: 5px 8px;
        background-color: #efefef;
        float: left;
        width: 100%;
    }

    .comment-text span {
        float: left;
        width: 100%;
    }

    .btn-file {
        padding: 10px 0 15px;
        float: left;
    }

    .my-post a {
        text-align: right;
        padding: 10px 0px;
    }

    a.disabled {
        pointer-events: none;
        cursor: default;
    }

    .disabled-div {
        pointer-events: none;
        /* for "disabled" effect */
        opacity: 0.5;
        background: #CCC;
    }

    .disabled-lnk {
        pointer-events: none;
        cursor: default;
        text-decoration: none;
        color: black;
    }

    .btnContact {
        float: right;
        margin-left: 10px;
    }

    .comments-box {
        display: none;
        margin-top: 1px;
    }

    .down {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }
</style>
@endpush

@push('after-scripts')
<!-- {!! script(theme_url('js/lightbox-plus-jquery.min.js')) !!} -->
<script type="text/javascript">
    $(document).ready(function () {
        //Init map
        initPostDetailsMap('{{$post->lat}}', '{{$post->lang}}');
    });
</script>
@endpush