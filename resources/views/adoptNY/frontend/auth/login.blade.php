@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
<style>
    .signin-header {
        width: 1200px;
    }
</style>


<div class="sign-header">
    <div class="wrapper">
        <div class="sign-in-page">
            <div class="signin-header">
                <div class="signin-head">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="login-sec">

                                <div class="sign_in_sec current tab-1" style="width:87%">
								
								 <div class="row">
								 <div class="col-sm-6">
                                    @if($errors->any())
                                    <div class="alert alert-danger">
                                        <p>{{$errors->first()}}</p>
                                    </div>
                                    @endif

                                    @if ($message = Session::get('flash_success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                    @endif

					            <p ><b> Welcome to The Council's Adopt A Company Program</b>  </p>
                                    
                                    <p style="font-size: 14px">
                                         The Council understands that life has changed dramatically for us all. Our goal to support, offer guidance, and bring you the information that matters to you and your businesses hasn’t changed. With that in mind we present to you “Adopt a Company Program”!
                                    </p>
<p style="font-size: 14px">
                                       What better way for businesses to pay it forward. Help a business owner that is facing similar challenges as you may have in the past. Knowledge is power! Here we have created a platform for like-minded individuals to come together and adopt a company that they can offer their expertise to. Pay it forward! It’s our responsibility to make sure our businesses thrive in these ever changing times. It starts with US! Join us today and make a difference – adopt a company today.

Please email <a href="mailto:Lsantos@nynjmsdc.org"> Lsantos@nynjmsdc.org</a>  if you are unable to register or if you need assistance.
                                    </p>


                                   </div>
                                    <div class="col-sm-6">

                                    <h3>Sign in</h3>
                                    {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                                        <div class="row">
                                            <div class="col-lg-12 no-pdd">
                                                <div class="sn-field">
                                                    <input class="form-control" type="email" name="email" id="email" value="" placeholder="Email" maxlength="191" required="">
                                                </div>
                                                <!--sn-field end-->
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                                <div class="sn-field">
                                                    {{ html()->password('password')
                                                            ->class('form-control')
                                                            ->placeholder(__('validation.attributes.frontend.password'))
                                                            ->required() }}
                                                    <span><i class="fa fa-eye" id="showPwd"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                                <div class="checky-sec">
                                                    <div class="fgt-sec">
                                                        <!--                                                        {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}-->
                                                    </div>
                                                {{ form_submit(__('labels.frontend.auth.login_button'))->class('btn-info pull-left f_left') }}

                                                    <a href="{{ route('frontend.auth.password.reset') }}">@lang('labels.frontend.passwords.forgot_password')</a>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 no-pdd">
                                            <a href="{{route('frontend.auth.register')}}" class="btn btn-success pull-right " style="color: white">Register here</a>

                                            </div>
                                        </div>
                                  

                                    {{ html()->form()->close() }}
                                    </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@endsection

@push('after-scripts')
<script>
    $(document).ready(function() {
        localStorage.setItem("dashboard_popup", "false");

    });
</script>

@endpush