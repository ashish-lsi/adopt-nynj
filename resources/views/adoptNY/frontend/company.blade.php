@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')

<section class="profile-account-setting profile-account-post profile-account-company">
    <div class="container">
        <div class="account-tabs-setting filter-post-company">
            <div class="row">
                <div class="col-lg-3">
                    <div class="right-sidebar">
                        <div class="filter-secs">
                            <div class="sd-title">
                                <h3>Find company</h3>
                            </div>
                            <!--filter-heading end-->
                            <div class="paddy">
                                {{ Form::open(array('url' => route('frontend.user.findCompany'), 'method' => 'get', 'id' => 'frmSearch')) }}

                                <div class="filter-dd">
                                    <input name="q" type="text" value="{{Input::get('q')}}" placeholder="Search" aria-label="Search">
                                </div>
                                <div class="filter-dd">
                                    <!-- <form class="job-tp"> -->
                                    <select name="company_type">
                                        <option value=""> Company Type</option>
                                        @foreach($company_type as $com_type)
                                        <option value="{{$com_type->company_type_id}}" {{Input::get('company_type') == $com_type->company_type_id ? 'selected' : ''}}> {{$com_type->name}}</option>
                                        @endforeach
                                    </select>
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    <!-- </form> -->
                                </div>

                                <div class="filter-dd br-dd">
                                    <select name="organization_serves[]" class="select-text chkmore" multiple>
                                        <option value="">Select Organization Serve</option>
                                        @foreach ($organization_serves as $os)
                                        <option value="{{$os->id}}" {{Input::has('organization_serves') && in_array($os->id,Input::get('organization_serves')) ? 'selected':''}}>{{$os->name}}</option>
                                        @endforeach
                                    </select>
                                    <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                </div>

                                <div class="filter-dd">
                                    <input name="location" type="text" value="{{Input::get('location')}}" placeholder="Location" aria-label="Location">
                                </div>
                                <div class="filter-dd">
                                    <input name="state" type="text" value="{{Input::get('state')}}" placeholder="State" aria-label="State">
                                </div>
                                <div class="filter-dd">
                                    <input name="city" type="text" value="{{Input::get('city')}}" placeholder="City" aria-label="City">
                                </div>

                                <div class="">
                                    <div class="fl_lft">
                                        <button class="btn btn-primary weather-btn mr-left" id="btnSubmit">Submit</button>
                                    </div>
                                    <div class="fl_lft">
                                        <button class="btn btn-primary weather-btn" id="btnReset">Reset</button>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 ">
                    <div class="filter_box clearfix">
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active show">
                                <div class="main-ws-sec">
                                    <div class="posts-section">
                                        <div class="clearfix"></div>
                                        <div id="postList" class="home-podcast">
                                            @include('frontend.includes.companyCompact')
                                        </div>
                                        <div class="process-comm hidden">
                                            <div class="spinner">
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--posts-section end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--account-tabs-setting end-->
        </div>
    </div>
</section>
@endsection
@push('after-scripts')
<script>
    $(document).ready(function () {
        $('#btnReset').click(function () {
            $('input').each(function () {
                $(this).val('');
            });
            $('select').each(function () {
                $(this).val('');
            });
        })

        $('#btnSubmit').click(function () {
            $('#frmSearch').submit();
        })
    })
</script>
<script>
    var loc = location.href;
    if (loc.indexOf("?") === -1)
        loc += "?";
    else
        loc += "&";
    var currentUrl = loc;
    var page = 1;
    var noMorePost = false;
    $(window).scroll(function () {
        // console.log($(window).scrollTop() + $(window).height() );
        var scrollHeight = $(window).scrollTop() + $(window).height();
        if (scrollHeight + 1 >= $(document).height() && !noMorePost) {
            //Your code here\
            $('.process-comm').removeClass('hidden');
            $.ajax({
                url: currentUrl + 'page=' + (page + 1),
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if (data.count != 0) {
                        page++;
                        $('#postList').append(data.html);
                        $('.process-comm').addClass('hidden');
                    } else {
                        // console.log(data.hmtl);
                        $('#postList').append(data.html);
                        $('.process-comm').addClass('hidden');
                        noMorePost = true;
                    }

                    //console.log(data);
                }
            })
        }
    });
</script>
@endpush