@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . 'Report an issue')

@section('content')
<section class="forum-page hrlp_co">
    <div class="container">
        <div class="row">
            @if($errors->any())
            <div class="alert alert-danger">
                <p>{{$errors->first()}}</p>
            </div>
            @endif

            @if ($message = Session::get('flash_success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <div class="col-lg-6 pr_ho">
                <div class="sd-title">
                    <h4>Frequently asked questions </h4>
                    <div class="faq_content">
                        <div class="acc">
                            <h3>What are the steps for Signing up?</h3>
                            <div class="content" style="">
                                <div class="content-inner">
                                    <ul>
                                        <li>Click on the Sign-up button
                                        </li>
                                        <li>Enter all information requested on the form  
                                        </li>
                                        <li>Click on the captcha code and accept terms and conditions
                                        </li>
                                        <li>Submit
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Accordion Starts-->
                        <div class="acc">
                            <h3>How to create posts?</h3>
                            <div class="content" style="display: none;">
                                <div class="content-inner">
                                    <ul>
                                        <li>You may click either on the Donate, find help or Create Post button</li>
                                        <li>Enter the following information:</li>

                                        <div class="inside_ui">
                                            <ul>
                                                <li>Title of what you want to donate</li>
                                                <li>Description – Describing the item in detail 
                                                </li>
                                                <li>Quantity – Number of items to donate
                                                </li>
                                                <li>Category – Which category does the item fall under
                                                </li>
                                                <li>Type of help – Select the radio button to confirm if you are: Seeking help or Providing help
                                                </li>
                                                <li>Location – You may either select a pre-entered location from your profile or opt for a new location by clicking Other
                                                </li>
                                                <ul class="ininside_ui">
                                                    <li>If other is clicked, input your location and the system will autofill the remaining fields of State, City and Zip code
                                                    </li>
                                                </ul>
                                                <li>Upload – You may upload pictures of the product you wish to offer or receive
                                                </li>
                                                <li>Submit the post</li>


                                            </ul>
                                        </div>
                                        <li>The post will now reflect in the category which you selected. You may also view the post in your profile
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Accordian -->
                        <div class="acc">
                            <h3>What to use search bar for? </h3>
                            <div class="content" style="">
                                <div class="content-inner">
                                    <p> you may search for any post or item via the search bar given on top of the page</p>
                                    <ul class="inside_ui">

                                        <li> Select the 3 dots on the search bar to reveal the search options. These allow you to conduct following searches:</li>
                                    </ul>
                                    <ul class="inninside_ui isd">
                                        <li>Posts only seeking help
                                        </li>
                                        <li>Posts only providing help
                                        </li>
                                        <li>Posts for products under a specific category
                                        </li>
                                        <li>Posts from a certain City, State or Zipcode
                                        </li>
                                    </ul>
                                    <ul class="inside_ui">
                                        <li>You may reset your search anytime by clicking on the Reset button
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Accordian -->
                        <div class="acc">
                            <h3>How you can complete a transaction?</h3>
                            <div class="content" style="">
                                <div class="content-inner">
                                    <p> To complete a transaction and facilitate the donation between both parties, a user will follow these steps:</p>

                                    <ul>
                                        <li>Click on the “Help Seeker” or “Help Provider” post</li>
                                        <li>On the post, a user (Called User 1) may either donate or Request for help by clicking the respective buttons</li>
                                        <li>This will open a pop-up where the user 1 may input the details and select the quantity</li>
                                        <li>This will then trigger an email to user 2 who posted the information</li>
                                        <li>User 2 will approve the request, allowing the User 1 to view their contact information and take the transaction offline</li>
                                        <li>Once the transaction is completed, the user 2 may confirm the same on the tool</li>
                                    </ul>

                                </div>
                            </div>
                        </div>


                        <!-- Accordian -->
                        <div class="acc">
                            <h3>What do you find in Profile? </h3>
                            <div class="content" style="">
                                <div class="content-inner">
                                    <ul class="inside_ui">
                                        <li>Profile – This contains the user’s personal information, including their username, phone number, email address and their posts</li>
                                        <li>Company address – Which has their registered address
                                        </li>
                                        <li>My interactions – Information on all interactions they’ve had on the tool
                                        </li>
                                        <li>Awards &amp; Achievements – Any awards or achievements they want to list
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="sd-title">
                    <h4>Contact us</h4>
                    <br><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="report_issue">
                                <form action="{{route('frontend.contact.send')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <select name="issue" required>
                                        <option value="">Select</option>
                                        <option value="I am unable to register">I am unable to register</option>
                                        <option value="I am getting an error">I am getting an error</option>
                                        <option value="I am unable to post">I am unable to post</option>
                                        <option value="I am unable to reply to a post">I am unable to reply to a post</option>
                                        <option value="How do I close my post?">How do I close my post?</option>
                                        <option value="How do I delete my post?">How do I delete my post?</option>
                                    </select>

                                    <br><br>
                                    <input type="text" name="name" placeholder="Your name *" required value="{{ auth()->user()->company_name ?? '' }}"> <br><br>
                                    <input type="email" name="email" placeholder="Your email *" required value="{{ auth()->user()->email ?? '' }}"> <br><br>
                                    <input type="text" name="subject" placeholder="Subject *" required> <br><br>
                                    <input type="file" name="file"> <br><br>
                                    <textarea id="" cols="30" rows="10" placeholder="Message *" name="message" required></textarea>
                                    <button class="btn btn-primary weather-btn">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
    </div>
</section>
@endsection