<div id="accordion">
    @foreach($comments as $user)

    @php
    $comment = $user[0];
    $user_post_apply = App\Models\UserPostApply::where(['post_id'=>$post->post_id,'user_id'=>$comment->user->id])->first();
    $notification = App\Models\Notification::where('comment_id', $comment->comment_id)->first();
    @endphp

    <h3>
        @if($comment->user->avatar_location)
        <img src=" {{App\Helpers\Helper::getProfileImg($comment->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
        @else
        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
        @endif
        <p>{{is_null($comment->user->company_name) ? $comment->user->first_name .' '. $comment->user->last_name : $comment->user->company_name}}</p>
<!--        <span><div class="not_d">4</div></span>-->
        <i class="fa fa-angle-down"></i>
        <i class="fa fa-angle-up"></i>
    </h3>

    <div>
        <div class="comment-section">
            <div class="comment-sec">
                <div class="tab-pane" id="comments-{{$comment->comment_id}}" role="tabpanel" aria-labelledby="home-tab">
                    @foreach($user as $key => $comment)
                    <div class="row">
                        <div class="col-md-1 comment-profile no-pdd">
                            @if($comment->user->avatar_location)
                            <img src=" {{App\Helpers\Helper::getProfileImg($comment->user->avatar_location)}}" class="img img-rounded img-fluid profile-pic-xs" />
                            @else
                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid profile-pic-xs" />
                            @endif
                        </div>
                        <div class="col-md-11 comment-profile no-pdd">
                            <div class="comment-text">
                                <div class="clearfix disfx">
                                    <div class="fr_left">
                                        <a href="{{route('frontend.user.view-profile',['user'=>$comment->user->id])}}">
                                            <strong>{{is_null($comment->user->company_name) ? $comment->user->first_name .' '. $comment->user->last_name : $comment->user->company_name}}</strong>
                                        </a>
                                        @if($comment->type)
                                        <span><b>Quantity :</b> {{$user_post_apply->qnty ?? ''}}</span> 
                                        @endif
                                        <span class="sub_comm">{!! nl2br($comment->comment) !!}</span>

                                    </div>

                                    @if($user_post_apply && $user_post_apply->approved === 0 && auth()->user()->id != $post->company_id && $comment->type)
                                    <a class="btn btn-primary createpost-btn">Waiting For Approval</a>
                                    @elseif($user_post_apply && $user_post_apply->approved === 0 && auth()->user()->id == $post->company_id)
                                    <div class="fr_right fr_right_com_yn">
                                        @if($comment->type)
                                        <span>Do you want to approve?</span>
                                        <form action="{{route('frontend.user.approve')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="post_apply_id" value="{{$user_post_apply->id}}">
                                            <input type="hidden" name="is_reply_to_id" value="{{$comment->comment_id}}">
                                            <input type="hidden" name="notif_id" value="{{ $notification->id }}" />
                                            <input type="submit" class="btn btn-sm btn-success" name="submit" value="Yes">
                                            <input type="submit" class="btn btn-sm btn-danger" name="submit" style="background: red;" value="No">
                                        </form>
                                        @endif
                                    </div>
                                    @elseif($user_post_apply && $user_post_apply->help_provided == 0 && auth()->user()->id == $post->company_id && $comment->type)
                                    <a class="btn btn-danger createpost-btn postClose" data-post_id="{{$post->post_id}}" data-max_qnty='{{$post->qnty}}' data-is_reply_to_id="{{$comment->comment_id}}" data-post_apply_id="{{$user_post_apply->id}}">Close Request</a>
                                    @elseif($user_post_apply && $user_post_apply->approved == 1 && $user_post_apply->help_provided == 0 && $comment->type)
                                    <a class="btn btn-primary createpost-btn">Approved</a>
                                    @elseif($user_post_apply && $user_post_apply->approved == 2 && $user_post_apply->help_provided == 0 && $comment->type)
                                    <a class="btn btn-primary createpost-btn red">Rejected</a>
                                    @elseif($user_post_apply && $user_post_apply->approved == 1 && $user_post_apply->help_provided == 1 && $comment->type)
                                    <a class="btn btn-primary badge createpost-btn">Closed</a>
                                    @endif
                                </div>

                                @if($comment->attachment != "")
                                <span><i class="fa fa-paperclip"></i> <a href="/download/{{$comment->attachment}}">{{$comment->attachment}} </a></span>
                                @endif
                            </div>

                            <div class="comment-tool clearfix">
                                @if($post->status == 0)
                                <a class=" btn  ml-2 new-comment-box" data-id="R{{$comment->comment_id}}"> 
                                    <i class="fa fa-reply"></i> Reply</a>
                                @endif
                                <a class=" btn  ml-2" data-toggle="tooltip"> 
                                    <span class="fa fa-clock-o"> </span>
                                    {{$comment->created_at->diffForHumans()}}
                                </a>
                            </div>

                            <div class="show-comment comment-reply-box hidden" data-id="R{{$comment->comment_id}}">
                                <form method="post" class="commentForm" enctype="multipart/form-data" action="{{ route('frontend.user.comment') }}">
                                    <div class="input-group ">
                                        <!--<input type="text" name="body" id="userComment" class="form-control input-sm chat-input" placeholder="Write your message here..." />-->
                                        @csrf
                                        <p id="contentboxR{{$comment->comment_id}}" class="contentbox" contenteditable="true" data-id="R{{$comment->comment_id}}"></p>
                                        <div id="displayR{{$comment->comment_id}}" class="display"></div>
                                        <div id="msgboxR{{$comment->comment_id}}" class="msgbox"></div>
                                        <input type="hidden" id="commentBodyR{{$comment->comment_id}}" name="body">

                                        <input type="hidden" name="post_id" value="{{ $comment->post_id }}" />
                                        <input type="hidden" name="is_reply_to_id" value="{{ $comment->comment_id }}" />
                                        <input type="hidden" name="reply_user_id" value="{{ $comment->company_id }}" />

                                        <span class="input-group-btn">
                                            <button type="submit" class="commentBtn btn btn-info pull-right" data-id="R{{$comment->comment_id}}"><span class="glyphicon glyphicon-comment"></span> Reply</button>
                                        </span>
                                    </div>
                                </form>
                            </div>

                            <div class="show-comment " data-id="{{$comment->comment_id}}">
                                @include('frontend.includes.replyBox')
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!--comment-sec end-->
        </div>
    </div>
    @endforeach
</div>