@if(count($posts)> 0)
@foreach($posts as $post)
<div class="post-bar">
    <div class="post_topbar post_topbar_con">
        <div class="usy-dt">
            <a href="{{route('frontend.user.view-profile',['user'=>$post->user->id])}}">
                <img class="profile" src="{{App\Helpers\Helper::getProfileImg( $post->user->avatar_location) }}" alt="">
            </a>
            <div class="usy-name usy-name-2">
                <h3> <b> {{$post->user->company_name}} </b>   </h3>
            </div>
        </div>
        <div class="drop_down">
            <div class="drop_up"><i class="fa fa-angle-down"></i></div>
            <div class="drop_down"><i class="fa fa-angle-up"></i></div>
        </div>
        <div class="ed-opts">
            <span class="badge {{$post->type == 0 ? 'badge-primary' : 'badge-secondary' }}"> {{($post->type==0)? __('Help Seeker'):__('Help Giver')}}</span>

            @if($post->status == 1)
            <span class="badge badge-tiranry">Closed</span>
            @endif
            <!--  <a href="mailto:?subject={{$post->title}}&amp;body={{$post->article}}" title="Share by Email">
               <img src="http://png-2.findicons.com/files/icons/573/must_have/48/mail.png">
             </a> -->
            <a href="#" title="share" class="ed-opts-open"> <i class="la la-share-alt"></i> </a>
            <ul class="ed-options">
                {!! Share::page(route('frontend.social_post_details',[$post->post_id,str_slug($post->title)]), $post->title,['target' => '_blank'])
                ->facebook()
                ->twitter()
                ->linkedin()
                !!}
                <li>
                    <a href="mailto:?subject={{$post->title}}&amp;body={{route('frontend.social_post_details',[$post->post_id,str_slug($post->title)])}}" title="Share by Email">
                        <span class="fa fa-envelope"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="post_det_dec">
        @php
        $org_serve = App\OrganizationServes::whereIn('id',explode(',',$post->user->organization_serves))->get();
        @endphp
        @if( count($org_serve)>0)
        @php
        $srver = "";
        @endphp
        @foreach($org_serve as $os)
        @php
        $srver .= $os->name.",";
        @endphp
        @endforeach
        <p>{{rtrim($srver, ',')}}</p>
        @endif

    </div>

    <div class="epi-sec">
        <ul class="descp">
            <li><img src="images/icon8.png" alt=""><span>Epic Coder</span></li>
            <li><img src="images/icon9.png" alt=""><span>India</span></li>
        </ul>
        <ul class="bk-links">
            <li><a href="#" title=""><i class="la la-bookmark"></i></a></li>
            <li><a href="#" title=""><i class="la la-envelope"></i></a></li>
        </ul>
    </div>
    <div class="job_descp post_deta">
        <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id,'post_title'=>str_slug($post->title)])}}">
            <h3>{{$post->title}}</h3>
        </a>
        <span class="date_b">{{ $post->created_at ? $post->created_at->format('m-d-Y') : ''}}</span>
        <div class="clearfix"></div>
        <ul class="job-dt">
            <li><a href="#" title="">Full Time</a></li>
            <li><span>$30 / hr</span></li>
        </ul>

        <p class="job_descp_home">{{ $post->article }}
        <p>Quantity: {{$post->qnty}}</p>
        <p class="job_descp_home_link">
            <a href="{{route('frontend.user.post-details',['post_id'=>$post->post_id,'post_title'=>str_slug($post->title)])}}">view details</a>   
        </p>
        <div class="collapse" id="viewdetails">
            <div class="post_detail-type">
                <div class="details">
                    <label>Category </label> <span class="span_col">:</span>
                    @if(isset($post->category->name))
                    <span>
                        {{$post->category_id != 11 ? $post->category->name : $post->category_name}}
                    </span>
                    @endif

                </div>
                <div class="details">
                    <label>Location </label> <span class="span_col">:</span>
                    @if($post->type != 2)
                    <span>
                        <!-- {{$post->location}} -->
                        {{ ($post->state) ? $post->state : "" }}
                        {{ ($post->city) ? ", ".$post->city : "" }}
                        {{ ($post->pincode) ? ", ".$post->pincode : "" }}
                    </span>
                    @endif
                </div>

            </div>
        </div>
    </div>
    <div class="job-status-bar post-sta">
        <ul class="like-com">
            <input type="hidden" id="comment_loaded{{$post->post_id}}" value="0">
            <li><a data-id="{{$post->post_id}}" class="com"><i class="fa fa-comment-alt"></i> Comments</a></li>
        </ul>
        @if(auth()->user()->id != $post->company_id)
        @endif
    </div>
</div>
@endforeach
@else
<div class="post-bar">
    Currently there are no active posts to display
</div>
@endif