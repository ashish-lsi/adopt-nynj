//----------------------------On location change submit the form----------------------------
$('#weather-lat').change(function (e) {
    $('#frmWeather').submit();
});

//----------------------------On form submit do the action----------------------------
$('#frmWeather').submit(function (e) {
    var country = $('#weather-country').val();
    var addr_lat = $('#weather-lat').val();

    if (country != 'United States') {
        alert('Only United States locations are allowed to register!!');
        $('#weather-addr').focus();
        return false;
    }

    if (addr_lat == '') {
        alert('Please select a valid location!!');
        $('#weather-addr').focus();
        return false;
    }
});

//----------------------------On form submit do the action----------------------------
$('#frmWeatherNav').submit(function (e) {
    var country = $('#weather-country-nav').val();
    var addr_lat = $('#weather-lat-nav').val();

    if (country != 'United States') {
        alert('Only United States locations are allowed to register!!');
        $('#weather-addr-nav').focus();
        return false;
    }
});

//----------------------------Pagination things----------------------------
if ($('#pagination-demo').length > 0) {
    var totalPage = $('.jumbotron').length;

    if (totalPage > 0) {
        $('#pagination-demo').twbsPagination({
            totalPages: totalPage,
            visiblePages: totalPage,
            prev: 'Prev',
            next: 'Next',
            // callback function
            onPageClick: function (event, page) {
                $('.page-active').removeClass('page-active');
                $('#page' + page).addClass('page-active');
            },
        });
    }
}

//----------------------------Load the weather widget----------------------------
if ($('#openweathermap-widget-18').length > 0) {
    window.myWidgetParam ? window.myWidgetParam : window.myWidgetParam = [];
    window.myWidgetParam.push({id: 18, cityid: cityId, appid: weather_key, units: 'imperial', containerid: 'openweathermap-widget-18', });
    (function () {
        var script = document.createElement('script');
        script.async = true;
        script.charset = "utf-8";
        script.src = jsUrl;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(script, s);
    })();
}

//----------------------------Loading the weather widget in navigation menu----------------------------
$(document).ready(function () {
    if ($('#openweathermap-widget-13').length > 0) {
        navigator.geolocation.getCurrentPosition(
                function (position) { // success cb
                    var lat = position.coords.latitude;
                    var lng = position.coords.longitude;

                    $.ajax({
                        url: "/weather/getLatLang",
                        data: {lat: lat, lng: lng},
                        type: 'post',
                        success: function (data) {
                            showTopWeather(data)
                        }
                    });
                },
                );
    }
});

function showTopWeather(user_city_id) {
    window.myWidgetParam ? window.myWidgetParam : window.myWidgetParam = [];
    window.myWidgetParam.push({id: 13, cityid: user_city_id, appid: weather_key, units: 'imperial', containerid: 'openweathermap-widget-13'});

    (function () {
        var script = document.createElement('script');
        script.async = true;
        script.charset = "utf-8";
        script.src = "/js/backend/weather-widget-generator.js";
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(script, s);
    })();
}