$(window).on("load", function (e) {
    "use strict";
    $(".loader-container").fadeOut("slow");

    setTimeout(function () {
        $('.alert').fadeOut('slow');
    }, 10000);
    //  ============= POST PROJECT POPUP FUNCTION =========

    //  $(".chat-hist, .messages-line").mCustomScrollbar();
    //      axis:"yx"
    // });

    // $("#RegisterButton").on("click", function () {
    //     $(".post-popup.pst-pj").addClass("active");
    //     $(".wrapper").addClass("overlay");
    //     return false;
    // });

    $(".post_project").on("click", function () {
        $(".post-popup.pst-pj").addClass("active");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".post-project > a").on("click", function () {
        $(".post-popup.pst-pj").removeClass("active");
        $(".wrapper").removeClass("overlay");
        return false;
    });


    $('#accordion h3').on("click", function () {

        $('#accordion h3 .fa-angle-up').hide();
        $('#accordion h3 .fa-angle-down').show();
         $(this).find('.fa-angle-up').show();
        $(this).find('.fa-angle-down').hide();
    });


        $('.acc h3').click(function(){
        $(this).next('.content').slideToggle();
        $(this).parent().toggleClass('active');
        $(this).parent().siblings().children('.content').slideUp();
        $(this).parent().siblings().removeClass('active');
    });

    //  ============= POST JOB POPUP FUNCTION =========

    $(".post-jb").on("click", function () {
        $(".post-popup.job_post").addClass("active");
        $(".wrapper").addClass("overlay");
        return false;
    });


    $(".edit-icon-awards .edit_pr").on("click", function () {
        $(".post-popup.awards-popup").addClass("active");
        $(".wrapper").addClass("overlay");
        return false;
    });

//    $('.edit_pr').on("click", function () {
//        $('.show_profile').fadeOut(500);
//        $('.edit_profile').fadeIn(500);
//    });

    $('.paddy .fa-times').on("click", function () {
        $('.search-dropdown .filter-secs-sec').slideUp(500);
    });

    $('.mul_sel').on("click", function () {
        $('.filter-secs-sec .multiselect-container.dropdown-menu').toggleClass('show');

    });

    $('.show_more_data').on("click", function () {
        $('.help_s .expand').fadeIn(500);
        $(this).fadeOut();
        $('.show_less_data').fadeIn(500);

    });

    $('.show_less_data').on("click", function () {
        $('.help_s .expand').fadeOut(500);
        $(this).fadeOut();
        $('.show_more_data').fadeIn(500);

    });


    $('.apply').on("click", function () {
        $(".post-popup2.job_post.apply_box").addClass("active");
        $(".wrapper").addClass("overlay");
        return false;
    });

    $(".post-project > a").on("click", function () {
        $(".post-popup.job_post,.post-popup2.job_post,.post-popup.awards-popup").removeClass("active");
        $(".wrapper").removeClass("overlay");
        return false;
    });


//   $('body').on("click", ".fa-angle-up", function (e) { 
//         e.stopPropagation();
//         $(this).fadeToggle();
//          $(this).parent().parent().find('.fa-angle-down').fadeToggle();
//           $(this).parent().parent().parent().parent().find('.job_descp.post_deta').fadeToggle();

//     });


//      $('body').on("click", ".fa-angle-down", function (e) { 
//           e.stopPropagation();
//         $(this).fadeToggle();
//         $(this).parent().parent().find('.fa-angle-up').fadeToggle();
//          $(this).parent().parent().parent().parent().find('.job_descp.post_deta').fadeToggle();


//     });


//     $('body').on("click", ".home-podcast .post-bar", function (e) { 
//          e.stopPropagation();
//         $(this).find('.job_descp.post_deta').fadeToggle();
//         $(this).find('.fa-angle-up').fadeToggle();
//         $(this).find('.fa-angle-down').fadeToggle();

//     });

    //  ============= SIGNIN CONTROL FUNCTION =========

    $('.sign-control li').on("click", function () {
        var tab_id = $(this).attr('data-tab');
        $('.sign-control li').removeClass('current');
        $('.sign_in_sec').removeClass('current');
        $(this).addClass('current animated fadeIn');
        $("#" + tab_id).addClass('current animated fadeIn');
        return false;
    });

    //  ============= SIGNIN TAB FUNCTIONALITY =========

    $('.signup-tab ul li').on("click", function () {
        var tab_id = $(this).attr('data-tab');
        $('.signup-tab ul li').removeClass('current');
        $('.dff-tab').removeClass('current');
        $(this).addClass('current animated fadeIn');
        $("#" + tab_id).addClass('current animated fadeIn');
        return false;
    });

    //  ============= SIGNIN SWITCH TAB FUNCTIONALITY =========

    $('.tab-feed ul li').on("click", function () {
        var tab_id = $(this).attr('data-tab');
        $('.tab-feed ul li').removeClass('active');
        $('.product-feed-tab').removeClass('current');
        $(this).addClass('active animated fadeIn');
        $("#" + tab_id).addClass('current animated fadeIn');
        return false;
    });



    //add remove profile

    $(".btn.btn-remove").click(function (e) {
        $(this).parent().parent().hide();

    });

    $(".add-more").click(function (e) {
        e.preventDefault();
        // var html = $(".copy").html();
        $(".copy").show();
    });


    $("body").on("click", ".remove", function () {
        e.preventDefault();
        $(".copy").hide();
    });


    //add remove profile


    //profile awards and achievment

    $('.post-bar.awards-edit').on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr('data-related');
        $('.post-bar.awards-edit').css('background', '#f3f3f3');
        $(this).css('background', '#fff');
        $(".main-ws-sec-awards .posts-section .post-bar").each(function () {
            $(this).hide();
            if ($(this).attr('id') == id) {
                $(this).show();
            }
        });
    });

    //profile awards and achievment



    //  ============= COVER GAP FUNCTION =========

    var gap = $(".container").offset().left;
    $(".cover-sec > a, .chatbox-list").css({
        "right": gap
    });

    //  ============= OVERVIEW EDIT FUNCTION =========

    $(".overview-open").on("click", function () {
        $("#overview-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#overview-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= EXPERIENCE EDIT FUNCTION =========

    $(".exp-bx-open").on("click", function () {
        $("#experience-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#experience-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= EDUCATION EDIT FUNCTION =========

    $(".ed-box-open").on("click", function () {
        $("#education-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#education-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= LOCATION EDIT FUNCTION =========

    $(".lct-box-open").on("click", function () {
        $("#location-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#location-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= SKILLS EDIT FUNCTION =========

    $(".skills-open").on("click", function () {
        $("#skills-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#skills-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= ESTABLISH EDIT FUNCTION =========

    $(".esp-bx-open").on("click", function () {
        $("#establish-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#establish-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });


    // $('.effect-16').on("click", function () {
    // if($('.effect-16').val() == ''){
    //     console.log("true");
    //     $('.effect-16 ~ label').css('top','-8px');

    // }
    // else if($('.effect-16').val() != ''){
    //       console.log("false");
    //      $('.effect-16 ~ label').css('top','8px');
    // }

    //  });



    //  ============= CREATE PORTFOLIO FUNCTION =========

    $(".portfolio-btn > a").on("click", function () {
        $("#create-portfolio").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#create-portfolio").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  ============= EMPLOYEE EDIT FUNCTION =========

    $(".emp-open").on("click", function () {
        $("#total-employes").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#total-employes").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });

    //  =============== Ask a Question Popup ============

    $(".ask-question").on("click", function () {
        $("#question-box").addClass("open");
        $(".wrapper").addClass("overlay");
        return false;
    });
    $(".close-box").on("click", function () {
        $("#question-box").removeClass("open");
        $(".wrapper").removeClass("overlay");
        return false;
    });


    //  ============== ChatBox ============== 


    $(".chat-mg").on("click", function () {
        $(this).next(".conversation-box").toggleClass("active");
        return false;
    });
    $(".close-chat").on("click", function () {
        $(".conversation-box").removeClass("active");
        return false;
    });

    //  ================== Edit Options Function =================


    $('body').on("click", ".ed-opts-open", function (e) {

        e.stopPropagation();

        $(this).next(".ed-options").toggleClass("active");
        return false;
    });


    // ============== Menu Script =============




    //  ============ Notifications Open =============

    $(".menu-btn a i").on("click", function (e) {
        e.stopPropagation();
        $(".user-account-settingss,.weather-setting").slideUp("fast");
        $(".nav-account-settings").slideToggle("fast");
    });

    $(".not-box-open").on("click", function (e) {
        e.stopPropagation();
        $("#message").hide();
        $(".user-account-settingss,.weather-setting").slideUp("fast");
        $(this).next("#notification").toggle();
    });

    //  ============ Messages Open =============

    $(".not-box-openm").on("click", function () {
        $("#notification").hide();
        $(".user-account-settingss").hide();
        $(this).next("#message").toggle();
    });


    // ============= User Account Setting Open ===========
    /*
     $(".user-info").on("click", function(){$("#users").hide();
     $(".user-account-settingss").hide();
     $(this).next("#notification").toggle();
     });
     
     */
    $(".user-info").click(function (e) {
        e.stopPropagation();
        $('.nav-account-settings').slideUp('fast');
        $(".user-account-settingss").slideToggle("fast");
        $("#message,.weather-setting").not($(this).next("#message")).slideUp();
        $("#notification").not($(this).next("#notification")).slideUp();
        // Animation complete.
    });

    $('.weather-setting,.filter-secs-sec').click(function (e) {
        e.stopPropagation();
    });



    $(".weather-info").click(function (e) {
        e.stopPropagation();
        $('.weather-setting').slideToggle("fast");
        $(".user-account-settingss,.notification-box").slideUp("fast");
        //$(".suggestions-list-weather").slideToggle("fast");
        //$("#message").not($(this).next("#message")).slideUp();
        //$("#notification").not($(this).next("#notification")).slideUp();
        // Animation complete.
    });

    $('.cv_upload input[type="file"]').click(function () {
        // $('.car_cv_upload_msg_err').html("uploading...");
        // $('.car_cv_upload_msg_err').show();
        $(this).change(function () {
            var filePath = $(this).val();
            if (filePath.match(/fakepath/)) {
                // update the file-path text using case-insensitive regex
                filePath = filePath.replace(/C:\\fakepath\\/i, '');
                console.log(filePath);
            }
            if (filePath == '') {
                $(this).parent().parent().find('label span').text('upload resume');
                $('.car_cv_upload_msg_err').html("Please enter valid file i.e. doc,docx,pdf,rtf");
                $('.car_cv_upload_msg_err').show();

            } else {
                $(this).parent().parent().find('label span').append(filePath);
                $('.car_cv_upload_msg_err').html("");
                $('.car_cv_upload_msg_err').hide();
            }
        });
    });

// Login
    var get_url = location.pathname.split('/').slice(-1)[0];

    if (get_url == 'login' || get_url == 'expired' || get_url == 'reset' || get_url == 'about') {
        $('footer').addClass('fx_login');

    }
// Login

    $(".cus_radio input[name='radio-group']").click(function () {
        $("input[name='radio-group']:checked").each(function () {
            if ($(this).val() == "Seeking help") {
                $('.weather-btn-submit,.weather-btn-2-next').hide();
                $('.weather-btn-2-next-active').show();
            } else if ($(this).val() == "Providing help") {
                $('.weather-btn-submit,.weather-btn-2-next-active').hide();
                $('.weather-btn-2-next').show();
            }
            // else if($(this).val() == "General"){
            //     $('.weather-btn-submit').show();
            //     $('.weather-btn-2-next,.weather-btn-2-next-active').hide();
            // }
        });
    });

    $('.search-d .fa-ellipsis-h').click(function (e) {
        e.stopPropagation();
        $('.search-w .filter-secs-sec').slideDown(500);
    });



    $('.weather-btn-2-next-active').click(function (e) {
        $('.form-one,.providing_help_container').hide();
        $('.seeking_help_container').show();
    });


    $('.weather-btn-2-next').click(function (e) {
        let title = $('#place_title').valid();
        let article = $('#article').valid();
        let qnty = $('#qnty').valid()
        if (title && article && qnty) {
            $('.form-one,.seeking_help_container').hide();
            $('.providing_help_container').show();
        }

    });

    $('.share_us').click(function (e) {
        $('.share_icon').toggleClass('dis_b');

    });

    $(".lb-dataContainer").insertBefore(".lb-outerContainer");

    // $(".js-select2").select2({
    //     placeholder: "Select Diversity",
    //     theme: "material"
    // });
    $(".select2-selection__arrow").addClass("material-icons").html("arrow_drop_down");




    // $('.createpost-btn').click(function(e){
    //     $('.form').slideToggle(500);
    // });

    $('.fa-arrow-left').click(function (e) {
        $('.form-one').show();
        $('.seeking_help').hide();

    });


    // $('.cv_upload input[type=file]').click(function(e){
    //     $(".cv_upload label span").text(function(index, currentText) {
    //     return currentText.substr(0, 5);
    // });
    // });


    //  ============= FORUM LINKS MOBILE MENU FUNCTION =========

    $(".forum-links-btn > a").on("click", function () {
        $(".forum-links").toggleClass("active");
        return false;
    });
    $("html").on("click", function () {
        $(".forum-links,.ed-options").removeClass("active");
        $('.notification-box,.user-account-settingss,.weather-setting,.nav-account-settings,.filter-secs-sec').slideUp();
    });
    $(".forum-links-btn > a, .forum-links").on("click", function () {
        e.stopPropagation();
    });

    //  ============= PORTFOLIO SLIDER FUNCTION =========




// resposive design change
    if ($(window).width() < 800) {
        $('.search-bar').insertAfter('.weather-search');
        $('.dash_center .help_s').insertAfter('.dash_left .item:nth-child(1)');
        $('.dash_right .item:nth-child(1)').insertAfter('.dash_left .item:nth-child(2)');
        $('.home-podcast').insertAfter('.user-data');
        $('.user-data').insertAfter('.pd-right-none .right-sidebar');
        $('.main-left-sidebar .suggestions').insertAfter('.pd-right-none .right-sidebar');
        $('.col-lg-8.no-pdd').removeClass('no-pdd');
        $('.menu-btn').insertBefore('.logo');
        $('nav').insertAfter('.user-account');
        $('.home-podcast').insertAfter('.user-data');
        // $('header nav ul li:first-child').insertAfter('.nav_create_weather');
        $('.weather-search').insertAfter('.nav_create_weather');
        $('.nav-account-settings li .fa-home').insertBefore('.not-box-open');
	$('.sign-header .col-lg-12 button').insertBefore('.checky-sec');
	$('.sign_in_sec form button').css('margin-top','0px');

// $('.forum-post-details .request-disabled.postClose').insertAfter('.forum-post-view .usr_quest > p')



        $(".menu-btn > a").on("click", function () {
            $(".nav-account-settings-con").slideToggle("fast");
            return false;
        });


        var get_outer = $('header').outerHeight() + 20;
        $('.dashboard_container,main,.weather-box,.update_pass,.forum-questions-sec').css('margin-top', get_outer + 'px')

    }


// resposive design change
    $('.chkmore').multiselect({
        includeSelectAllOption: true,
        numberDisplayed: 1
    });


    $('.chkmorenav').multiselect({
        includeSelectAllOption: true,
        numberDisplayed: 1
    });
});






function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents: function () {
        var obj = this;

        obj.dd.on('click', function (event) {
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click', function () {
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue: function () {
        return this.val;
    },
    getIndex: function () {
        return this.index;
    }
}

$(function () {

    var dd = new DropDown($('#dd'));
    var dd1 = new DropDown($('#dd1'));

    $(document).click(function () {
        // all dropdowns
        $('.wrapper-dropdown-3').removeClass('active');
        $('.wrapper-dropdown-31').removeClass('active');
    });

});



$(document).on('click', 'a.com', function (e) {
    //alert('clicks')
    var post_id = $(this).data("id");
    if ($('#comment_loaded' + $(this).data("id")).val() == 0) {
        fetchComments(post_id)
    }

    e.preventDefault();
    // debugger;
    $('.comment_box.cb' + post_id).fadeToggle();
    //  $(this).parent().closest('div').next().fadeToggle();

});
$(document).on('click', '.new-comment-box', function () {
    var data_id = $(this).data("id");
    // alert(data_id);
    $('.show-comment[data-id="' + data_id + '"]').toggleClass('hidden');
});
$(document).on('click', '.new-comment-1', function () {
    var data_id = $(this).data("id");
    // alert(data_id);
    $('.show-comment-1[data-id="' + data_id + '"]').toggleClass('hidden');
});

function fetchComments(post_id) {
    $.ajax({
        url: "/fetchComments",
        data: {"post_id": post_id},
        type: 'post',
        success: function (data) {
            $('#comments-' + post_id).html(data);
            $('#comment_loaded' + post_id).val(1);
            $('.comment_box.cb' + post_id).fadeToggle();
            $("#accordion").accordion();

            //Open the comment box with comment id
            var load_comment = $('#load_comment').val();
            if (load_comment !== '') {
                var accord_id = $('a[data-id=R' + load_comment + ']').closest('div.ui-accordion-content').attr('id'); //Get the tab id i.e ui-id-4
                var n = accord_id.lastIndexOf('-'); //get the last count i.e 4
                var tab_cnt = (accord_id.substring(n + 1) / 2) - 1; //getting the tab count to open i.e 1

                $("#accordion").accordion("option", "active", tab_cnt); //Open the tab
                var scroll_id = $('a[data-id=R' + load_comment + ']').closest('div.tab-pane').attr('id'); //get the parent divs id to scroll
                
                $('html, body').animate({
                    scrollTop: $('#' + scroll_id).offset().top - 1200
                }, 1000);
            }
        }
    })
}$(document).on('submit', '.commentForm', function (e) {
    e.preventDefault();
    var formData = new FormData(this);
    var self = this;
    // $(self).parent().toggleClass('hidden');
    $.ajax({
        url: $(self).attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
            if (data.parent == 1) {
                $('#comments-' + data.post_id).prepend(data.data);
            } else {
                $(self)[0].reset();
                $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function () { });
                $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);
                $('.show-comment[data-id="R' + data.comment_id + '"]').addClass('hidden');

                console.log(data);
            }

        },
        processData: false,
        contentType: false
    });
});
function commentAjax(form) {


    var formData = new FormData(form);
    $.ajax({
        url: $(form).attr('action'),
        type: 'POST',
        data: formData,
        dataType: 'json',
        success: function (data) {
            //$(self)[0].reset();
            $('.show-comment[data-id="' + data.comment_id + '"]').slideDown('1000', "swing", function () { });
            $('.show-comment[data-id="' + data.comment_id + '"]').html(data.data);


            console.log(data);
        },
        processData: false,
        contentType: false
    });
    return false;
}

$('#helpSeekerType').on('change', function () {
    $('.select_diversity').addClass('hidden');
});
$('#providingHelpType').on('change', function () {
    $('.select_diversity').removeClass('hidden');
});
$('#apply_post_qnty,#close_post_qnty').on('keyup', function (event) {
    var maxqnty = $(this).data('qnty');
    var current = $(this).val()

    if (current > maxqnty) {

        alert('Should not be more than ' + maxqnty)
        $(this).val('')
        return false;

    }
});


$('.comment_box ').on("click", '.postClose', function () {
    //alert('d');
    var apply_id = $(this).data('post_apply_id');
    //debugger;
    var is_reply_to_id = $(this).data('is_reply_to_id');
    var maxQnty = $(this).data('max_qnty');

    //alert(reply_user_id);
    $('#post_apply_id').val(apply_id);
    $('#is_reply_to_id_close_post').val(is_reply_to_id);
    $('#close_post_qnty').data('qnty', maxQnty);
    //alert(maxQnty);
    $(".post-popup2.post_close").addClass("active");
    $(".wrapper").addClass("overlay");
    return false;
});
//========code for scroll to bottom check ========

$('.posts-section,.comment_box').on('click', '.new-comment', function () {
    var data_p = $(this).data('id');
    $('.show-comment[data-id="' + data_p + '"]').toggleClass('hidden');
    //alert(data_p);
});

$('#location_dropdown').on('change', function () {
    if ($(this).val() == -1) {
        $('#google-locatiton').removeClass('hidden')
    } else {
        $('#google-locatiton').addClass('hidden')
        $.ajax({
            url: '/getUserAddress',
            type: 'get',
            dataType: 'json',
            data: {'address_id': $(this).val()},
            success: function (data) {
                $('#post-address').val(data[0].address);
                $('#State').val(data[0].State);
                $('#City').val(data[0].City);
                $('#Pincode').val(data[0].Pincode);
                $('#addr-lat').val(data[0].lat);
                $('#addr-long').val(data[0].lang);
            }
        });
    }
});

$("#showPwd").on("click", function () {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
});




var my_time;
$(document).ready(function () {
$('.display .display_box a').each(function() {
  $(this).insertAfter( $(this).next('img') );
});

    pageScroll();
    $("#contain").mouseover(function () {
        clearTimeout(my_time);
    }).mouseout(function () {
        pageScroll();
    });
});

function pageScroll() {
    var objDiv = document.getElementById("contain");
    if (objDiv !== null) {
        objDiv.scrollTop = objDiv.scrollTop + 1;

        if (objDiv.scrollTop == (objDiv.scrollHeight - 195)) {
            objDiv.scrollTop = 0;
        }
        my_time = setTimeout('pageScroll()', 50);
    }
}