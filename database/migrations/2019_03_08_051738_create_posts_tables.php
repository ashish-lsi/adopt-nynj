<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('post_id');
            $table->integer('company_id');
            $table->integer('category_id')->nullable();
            $table->integer('address_id')->nullable();
            $table->string('type',20);
            $table->string('title');
            $table->text('article')->nullable();
            $table->string('title_clean')->nullable();
            $table->string('file')->nullable();
            $table->string('banner_image')->nullable();
            $table->string('diversity_type_ids')->nullable();
            $table->string('location')->nullable();
            $table->string('state',30)->nullable();
            $table->string('city',30)->nullable();
            $table->string('pincode',15)->nullable();
            $table->tinyInteger('featured')->unsigned()->default(1);
            $table->tinyInteger('enabled')->unsigned()->default(1);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('comment_enable')->unsigned()->default(1);
            $table->integer('views')->nullable();
            $table->timestamps();
        });
/*
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('comment_id');
            $table->integer('company_id');
            $table->integer('post_id');
            $table->string('is_reply_to_id')->nullable();
            $table->text('comment')->nullable();
            $table->tinyInteger('mark_read')->unsigned();
            $table->tinyInteger('enabled')->unsigned();
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('category_id');
            $table->string('name');
            $table->string('name_clean')->nullable();
            $table->tinyInteger('enabled')->unsigned();
            $table->timestamps();
        });

        Schema::create('post_to_category', function (Blueprint $table) {
            $table->integer('company_id');
            $table->integer('post_id');
            $table->timestamps();
        });

        Schema::create('attachment', function (Blueprint $table) {
            $table->increments('attachment_id');
            $table->integer('post_id');
            $table->string('is_reply_to_id')->nullable();
            $table->text('comment')->nullable();
            $table->tinyInteger('mark_read')->unsigned();
            $table->tinyInteger('enabled')->unsigned();
            $table->timestamps();
        });


        Schema::create('address', function (Blueprint $table) {
            $table->increments('address_id');          
            $table->integer('company_id')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('City')->nullable();
            $table->string('State')->nullable();
            $table->string('Pincode')->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('google_map_link')->nullable();           
            $table->timestamps();
            
        });

        Schema::create('company_type', function (Blueprint $table) {
            $table->increments('company_type_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->tinyInteger('enabled')->unsigned();
            $table->timestamps();
        });
*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('category');
        Schema::dropIfExists('post_to_category');
        Schema::dropIfExists('attachment');
        Schema::dropIfExists('address');
        Schema::dropIfExists('company_type');
        
    }
}
