 <?php
//dv at josheli.com

$user = 'adoptcompany';
$pass = '8NS=.wSf';
$url = 'kingcounty.adoptcompany.com';//do not include 'http://'

/*
link spam in the referer list is a growing problem. this will remove that section of the stats.
to show referers, set this to false
*/
$remove_referers = true;

//retrieves the webalizer file, either .html or .png
function getFile($file)
{
  global $user, $pass, $url;
  return file_get_contents("https://$user:$pass@$url:2083/tmp/$user/webalizer/$file");
}

//alters links, either .html or .png
function changeLinks($subject, $type)
{
  return preg_replace("/($type=\")(?!http)(.*?)\"/is","$1{$_SERVER['PHP_SELF']}?$2\"", $subject);
}

function removeReferers($html) 
{
  $html = str_replace('<A HREF="#TOPREFS">[Referrers]</A>', '', $html);
  $html = preg_replace('@<A NAME="TOPREFS">.*?</TABLE>@is', '', $html);    
  return $html;
}

if(!empty($_SERVER['QUERY_STRING']))
{
  //get file (whether png or html)
  $page = getFile($_SERVER['QUERY_STRING']);

  //if png, output appropriate header
  if(strpos($_SERVER['QUERY_STRING'],'.png')!==false)
  {
    header("Content-type: image/png");
  }
  else //change the .png src(s)
  {
    $page = changeLinks($page, 'src');

    if($remove_referers)
    {
      $page = removeReferers($page);
    }
  }
}
else   //process index page 
{
  $page = getFile('index.html');

  //change links
  $page = changeLinks($page, 'href');

  //change the usage.png src
  $page = changeLinks($page, 'src');
}

//output it
echo $page;

?> 